package fr.univ_lille.cristal.emeraude.n2s3.apps.models

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.{Neuron, NeuronConnection}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Implementation of a neuron which just resend all received messages
  */
class PassiveNeuron extends Neuron {

  /**
    * Return a passive connection
    */
  override def defaultConnection: NeuronConnection = new PassiveNeuronConnection

  /**
    * send all received message to all output with no delay
    *
    * @param timestamp is the current time
    * @param message   is the content
    */
  override def processSomaMessage(timestamp: Timestamp, message: Message, fromSynapse : Option[Int], ends: NeuronEnds): Unit = {
    ends.sendToAllOutput(timestamp, message)
  }
}

/**
  * Specialization of the connection, which only transmit message same message to soma with some delay
  */
class PassiveNeuronConnection(delay : Int = 0) extends NeuronConnection {
  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = {
    ends.sendToOutput(timestamp+delay, message)
  }
}