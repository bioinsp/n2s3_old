package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import java.io.{File, PrintWriter}

import akka.actor.Props
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronFireEvent
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, ShapelessSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.RandomConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.DelayAdaptation
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{ExportNetworkTopology, NeuronsFireLogText}
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe
import fr.univ_lille.cristal.emeraude.n2s3.support.io._
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, Time}

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.util.Random
/**
  * Created by falezp on 14/10/16.
  */

class MoveEntry extends InputStream[InputSeq[N2S3Input]] {

  val entrySize = 10
  val velocity = 2 MilliSecond // per entry point

  var inputData = Seq[(Timestamp, Int)]()
  var labelTimestamps = Seq[(String, Timestamp)]()

  var labelCursor : Int = 0
  var dataCursor : Int = 0

  var currentSecond : Int = 0

  def generate(duration : Time, patternFrequency : Float) : Unit = {

    labelTimestamps = InputDistribution.poisson(Random, 0, duration.timestamp, patternFrequency*duration.asSecond, 2*velocity.timestamp*entrySize+(100 MilliSecond).timestamp).map{ t =>
      (if(Random.nextBoolean()) "Up" else "Down", t)
    }

    inputData = (0 until entrySize).flatMap { i =>
      /*InputDistribution.poisson(Random, 0, duration.timestamp, duration.asSecond * 5f).map(t => (t, i))++*/
        labelTimestamps.flatMap { case(label, start) =>
          if(label == "Up") {
            (0 until entrySize).map(j => (start+j*velocity.timestamp, j))
          }
          else { //down
            (0 until entrySize).map(j => (start+j*velocity.timestamp, entrySize-1-j))
          }
        }
    }.sortBy(_._1)
  }

  override def next(): InputSeq[N2S3Input] = {
   /* val chunkSize = 64

    if((inputData(cursor)._1/1000000L).toInt != currentSecond) {
      currentSecond = (inputData(cursor)._1/1000000L).toInt
      println("[Spike Train] "+currentSecond+"s")
    }

    var list = ListBuffer[N2S3InputSpike]()

    while(list.size < chunkSize && cursor < inputData.size) {
      val currentTimestamp = inputData(cursor)._1
      while(cursor < inputData.size && currentTimestamp == inputData(cursor)._1) {
        list += N2S3InputSpike(ShapelessSpike, inputData(cursor)._1, Seq(inputData(cursor)._2))
        cursor += 1
      }
    }

    //println(cursor+" / "+inputData.size+" ["+list.head.timestamp+";"+list.last.timestamp+"]")

    if(list.nonEmpty) {
      new InputSeq(list++labelTimestamps.filter(e => e._2 >= list.head.timestamp && e._2 <= list.last.timestamp).map(e => N2S3InputLabel(e._1, e._2, e._2+velocity.timestamp*entrySize)))
    }
    else InputSeq(Seq())
    */

    if(dataCursor < inputData.size && (inputData(dataCursor)._1/1000000L).toInt != currentSecond) {
      currentSecond = (inputData(dataCursor)._1/1000000L).toInt
      println("[Spike Train] "+currentSecond+"s")
    }


    val endTimestamp = if(labelCursor+1 < labelTimestamps.size) labelTimestamps(labelCursor+1)._2-1L else Long.MaxValue
    val label_info = labelTimestamps(math.min(labelCursor, labelTimestamps.size-1))

    var list = ListBuffer[N2S3InputSpike]()
    while(dataCursor < inputData.size && inputData(dataCursor)._1 <= endTimestamp) {
      list += N2S3InputSpike(ShapelessSpike, inputData(dataCursor)._1, Seq(inputData(dataCursor)._2))
      dataCursor += 1
    }
    labelCursor += 1
    if(list.nonEmpty) {

      new InputSeq(list++Some(N2S3InputLabel(label_info._1, label_info._2, endTimestamp)))
    }
    else new InputSeq(Seq())

  }

  override def atEnd(): Boolean = labelCursor >= labelTimestamps.size

  override def reset(): Unit = {
    labelCursor = 0
    dataCursor = 0
  }

  def saveLabel(filename : String) : Unit = {
    val writer = new PrintWriter(new File(filename))
    labelTimestamps.foreach { t =>
      writer.println(t)
    }
    writer.close()
  }
}

/*
 * From "A supervised learning approach based on STDP and polychronization in spiking neuron network"
 * http://publications.idiap.ch/downloads/papers/2007/paugam-esann-2007.pdf
 */
object SmallReservoir extends App {


  val n2s3 = new N2S3()

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(10))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new MoveEntry

  inputData.generate(100 Second, 10)
  inputStream.append(inputData)

  // input append

  val reservoir = n2s3.createNeuronGroup("Reservoir", 100)
    .setNeuronModel(LIFNeuron, Seq(
      (MembraneThresholdFloat, 10f),
      (MembraneLeakTime, 10 MilliSecond),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 5 MilliSecond)
    ))

  val classifier = n2s3.createNeuronGroup("Classifier", 2)
    .setNeuronModel(LIFNeuron, Seq(
      (MembraneThresholdFloat, 20f),
      (MembraneLeakTime, 20 MilliSecond),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 20 MilliSecond)
    ))

  inputLayer.connectTo(reservoir, new RandomConnection(0.1f, () => new FixedSynapse(3.0f)))
  reservoir.connectTo(reservoir, new RandomConnection(0.3f, () => new FixedSynapse(if(Random.nextFloat() <= 0.8) Random.nextFloat() else -Random.nextFloat(), Random.nextFloat()*20f MilliSecond)))
  reservoir.connectTo(classifier, new RandomConnection(1.0f, () => new FixedSynapse(0.5f, Random.nextFloat()*20f MilliSecond)))

  n2s3.create()

  ExportNetworkTopology.save("topology_graph", n2s3)

 /* val teacher = n2s3.system.actorOf(Props(new SupervisedSTDPLearning(n2s3, Map(
    ("Up", classifier.neuronPaths.head),
    ("Down", classifier.neuronPaths(1))
  ))), LocalActorDeploymentStrategy, "STDP_learning")
*/

  println("Start Training ...")
  DelayAdaptation.execute(n2s3, Map(
    "Up" -> classifier.neuronPaths.head,
    "Down" -> classifier.neuronPaths(1)
  ), 1.0f)


  inputData.reset()
  inputData.generate(10 Second, 10)
  inputStream.clean()
  inputStream.append(inputData)

  println("Start Testing ...")
  //unsupervisedLayer.fixNeurons()


  val logger = n2s3.system.actorOf(Props(new NeuronsFireLogText("fire.out")), LocalActorDeploymentStrategy)
  inputLayer.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }
  reservoir.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }
  classifier.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(classifier)

  n2s3.runAndWait()

  implicit val timeout = Config.defaultTimeout
  Await.result(logger ? Done, Config.defaultTimeout.duration)

  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")
}
