package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{Shape, StreamEntry}
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.SpikePropExtended
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{InputSeq, N2S3Input}
import squants.time.FrequencyConversions.FrequencyConversions

import scala.util.Random
/**
  * Created by falezp on 19/10/16.
  */


/*
 *  From "Extending Spikeprop"
 *  http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=1379954
 */
object SupervisedXORExtended extends App {

  5 hertz

  val n2s3 = new N2S3()

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(3))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new XOREntry(1000)
  inputStream.append(inputData)
/*
  val tau_neuron = 50 MilliSecond
  val tau_synapse = 0.2 MilliSecond*/

  val tau = 7 MilliSecond

  // todo : 1 inh neuron
  val hiddenLayer = n2s3.createNeuronGroup("Hidden_layer", 5)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 1f)/*,
      (TauMembrane, tau_neuron),
      (TauSynapse, tau_synapse)*/
    ))

  val outputLayer = n2s3.createNeuronGroup("Output_layer", 2)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 1f)/*,
      (TauMembrane, tau_neuron),
      (TauSynapse, tau_synapse)*/
    ))

  inputLayer.connectTo(hiddenLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => new FixedSynapse(Random.nextFloat(), d)))
  hiddenLayer.connectTo(outputLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => {
    new FixedSynapse(if(i == 0) -Random.nextFloat() else Random.nextFloat(), d)
  }))

  n2s3.createSynapseWeightGraphOn(hiddenLayer, outputLayer)
  n2s3.create()
/*
  val spikeResponse : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      math.exp(-delta.toDouble/tau_neuron.timestamp.toDouble)-math.exp(-delta.toDouble/tau_synapse.timestamp.toDouble)
  }

  val spikeResponseDerivative : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      math.exp(-delta.toDouble/tau_synapse.timestamp.toDouble)/tau_synapse.timestamp.toDouble - math.exp(-delta.toDouble/tau_neuron.timestamp.toDouble)/tau_neuron.timestamp.toDouble
  }*/


  val spikeResponse : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      (delta.toDouble/tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
  }

  val spikeResponseDerivative : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      -(1.0/(tau.timestamp.toDouble*tau.timestamp.toDouble))*(delta.toDouble-tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
}

  SpikePropExtended.execute(n2s3, Map(
    "0" -> outputLayer.neuronPaths.head,
    "1" -> outputLayer.neuronPaths(1)
  ), 0.01, 1.0, spikeResponse, spikeResponseDerivative)

}
