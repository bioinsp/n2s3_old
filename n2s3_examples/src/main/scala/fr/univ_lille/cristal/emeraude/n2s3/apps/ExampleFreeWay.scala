package fr.univ_lille.cristal.emeraude.n2s3.apps


import akka.actor.Props
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronFireEvent
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.report.{ImageSynapsesWeight, ReconstructSynapsesWeight}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{InputOutputGraphNetworkObserver, NeuronsFireLogText}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.concurrent.Await

object ExampleFreeWay extends App {

  val n2s3 = new N2S3()
  var lastTimestamp: Long = 0

  QBGParameters.alf_m = 0.05f
  QBGParameters.alf_p = 0.1f
  QBGParameters.beta_m = 0f
  QBGParameters.beta_p = 0f

  QBGParameters.init_mean = 0.8f
  QBGParameters.init_std = 0.16f

  val dataFile = this.getClass.getResource("/freeway.mat.dat").getFile

  val retinaReader = AERRetina.withSeparateSign(128,128)
  val inputLayer = n2s3.createInput(retinaReader)

  val layer1 = n2s3.createNeuronGroup("Layer1", 60)
      .setNeuronModel(QBGNeuron,  Seq(
        (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
        (MembraneThresholdPotential, 1060 volts),
        (MembraneRefractoryDuration, 517 MilliSecond),
        (NeuronInhibitRefractory, 10.2 MilliSecond),
        (MembraneLeakTime, 187 MilliSecond)))

  val layer2 = n2s3.createNeuronGroup("Layer2", 10)
    .setNeuronModel(QBGNeuron,  Seq(
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneThresholdPotential, 2.24 millivolts),
      (MembraneRefractoryDuration, 470 MilliSecond),
      (NeuronInhibitRefractory, 182 MilliSecond),
      (MembraneLeakTime, 477 MilliSecond)))

  inputLayer.connectTo(layer1, new FullConnection(() => new QBGNeuronConnection(14.7 MilliSecond)))
  val Layer1WTAconnection = layer1.connectToItselfUsing(new FullConnection(() => new QBGInhibitorConnection))

  n2s3.createSynapseWeightGraphOn(inputLayer, layer1)
    .setCaseDimension(1, 1)
    .setRefreshRate(10000)

  n2s3.addNetworkObserver(InputOutputGraphNetworkObserver(Seq(
    (inputLayer, 128, 4),
    (layer1, 20, 20),
    (layer2, 1, 20))))

  println("### Training First Layer ###")

  retinaReader.append(new AERInputStream(dataFile, Shape(128, 128)).repeat(8))
  n2s3.runAndWait(timeout = Config.longTimeout)

  println("### Training Second Layer ###")

  layer1.fixNeurons()
  Layer1WTAconnection.disconnect()

  layer1.connectTo(layer2, new FullConnection(() => new QBGNeuronConnection(46.5 MilliSecond)))
  val Layer2WTAconnection = layer2.connectTo(layer2, new FullConnection(() => new QBGInhibitorConnection))

  n2s3.createSynapseWeightGraphOn(layer1, layer2)
    .setCaseDimension(1, 1)

  retinaReader.append(new AERInputStream(dataFile, Shape(128, 128)).repeat(8))
  n2s3.runAndWait(timeout = Config.longTimeout)

  println("### Testing ###")

  Layer2WTAconnection.disconnect()
  layer2.fixNeurons()

  retinaReader.append(new AERInputStream(dataFile, Shape(128, 128)))

  val logs = n2s3.system.actorOf(Props(new NeuronsFireLogText("results/freeway_result")), LocalActorDeploymentStrategy)

  for(neuron <- layer2.neurons){
    neuron.send(Subscribe(NeuronFireEvent, ExternalSender.getReference(logs)))
  }
  n2s3.runAndWait(timeout = Config.longTimeout)

  println("### Save results ###")
  Await.result(ask(logs, Done)(Config.defaultTimeout), Config.defaultTimeout.duration)

  ImageSynapsesWeight.save(layer1.neurons.map(n => (n.getNetworkAddress, 128, 128)), 4, 1, "freeway.layer1.png")
  ImageSynapsesWeight.save(layer2.neurons.map(n => (n.getNetworkAddress, 1, 60)), 8, 1, "freeway.layer2.png")

  ReconstructSynapsesWeight.save(
    inputLayer.neurons.map { n => n.getNetworkAddress},
    128, 128,
    layer2.neurons.map { n => n.getNetworkAddress},
    4, 1,
    "freeway.all.png")

  println("### End ###")
}