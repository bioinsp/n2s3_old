package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdPotential
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, SetProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions._

object ExampleDigitalHex extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f
  QBGParameters.stdpWindow = 50 MilliSecond


  val oversampling = 2

  val n2s3 = new N2S3()

  val inputStream =
        DigitalHexEntry() >>
        StreamOversampling[SampleInput](oversampling, oversampling) >>
        SampleToSpikeTrainConverter(0, 23, 350 MilliSecond, 150 MilliSecond)

  val inputLayer = n2s3.createInput(inputStream)

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 18)
    .setNeuronModel(QBGNeuron, Seq(
      (MembraneThresholdPotential, 2 * oversampling millivolts)
    ))
  inputLayer.connectTo(unsupervisedLayer,
      (new FullConnection).setDefaultConnectionConstructor(() => new QBGNeuronConnectionWithNegative))
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new QBGInhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)


  println("Start Training ...")
  inputStream.append(new DigitalHexInputStream().repeat(100).shuffle())
  n2s3.runAndWait()


  println("Start Testing ...")
  unsupervisedLayer.fixNeurons()

  for(n <- unsupervisedLayer.neuronPaths) {
    ExternalSender.sendTo(n, SetProperty(WeightPrecision, 2))
  }

  inputStream.clean()
  inputStream.append(new DigitalHexInputStream().repeat(10).shuffle())
//  n2s3.runAndWait(100)
//  inputLayer.setInput(DigitalHex.distributedInput(2, 1))
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()

  println(benchmarkMonitor)
  benchmarkMonitor.exportToHtmlView("test.html")

  System.exit(0)
}
