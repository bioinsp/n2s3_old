package fr.univ_lille.cristal.emeraude.n2s3.apps.tool

import java.io.{File, PrintWriter}

import scala.io.Source

/**
  * Created by falezp on 20/06/16.
  */
object FreewayComputeResult extends App {

  val time_tolerance = 500000

  val labels = Source.fromFile("label.out").getLines().filter(_ != "").map { line =>
    val fields = line.split(" ")
    (fields(0), fields(1).toLong)
  }.toSeq.groupBy(_._1)

  val neurons = Source.fromFile("freeway_result").getLines().filter(_ != "").map { line =>
    val fields = line.split("\t")
    (fields(0).dropRight(1), fields(1).toLong)
  }.toSeq.groupBy(_._1)

  /* println(neurons.map(e => (e._1, e._2.size)).mkString(", "))*/

  val neuron_assoc = for(neuron <- neurons)  yield {
    val list_neuron_spike = neuron._2.map(_._2)
    (neuron._1, (for(label <- labels) yield {
      val list_label_spike = label._2.map(_._2)
      (label._1, list_neuron_spike.count { spike =>
        list_label_spike.exists(s => math.abs(s - spike) < time_tolerance)
      })
    }).maxBy(_._2))
  }

  val best_neuron_assoc = neuron_assoc.filter{ case (neuron, (label, score)) =>
    neuron_assoc.filter(_._1 == neuron).map(_._2._2).max == score
  }.map{ case (neuron, (label, score)) =>
    (label, neurons(neuron).map(_._2))
  }

  val writer = new PrintWriter(new File("assoc_freeway"))
  val total = (for(label <- labels) yield {
    writer.println(label._2.map(_._2).mkString(" "))
    writer.println(best_neuron_assoc.getOrElse(label._1, Nil).mkString(" "))

    val score = label._2.count{ case (_, s1) =>
      best_neuron_assoc.getOrElse(label._1, Nil).exists(s2 => math.abs(s2 - s1) < time_tolerance)
    }.toFloat/label._2.size.toFloat*100f

    println(label._1+" : "+score+"%")

    score
  }).sum

  println("Total : "+(total/labels.size.toFloat+"%"))
  writer.close()

}
