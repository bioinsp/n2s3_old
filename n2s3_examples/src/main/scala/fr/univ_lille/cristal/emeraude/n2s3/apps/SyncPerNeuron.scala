package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdPotential
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{LocalSynchronizerPolicy, N2S3}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{MnistEntry, MnistFileInputStream, SampleToSpikeTrainConverter}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.{QBGInhibitorConnection, QBGNeuron, QBGParameters}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

object SyncPerNeuron extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f

  val n2s3 = new N2S3()

  n2s3.setSynchronizerPolicy(new LocalSynchronizerPolicy)

  val inputStream = MnistEntry() >>
    new SampleToSpikeTrainConverter(0, 22, 150 MilliSecond, 350 MilliSecond)


  //  val inputLayer = n2s3.createInput(Mnist.distributedInput("data/train2k-images.idx3-ubyte", "data/train2k-labels.idx1-ubyte"))
  val inputLayer = n2s3.createInput(inputStream)
  inputStream.append(new MnistFileInputStream("data/train-images.idx3-ubyte", "data/train-labels.idx1-ubyte"))

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 30)
      .setNeuronModel(QBGNeuron, Seq(
        (MembraneThresholdPotential, 35 millivolts)
      ))
  inputLayer.connectTo(unsupervisedLayer)
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new QBGInhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)

  println("Start Training ...")
  n2s3.runAndWait()

  inputStream.append(new MnistFileInputStream("data/t10k-images.idx3-ubyte", "data/t10k-labels.idx1-ubyte"))

  println("Start Testing ...")
  unsupervisedLayer.fixNeurons()

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")
}
