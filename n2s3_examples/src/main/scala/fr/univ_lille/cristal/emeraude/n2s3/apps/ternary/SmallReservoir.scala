package fr.univ_lille.cristal.emeraude.n2s3.apps.ternary

import java.io.{File, PrintWriter}

import akka.actor.Props
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.RandomConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.SupervisedSTDPLearning
import fr.univ_lille.cristal.emeraude.n2s3.models.ternary_synapse.{FixedSynapse, Neuron, Synapse}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.io._
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, Time}

import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
  * Created by falezp on 14/10/16.
  */

class MoveEntry extends InputStream[InputSeq[N2S3Input]] {

  val entrySize = 10
  val velocity = 10 MicroSecond // per entry point

  var inputData = Seq[(Timestamp, Int)]()
  var labelTimestamps = Seq[(String, Timestamp)]()
  var cursor : Int = 0
  var currentSecond : Int = 0

  def generate(duration : Time, patternFrequency : Float) : Unit = {

    labelTimestamps = InputDistribution.poisson(Random, 0, duration.timestamp, patternFrequency*duration.asSecond).map{ t =>
      (if(Random.nextBoolean()) "Up" else "Down", t)
    }

    inputData = (0 until entrySize).flatMap { i =>
      InputDistribution.poisson(Random, 0, duration.timestamp, duration.asSecond * 5f).map(
        t => (t, i))++labelTimestamps.flatMap { case(label, start) =>
          if(label == "Up") {
            (0 until entrySize).map(j => (start+j*velocity.timestamp, j))
          }
          else { //down
            (0 until entrySize).map(j => (start+j*velocity.timestamp, entrySize-1-j))
          }
        }
    }.sortBy(_._1)
  }

  override def next(): InputSeq[N2S3Input] = {
    val chunkSize = 64

    if((inputData(cursor)._1/1000000L).toInt != currentSecond) {
      currentSecond = (inputData(cursor)._1/1000000L).toInt
      println("[Spike Train] "+currentSecond+"s")
    }

    var list = ListBuffer[N2S3InputSpike]()

    while(list.size < chunkSize && cursor < inputData.size) {
      val currentTimestamp = inputData(cursor)._1
      while(cursor < inputData.size && currentTimestamp == inputData(cursor)._1) {
        list += N2S3InputSpike(ShapelessSpike, inputData(cursor)._1, Seq(inputData(cursor)._2))
        cursor += 1
      }
    }

    println(cursor+" / "+inputData.size+" ["+list.head.timestamp+";"+list.last.timestamp+"]")

    if(list.nonEmpty) {
      new InputSeq(list++labelTimestamps.filter(e => e._2 >= list.head.timestamp && e._2 <= list.last.timestamp).map(e => N2S3InputLabel(e._1, e._2, e._2+velocity.timestamp*entrySize)))
    }
    else new InputSeq(Seq())
  }

  override def atEnd(): Boolean = cursor >= inputData.size

  override def reset(): Unit = {
    cursor = 0
  }

  def saveLabel(filename : String) : Unit = {
    val writer = new PrintWriter(new File(filename))
    labelTimestamps.foreach { t =>
      writer.println(t)
    }
    writer.close()
  }
}
object SmallReservoir extends App {


  val n2s3 = new N2S3()

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(10))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new MoveEntry

  inputData.generate(10 Second, 10)
  inputStream.append(inputData)

  // input append

  val reservoir = n2s3.createNeuronGroup("Reservoir", 300)
    .setNeuronModel(Neuron, Seq(
      (MembraneThresholdFloat, 10f),
      (MembraneLeakTime, 20 MilliSecond)
    ))

  val classifier = n2s3.createNeuronGroup("Classifier", 2)
    .setNeuronModel(Neuron, Seq(
      (MembraneThresholdFloat, 10f),
      (MembraneLeakTime, 20 MilliSecond)
    ))

  inputLayer.connectTo(reservoir, new RandomConnection(0.1f, () => new FixedSynapse(1.0f)))
  reservoir.connectTo(reservoir, new RandomConnection(0.3f, () => new FixedSynapse(Random.nextFloat()*2f-1f)))
  reservoir.connectTo(classifier, new RandomConnection(1.0f, () => new Synapse))

  n2s3.create()

  val teacher = n2s3.system.actorOf(Props(new SupervisedSTDPLearning(n2s3, Map(
    ("Up", classifier.neuronPaths.head),
    ("Down", classifier.neuronPaths(1))
  ))), LocalActorDeploymentStrategy, "STDP_learning")

  n2s3.createSynapseWeightGraphOn(reservoir, classifier)

  println("Start Training ...")
  n2s3.runAndWait()

  inputData.reset()
  inputData.generate(10 Second, 10)
  inputStream.append(inputData)

  println("Start Testing ...")
  //unsupervisedLayer.fixNeurons()

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(classifier)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")
}
