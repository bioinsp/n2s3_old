package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import java.io.{File, PrintWriter}

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{Shape, StreamEntry}
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.SpikeProp
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{InputSeq, N2S3Input}

import scala.util.Random
/**
  * Created by falezp on 19/10/16.
  */

object SupervisedXORBench extends App {


  val results = for {
    learningRate <- Seq(0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0)
    repetition <- 0 until 100
  } yield {

    val n2s3 = new N2S3()

    val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(3))
    val inputLayer = n2s3.createInput(inputStream)

    val inputData = new XOREntry(1000)
    inputStream.append(inputData)

    val tau = 7 MilliSecond



    // todo : 1 inh neuron
    val hiddenLayer = n2s3.createNeuronGroup("Hidden_layer", 5)
      .setNeuronModel(SRM2Neuron, Seq(
        (MembraneThresholdFloat, 1f)
      ))

    val outputLayer = n2s3.createNeuronGroup("Output_layer", 2)
      .setNeuronModel(SRM2Neuron, Seq(
        (MembraneThresholdFloat, 1f)
      ))

    inputLayer.connectTo(hiddenLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => new FixedSynapse(Random.nextFloat(), d)))
    hiddenLayer.connectTo(outputLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => {
      new FixedSynapse(if (i == 0) -Random.nextFloat() else Random.nextFloat(), d)
    }))

    n2s3.create()

    val spikeResponse: Long => Double = { delta =>
      if (delta <= 0.0)
        0.0
      else
        (delta.toDouble / tau.timestamp.toDouble) * math.exp(1.0 - delta.toDouble / tau.timestamp.toDouble)
    }

    val spikeResponseDerivative: Long => Double = { delta =>
      if (delta <= 0.0)
        0.0
      else
        -(1.0 / (tau.timestamp.toDouble * tau.timestamp.toDouble)) * (delta.toDouble - tau.timestamp.toDouble) * math.exp(1.0 - delta.toDouble / tau.timestamp.toDouble)
    }

    /*
     * Training
     */

    SpikeProp.execute(n2s3, Map(
      "0" -> outputLayer.neuronPaths.head,
      "1" -> outputLayer.neuronPaths(1)
    ), learningRate, spikeResponse, spikeResponseDerivative)

    /*
     * Testing
     */

    inputStream.clean()
    inputStream.append(new XOREntry(100))

    val benchmarkMonitor = n2s3.createBenchmarkMonitor(outputLayer)

    n2s3.runAndWait()

    (learningRate, benchmarkMonitor.getResult.evaluationByFirstSpiking.score)
  }

  val pw = new PrintWriter(new File("bench_results2" ))
  results.groupBy(_._1).foreach { case(learning_rate, benchs) =>
    pw.println(learning_rate+" "+benchs.map(_._2).mkString(" "))
    println(learning_rate+" "+(benchs.map(_._2).sum.toDouble/benchs.size.toDouble))
  }
  pw.close()


  //println(results.groupBy(_._1).map(e => (e._1, e._2.map(_._2).sum.toDouble/e._2.size.toDouble)).mkString(", "))
}
