package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{Shape, StreamEntry}
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.SpikeProp
import fr.univ_lille.cristal.emeraude.n2s3.models.bio.{FixedSynapse, SRM2Neuron}
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

import scala.util.Random
/**
  * Created by falezp on 21/10/16.
  */

class DelayEntry(n : Int) extends InputStream[InputSeq[N2S3Input]] {

  val inputInterval = 50 MilliSecond
  var cursor = 0

  override def next(): InputSeq[N2S3Input] = {
    cursor += 1
    new InputSeq(Seq(N2S3InputSpike(ShapelessSpike, inputInterval.timestamp*(cursor-1), Seq(0)),
      N2S3InputLabel("OK", inputInterval.timestamp*(cursor-1), inputInterval.timestamp*cursor-1)))
  }

  override def reset(): Unit = {
    cursor = 0
  }

  override def atEnd(): Boolean = cursor >= n
}

object SupervisedDelay extends App {

  val n2s3 = new N2S3()

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(1))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new DelayEntry(500)
  inputStream.append(inputData)
  val tau = 7 MilliSecond

  val hiddenLayer = n2s3.createNeuronGroup("Hidden_layer", 1)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 0.5f)
    ))

  val outputLayer = n2s3.createNeuronGroup("Output_layer",1)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 0.5f)
    ))

  inputLayer.connectTo(hiddenLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => new FixedSynapse(Random.nextFloat(), d)))
  hiddenLayer.connectTo(outputLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => new FixedSynapse(Random.nextFloat(), d)))
  n2s3.create()

  val spikeResponse : Long => Double = { delta =>
  if(delta <= 0.0)
  0.0
  else
  (delta.toDouble/tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
}

  val spikeResponseDerivative : Long => Double = { delta =>
  if(delta <= 0.0)
  0.0
  else
  -(1.0/(tau.timestamp.toDouble*tau.timestamp.toDouble))*(delta.toDouble-tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
}

  SpikeProp.execute(n2s3, Map(
  "OK" -> outputLayer.neuronPaths.head
  ), 0.001, spikeResponse, spikeResponseDerivative)

}
