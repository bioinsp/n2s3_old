package fr.univ_lille.cristal.emeraude.n2s3.apps


import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdPotential
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, SetProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{MnistEntry, MnistFileInputStream, SampleToSpikeTrainConverter}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

object  ExampleMnist extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f

  val n2s3 = new N2S3()

  val inputStream = MnistEntry() >>
    new SampleToSpikeTrainConverter(0, 22, 150 MilliSecond, 350 MilliSecond)


//  val inputLayer = n2s3.createInput(Mnist.distributedInput("data/train2k-images.idx3-ubyte", "data/train2k-labels.idx1-ubyte"))
  val inputLayer = n2s3.createInput(inputStream)
  val dataFile = this.getClass.getResource("/train-images.idx3-ubyte").getFile
  val labelFile = this.getClass.getResource("/train-labels.idx1-ubyte").getFile
  inputStream.append(new MnistFileInputStream(dataFile, labelFile))

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 30)
    .setNeuronModel(QBGNeuron, Seq(
      (MembraneThresholdPotential, 35 millivolts)))
  inputLayer.connectTo(unsupervisedLayer)

  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new QBGInhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)

  println("Start Training ...")
  n2s3.runAndWait()

  val dataTestFile = this.getClass.getResource("/t10k-images.idx3-ubyte").getFile
  val labelTestFile = this.getClass.getResource("/t10k-labels.idx1-ubyte").getFile
  inputStream.append(new MnistFileInputStream(dataTestFile, labelTestFile))

  println("Start Testing ...")
  unsupervisedLayer.fixNeurons()
  for(n <- unsupervisedLayer.neuronPaths) {
    ExternalSender.sendTo(n, SetProperty(WeightPrecision, 2) )
  }

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult.evaluationByMaxSpiking)
  benchmarkMonitor.exportToHtmlView("test.html")
}
