package fr.univ_lille.cristal.emeraude.n2s3.apps

import akka.cluster.Cluster
import fr.univ_lille.cristal.emeraude.n2s3.cluster.RandomActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdPotential
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.{FullConnection, WinnerTakeAllConnection}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.N2S3ActorSystem
import squants.electro.ElectricPotentialConversions._
import squants.time.FrequencyConversions._

object ExampleDistribution extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f

  val oversampling = 2

  val n2s3 = new N2S3("N2S3ClusterSystem")
  val cluster = Cluster(n2s3.system.asInstanceOf[N2S3ActorSystem].system)
  n2s3.buildProperties.actorDeploymentPolicy = new RandomActorDeploymentStrategy(cluster)
  cluster.registerOnMemberUp {

    println("I'm up!")

    val inputStream =
      DigitalHexEntry() >>
        StreamOversampling[SampleInput](oversampling, oversampling) >>
        new SampleToSpikeTrainConverter(0, 23, 150 MilliSecond, 350 MilliSecond) >>
        InputNoiseGenerator(1 hertz)

    val inputLayer = n2s3.createInput(inputStream)

    val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 18)
      unsupervisedLayer.setNeuronModel(QBGNeuron, Seq(
        (MembraneThresholdPotential, 2 * oversampling millivolts),
        (SynapseLTP, 50 MilliSecond)
      ))
    inputLayer.connectTo(unsupervisedLayer, (new FullConnection).setDefaultConnectionConstructor(() => new QBGNeuronConnectionWithNegative))
    unsupervisedLayer.connectTo(unsupervisedLayer, new WinnerTakeAllConnection(() => new QBGInhibitorConnection))
    n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)


    println("Start Training ...")
    inputStream.append(new DigitalHexInputStream().repeat(1).shuffle())
    n2s3.runAndWait()


    println("Start Testing ...")
    unsupervisedLayer.fixNeurons()
    inputStream.clean()
    inputStream.append(new DigitalHexInputStream().repeat(1).shuffle())
    //  n2s3.runAndWait(100)
    //  inputLayer.setInput(DigitalHex.distributedInput(2, 1))
    val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

    n2s3.runAndWait()

    println(benchmarkMonitor)
    benchmarkMonitor.exportToHtmlView("results/test.html")

    System.exit(0)
  }
}
