package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.{FullConnection, InternalConvolutionCompetition}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional.ConvolutionalLayerBuilder
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.bio
import fr.univ_lille.cristal.emeraude.n2s3.models.bio.InhibitorConnection
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

import scala.util.Random

/**
  * Created by falezp on 18/08/16.
  */
object Hmax extends App {
  {

    val n2s3 = new N2S3()

    //n2s3.buildProperties.setNetworkEntitiesPolicy(new UniqueActorPolicy)

    val inputStream = LFWEntry() >>
      IntervalSampleToSpikeTrainConverter()
        .setIntervalDuration(70 MilliSecond)
        .setIntervalInterDuration(30 MilliSecond)
        .setPauseDuration(50 MilliSecond)
        .setIntervalPerInput(1)


    val inputLayer = n2s3.createInput(inputStream)

    val s1Layer = new ConvolutionalLayerBuilder()
      .setIdentifier("C1")
      .setDefaultNeuronConstructor(() => {
        val neuron = new bio.LIFNeuron()
        neuron.setProperty(MembraneThresholdFloat, 1000f)
        neuron.setProperty(MembraneLeakTime, 20 MilliSecond)
        neuron
      }
      )
      .addInternalConnection(new InternalConvolutionCompetition(() => new QBGInhibitorConnection))
      .setSharedConnectionConstructor[SharedConnectionWeight](obj => new SharedConnection(obj), () => new SharedConnectionWeight)
      .addFilters(20, 4, 4)

   /* for {
      (orientation, i) <- List(math.Pi/8.0, math.Pi/4.0+math.Pi/8.0, math.Pi/2.0+math.Pi/8.0, 3.0*math.Pi/4.0+math.Pi/8.0).zipWithIndex
      (scale, j) <- List(0.25, 0.3, 0.5, 0.71, 1.0).zipWithIndex
    } {
      inputLayer.connectTo(s1Layer(i*4+j), new ConvolutionConnection(kernelWidth = 5, kernelHeight = 5, offsetWidth = 5, offsetHeight = 5))

    }*/

    inputStream.append(new LFWReader("../n2s3/data/lfw2"))

    val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 5)
      .setNeuronModel(bio.LIFNeuron, Seq(
        (MembraneThresholdFloat, 1000f),
        (MembraneLeakTime, 20 MilliSecond)
      ))
    inputLayer.connectTo(unsupervisedLayer, new FullConnection(() => new bio.Synapse(math.max(0f, math.min(1f, 0.55f + Random.nextGaussian().toFloat * 0.016f)))))
    unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorConnection))
    n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer).setCaseDimension(1,1)

    println("Start Training ...")
    n2s3.runAndWait()

    inputStream.append(new MnistFileInputStream("../n2s3/data/t10k-images.idx3-ubyte", "../n2s3/data/t10k-labels.idx1-ubyte"))

    println("Start Testing ...")
    //unsupervisedLayer.fixNeurons()

    val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

    n2s3.runAndWait()
    println(benchmarkMonitor.getResult)
    benchmarkMonitor.exportToHtmlView("test.html")
  }
}