package fr.univ_lille.cristal.emeraude.n2s3.apps.ternary

import java.io.{File, PrintWriter}

import akka.actor.Props
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronFireEvent
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, ShapelessSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.ManualConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{NeuronsFireLogText, NeuronsPotentialLogText}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.NeuronPotentialEvent
import fr.univ_lille.cristal.emeraude.n2s3.models.ternary_synapse.{FixedSynapse, Neuron}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.util.Random
/**
  * Created by falezp on 14/10/16.
  */

class BitStreamEntry(pattern : Seq[Boolean]) extends InputStream[InputSeq[N2S3Input]] {

  var inputData = Seq[Float]()
  var labelTimestamps = ListBuffer[Timestamp]()

  def generate(seqLen : Int) : Unit = {
    inputData = (0 until seqLen).map(_ => if(Random.nextBoolean()) 1f else 0f)
    for{
      i <- 0 until seqLen-pattern.size
    } {
      if(!pattern.zipWithIndex.exists{case (b, index) => b != (inputData(i+index) == 1f)})
        labelTimestamps += i+pattern.size-1
    }

    println(inputData.mkString(", "))
    println(labelTimestamps)
  }

  var currentTimestamp : Timestamp = 0L

  override def next(): InputSeq[N2S3Input] = {
    while(currentTimestamp.toInt < inputData.size && inputData(currentTimestamp.toInt) != 1f)
      currentTimestamp += 1

    currentTimestamp += 1

    val seq = if(currentTimestamp.toInt <= inputData.size) Seq(N2S3InputSpike(ShapelessSpike, currentTimestamp-1, Seq(0))) else Seq(N2S3InputEnd(inputData.size))

    new InputSeq(seq)
  }

  override def atEnd(): Boolean = currentTimestamp.toInt >= inputData.size

  override def reset(): Unit = {
    currentTimestamp = 0L
  }

  def saveLabel(filename : String) : Unit = {
    val writer = new PrintWriter(new File(filename))
    labelTimestamps.foreach { t =>
      writer.println(t)
    }
    writer.close()
  }
}

object BitStream extends App {
  implicit val timeout = Config.defaultTimeout

  val n2s3 = new N2S3()

  val pattern = Seq(true, true, false, true)

  //n2s3.buildProperties.setNetworkEntitiesPolicy(new UniqueActorPolicy)

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(1))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new BitStreamEntry(pattern)
  inputData.generate(100)
  inputData.saveLabel("label")

  inputStream.append(inputData)

  val delayLayer = n2s3.createNeuronGroup("delay_line", pattern.size-1)
    .setNeuronModel(Neuron, Seq(
      (MembraneThresholdFloat, 1f),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 0 MicroSecond),
      (MembraneLeakTime, 0 MicroSecond)
    ))

  val outputLayer = n2s3.createNeuronGroup("output_layer", 1)
    .setNeuronModel(Neuron, Seq(      //s.actor ! Done
      (MembraneThresholdFloat, pattern.count(_ == true).toFloat),
      (MembraneThresholdType, MembraneThresholdTypeEnum.Static),
      (MembraneRefractoryDuration, 0 MicroSecond),
      (MembraneLeakTime, 0 MicroSecond)
    ))

  inputLayer.connectTo(delayLayer, new ManualConnection(Seq(
    (0, 0, Some(new FixedSynapse(1f, 1 MicroSecond)))
  )))
  delayLayer.connectTo(delayLayer, new ManualConnection(
    for{
      i <- 0 until pattern.size-2
    } yield (i, i+1, Some(new FixedSynapse(1f, 1 MicroSecond)))
  ))

  inputLayer.connectTo(outputLayer, new ManualConnection(
    Seq((0, 0, Some(new FixedSynapse(if(pattern.last) 1f else -1f))))
  ))
  delayLayer.connectTo(outputLayer, new ManualConnection(
    for{
      i <- 0 until pattern.size-1
    } yield (i, 0, Some(new FixedSynapse(if(pattern.reverse(i+1)) 1f else -1f)))
  ))

  n2s3.create()

  val fireLogger = n2s3.system.actorOf(Props(new NeuronsPotentialLogText("out", GlobalStream.prefix)), LocalActorDeploymentStrategy)
  val logger = n2s3.system.actorOf(Props(new NeuronsFireLogText("fire.out"/*, GlobalStream.prefix*/)), LocalActorDeploymentStrategy)
  outputLayer.neuronPaths.foreach{ n =>
    ExternalSender.askTo(n, Subscribe(NeuronPotentialEvent, ExternalSender.getReference(fireLogger)))
    ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(logger)))
  }

  n2s3.runAndWait()

  Await.result(fireLogger ? Done, timeout.duration)
  Await.result(logger ? Done, timeout.duration)

  println("end")

}
