package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, SetProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.models.bio
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

import scala.util.Random

/**
  * Created by falezp on 31/08/16.
  */
object SevenSegment extends App {

  val n2s3 = new N2S3()

  val inputStream =
    SevenSegmentEntry() >>
      IntervalSampleToSpikeTrainConverter()
        .setIntervalDuration(70 MilliSecond)
        .setIntervalInterDuration(30 MilliSecond)
        .setPauseDuration(50 MilliSecond)
        .setIntervalPerInput(10)

  val inputLayer = n2s3.createInput(inputStream)
  //  val inputLayer = n2s3.createInput(DigitalHex.distributedInput(2, 1))

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 10)
    .setNeuronModel(bio.LIFNeuron, Seq(
      (MembraneThresholdFloat, 3f),
      (MembraneLeakTime, 20 MilliSecond)
    ))
  inputLayer.connectTo(unsupervisedLayer, new FullConnection(() => new bio.Synapse(math.max(0f, math.min(1f, 0.6f+Random.nextGaussian().toFloat*0.2f)))))
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)

  println("Start Training ...")
  inputStream.append(new SevenSegmentInputStream().repeat(10000).shuffle())
  n2s3.runAndWait()


  println("Start Testing ...")
  for(n <- unsupervisedLayer.neuronPaths) {
    ExternalSender.sendTo(n, SetProperty(FixedParameter, true) )
    ExternalSender.sendTo(n, SetProperty(WeightPrecision, 2) )
  }

  inputStream.clean()
  inputStream.append(new SevenSegmentInputStream().repeat(100).shuffle())
  //  n2s3.runAndWait(100)
  //  inputLayer.setInput(DigitalHex.distributedInput(2, 1))
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()

  println(benchmarkMonitor)
  benchmarkMonitor.exportToHtmlView("test.html")

  System.exit(0)
}
