package fr.univ_lille.cristal.emeraude.n2s3.apps

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdPotential
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.WinnerTakeAllConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{InputNoiseGenerator, _}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions
import squants.time.FrequencyConversions._

object ExampleSeparateInhibitorLayer extends App {

  QBGParameters.alf_m = 0.02f
  QBGParameters.alf_p = 0.06f
  QBGParameters.beta_m = 3f
  QBGParameters.beta_p = 2f

  val oversampling = 2

  val n2s3 = new N2S3()
  val inputStream =
    DigitalHexEntry() >>
      StreamOversampling[SampleInput](oversampling, oversampling) >>
      new SampleToSpikeTrainConverter(0, 23, 150 MilliSecond, 350 MilliSecond) >>
      InputNoiseGenerator(1 hertz)

  val inputLayer = n2s3.createInput(inputStream)

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 20)
    .setNeuronModel(QBGNeuron, Seq(
      (MembraneThresholdPotential, 6 V)))

  val inhibitorLayer = n2s3.createNeuronGroup("InhibitorLayer", 20)
    .setNeuronModel(QBGNeuron)

  inputLayer.connectTo(unsupervisedLayer)
  unsupervisedLayer.connectTo(inhibitorLayer, new WinnerTakeAllConnection(() => new QBGInhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer)

  println("Start Training ...")
  inputStream.append(new DigitalHexInputStream().repeat(100).shuffle())
  n2s3.runAndWait()


  println("Start Testing ...")
  inputStream.append(new DigitalHexInputStream().repeat(10).shuffle())
  unsupervisedLayer.fixNeurons()
  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()

  println(benchmarkMonitor)
  benchmarkMonitor.exportToHtmlView("results/test.html")
}
