package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneLeakTime, MembraneThresholdFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, SetProperty}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.N2S3InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{IntervalSampleToSpikeTrainConverter, MnistEntry, MnistFileInputStream, StreamOversampling}
import fr.univ_lille.cristal.emeraude.n2s3.models.bio
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

/**
  * Created by falezp on 10/08/16.
  */
object MNIST extends App {

  val n2s3 = new N2S3()

  //n2s3.buildProperties.setNetworkEntitiesPolicy(new UniqueActorPolicy)

  val inputStream = MnistEntry() >>
    StreamOversampling(10, 10) >>
    IntervalSampleToSpikeTrainConverter()
      .setIntervalDuration(70 MilliSecond)
      .setIntervalInterDuration(30 MilliSecond)
      .setPauseDuration(70 MilliSecond)
      .setIntervalPerInput(1)


  //  val inputLayer = n2s3.createInput(Mnist.distributedInput("data/train2k-images.idx3-ubyte", "data/train2k-labels.idx1-ubyte"))
  val inputLayer = n2s3.createInput(inputStream)
  inputStream.append(new MnistFileInputStream("../n2s3/data/train-images.idx3-ubyte", "../n2s3/data/train-labels.idx1-ubyte").take(1000))

  val unsupervisedLayer = n2s3.createNeuronGroup("Layer1", 10)
    .setNeuronModel(bio.LIFNeuron, Seq(
      (MembraneThresholdFloat, 1000f),
      (MembraneLeakTime, 20 MilliSecond)
  ))
  inputLayer.connectTo(unsupervisedLayer, new FullConnection(() => new ReducedPrecisionSynapse/*bio.Synapse(math.max(0f, math.min(1f, 0.55f+Random.nextGaussian().toFloat*0.016f)))*/))
  unsupervisedLayer.connectTo(unsupervisedLayer, new FullConnection(() => new InhibitorConnection))
  n2s3.createSynapseWeightGraphOn(inputLayer, unsupervisedLayer).setCaseDimension(1,1)

  println("Start Training ...")
  n2s3.runAndWait()

  inputStream.append(new MnistFileInputStream("../n2s3/data/t10k-images.idx3-ubyte", "../n2s3/data/t10k-labels.idx1-ubyte").take(100))

  println("Start Testing ...")
  for(n <- unsupervisedLayer.neuronPaths) {
    ExternalSender.sendTo(n, SetProperty(FixedParameter, true) )
    ExternalSender.sendTo(n, SetProperty(WeightPrecision, 2) )
  }

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(unsupervisedLayer)

  n2s3.runAndWait()
  println(benchmarkMonitor.getResult)
  benchmarkMonitor.exportToHtmlView("test.html")
}
