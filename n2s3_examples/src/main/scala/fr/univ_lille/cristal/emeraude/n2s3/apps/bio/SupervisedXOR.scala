package fr.univ_lille.cristal.emeraude.n2s3.apps.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.MembraneThresholdFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.SpikeProp
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.ExportNetworkTopology
import fr.univ_lille.cristal.emeraude.n2s3.models.bio._
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

import scala.util.Random
/**
  * Created by falezp on 19/10/16.
  */

class XOREntry(val limit : Int) extends InputStream[InputSeq[N2S3Input]] {

  val inputDelay = 6 MilliSecond
  val inputInterval = 50 MilliSecond
  var cursor = 0

  val data = Seq(
      Seq(0.0, 0.0, 0.0) -> "0",
      Seq(0.0, 1.0, 0.0) -> "1",
      Seq(1.0, 0.0, 0.0) -> "1",
      Seq(1.0, 1.0, 0.0) -> "0"
  )

  override def next(): InputSeq[N2S3Input] = {
    val currData = data(Random.nextInt(data.size))

    val inputs = currData._1.zipWithIndex.map { case(d, i) =>
      N2S3InputSpike(ShapelessSpike, inputInterval.timestamp*cursor+(inputDelay.timestamp*d).toLong, Seq(i))
    }++Some(N2S3InputLabel(currData._2, inputInterval.timestamp*cursor, inputInterval.timestamp*(cursor+1)))

    cursor += 1

    new InputSeq(inputs)
  }

  override def reset(): Unit = {
    cursor = 0
  }

  override def atEnd(): Boolean = cursor >= limit
}

class MultipleDelayConnection(delays : Seq[Time], neuronConnectionConstructor : (Int, Int, Time) => NeuronConnection) extends ConnectionPolicy {
  override def generate(from : NeuronIterable, to : NeuronIterable) = {
    for {
      (in, in_index) <- from.toSeq.zipWithIndex
      (out, out_index) <- to.toSeq.zipWithIndex
      delay <- delays
      if in != out
    } yield {
      Connection(in, out, Some(neuronConnectionConstructor(in_index, out_index, delay)))
    }
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    for {
      (in, in_index) <- from.neurons.zipWithIndex
      (out, out_index) <- to.neurons.zipWithIndex
      delay <- delays
      if this.connects(in, out)
    } yield {
      Connection(in.actorPath.get, out.actorPath.get, Some(neuronConnectionConstructor(in_index, out_index, delay)))
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = aNeuron != anotherNeuron
}

/*
 *  From "SpikeProp: backpropagation for networks of spiking neurons."
 *  http://homepages.cwi.nl/~sbohte/publication/backprop.pdf
 */
object SupervisedXOR extends App {

  val n2s3 = new N2S3()

  val inputStream = new StreamEntry[InputSeq[N2S3Input]](Shape(3))
  val inputLayer = n2s3.createInput(inputStream)

  val inputData = new XOREntry(1000)
  inputStream.append(inputData)
/*
  val tau_neuron = 50 MilliSecond
  val tau_synapse = 0.2 MilliSecond*/

  val tau = 7 MilliSecond



  // todo : 1 inh neuron
  val hiddenLayer = n2s3.createNeuronGroup("Hidden_layer", 5)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 1f)/*,
      (TauMembrane, tau_neuron),
      (TauSynapse, tau_synapse)*/
    ))

  val outputLayer = n2s3.createNeuronGroup("Output_layer", 2)
    .setNeuronModel(SRM2Neuron, Seq(
      (MembraneThresholdFloat, 1f)/*,
      (TauMembrane, tau_neuron),
      (TauSynapse, tau_synapse)*/
    ))

  inputLayer.connectTo(hiddenLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => new FixedSynapse(Random.nextFloat(), d)))
  hiddenLayer.connectTo(outputLayer, new MultipleDelayConnection((1 to 16).map(_ MilliSecond), (i, o, d) => {
    new FixedSynapse(if(i == 0) -Random.nextFloat() else Random.nextFloat(), d)
  }))

  n2s3.createSynapseWeightGraphOn(hiddenLayer, outputLayer)
  n2s3.create()

  ExportNetworkTopology.save("topology_graph", n2s3)
/*
  val spikeResponse : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      math.exp(-delta.toDouble/tau_neuron.timestamp.toDouble)-math.exp(-delta.toDouble/tau_synapse.timestamp.toDouble)
  }

  val spikeResponseDerivative : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      math.exp(-delta.toDouble/tau_synapse.timestamp.toDouble)/tau_synapse.timestamp.toDouble - math.exp(-delta.toDouble/tau_neuron.timestamp.toDouble)/tau_neuron.timestamp.toDouble
  }*/


  val spikeResponse : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      (delta.toDouble/tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
  }

  val spikeResponseDerivative : Long => Double = { delta =>
    if(delta <= 0.0)
      0.0
    else
      -(1.0/(tau.timestamp.toDouble*tau.timestamp.toDouble))*(delta.toDouble-tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
}

  /*
   * Training
   */

  SpikeProp.execute(n2s3, Map(
    "0" -> outputLayer.neuronPaths.head,
    "1" -> outputLayer.neuronPaths(1)
  ), 0.001, spikeResponse, spikeResponseDerivative)

  /*
   * Testing
   */

  inputStream.clean()
  inputStream.append(new XOREntry(100))

  val benchmarkMonitor = n2s3.createBenchmarkMonitor(outputLayer)

  n2s3.runAndWait()

  println(benchmarkMonitor.getResult.evaluationByFirstSpiking.score)
  benchmarkMonitor.exportToHtmlView("xor.html")

}
