package fr.univ_lille.cristal.emeraude.n2s3.apps;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class Converter {
	
	public static void main(String[] args) throws IOException {
		
		//
		// Parameter
		//
		
        int start_label = 0;
        int end_label = 60000;
        String file_input = "train-labels.idx1-ubyte";
        String file_output = "labels.txt";
		
		//
		// Code
		//
		
		Path p = FileSystems.getDefault().getPath("", file_input);
        byte [] file_data = Files.readAllBytes(p);
        
        PrintWriter writer = new PrintWriter(new File(file_output));
        
        for(int i=start_label; i < end_label; i++) {
        	writer.println(file_data[8+i]);
        }
        
        writer.close();
        
        System.out.println("Done.");
	}

}
