import sbt.Keys._

/**
  * SBT project description
  */

name             := "N2S3_examples"

/*********************************************************************************************************************
  * Dependencies
  *******************************************************************************************************************/
libraryDependencies ++= {
  val akkaV = "2.3.7"
  Seq(
    "com.typesafe.akka" %% "akka-cluster"   % akkaV
  )
}
