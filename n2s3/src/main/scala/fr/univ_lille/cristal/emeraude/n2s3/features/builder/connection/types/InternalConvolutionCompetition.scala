package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, InternalConnectionPolicy}

/**
  * Created by falezp on 31/05/16.
  */
@deprecated("Should be replaced by external connection policies", "4/8/2016")
class InternalConvolutionCompetition(inhibitorConnection : () => NeuronConnection) extends InternalConnectionPolicy {

  def generate(layer: NeuronIterable) = {
    assert(layer.shape.dimensionNumber == 3)

    for{
      from_filter_index <- 0 until layer.shape(0)
      to_filter_index <- 0 until layer.shape(0)

      x <- 0 until layer.shape(1)
      y <- 0 until layer.shape(2)

      if from_filter_index != to_filter_index
    } yield {
      //println(from_filter_index+", "+x+", "+y+" -> "+to_filter_index+", "+x+", "+y)
        Connection(layer(from_filter_index, x, y), layer(to_filter_index, x, y), Some(inhibitorConnection()))
    }
  }

  override def generate(layer: NeuronGroupRef): Traversable[Connection] = {
    throw new NotImplementedError()
  }
}
