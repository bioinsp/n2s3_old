/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez 31/05/16.
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.convolutional.SharedNeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

/***********************************************************************************************************
 * 
 ***********************************************************************************************************/
class SharedConnectionWeight {
  var weight = 0.25f+Random.nextFloat()/2f
}


/***********************************************************************************************************
 * 
 ***********************************************************************************************************/
class SharedConnection(o1 : SharedConnectionWeight) extends SharedNeuronConnection[SharedConnectionWeight](o1) {
  var lastPreSpikeTimestamp : Timestamp = Long.MinValue

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      lastPreSpikeTimestamp = timestamp
      ends.sendToOutput(timestamp, WeightedSpike(obj.weight))
    case BackwardSpike =>
      obj.weight = if(lastPreSpikeTimestamp < timestamp - QBGParameters.stdpWindow.timestamp)
        QBGFormula.decreaseWeight(obj.weight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
      else {
        //assocObj.foreach(other => other.weight = other.weight-(other.weight-QBGFormula.decreaseWeight(other.weight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p))/assocObj.size.toFloat)
        QBGFormula.increaseWeight(obj.weight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
      }

  }

  def getWeight = obj.weight
  def setWeight(value : Float) : Unit = obj.weight = value
}
