package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.{File, FileOutputStream, PrintWriter}
import java.nio.file.Paths

import akka.actor.Actor
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{InputOutputGraph, NeuronsGraph, ValuesGraph}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.NeuronPotentialResponse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitTime.{TimeUnitType, _}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, PropsBuilder, SinglePropsBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.concurrent.duration._



object NeuronsFireLogText extends ActorCompanion {

  override def newPropsBuilder(): PropsBuilder = new SinglePropsBuilder[NeuronsFireLogText]
}

/**
 * Will monitor neurons which are firing in the file output/log/neuronfire.log
 */
class NeuronsFireLogText(filename : String ) extends Actor {
  val fos = new PrintWriter(new File(filename))

  def receive = {
    case NeuronFireResponse(timestamp, path) =>
      fos.println(path + "\t" + timestamp + "\n")
    case Done =>
      fos.close()
      sender ! Done
  }
}

/**
 * Will monitor potentials of neurons in the file output/log/neuron.log
 */
class NeuronsPotentialLogText(filename : String, startTimestamp : Timestamp = 0) extends Actor {
  val logFile = Paths.get(filename)
  /*
  if (!Files.exists(logFile.getParent)) {
     Files.createDirectories(logFile.getParent)
  }*/
  
  val fos: FileOutputStream = new FileOutputStream(logFile.toString)
  def receive = {
    case NeuronPotentialResponse(timestamp, source, value) =>
      fos.write((source.path.name + "\t" + (timestamp-startTimestamp) + "\t" + value + "\n").map(_.toByte).toArray)
    case Done =>
      fos.close()
      sender ! Done
  }
}

class NeuronInputOutputGraph(layer : Seq[(String, Int, Seq[NetworkEntityPath], Int)]) extends Actor {

  object ProcessDisplay
  
  implicit val timeout = Timeout(1000 seconds)
  
  override def postRestart(t : Throwable) = {System.exit(0)}
  
  var graph : InputOutputGraph =  new InputOutputGraph(layer)
  var hasRequestDisplay = false

  layer.foreach{ case (_, _, list, _) => list.foreach{ path => ExternalSender.sendTo(path,  Subscribe(NeuronFireEvent, ExternalSender.getReference(self))) }}

  var lastTime : Int = 0

  def receive = {
    case NeuronFireResponse(timestamp, path) =>
      graph.event(timestamp, path)
      if(!hasRequestDisplay) {
        self ! ProcessDisplay
        hasRequestDisplay = true
      }
      if(timestamp/1000000 > lastTime) {
        lastTime = (timestamp/1000000).toInt
        println("[Graph] "+lastTime+" seconds")
      }

    case ProcessDisplay =>
      hasRequestDisplay = false
  }
}

/**
 * Will monitor neurons which are firing during the simulation
 *
 * @param step : the difference in time between two points
 * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class NeuronsFireLogGraph(step: Int = 50, unit: TimeUnitType = MilliSecond) extends Actor {
  var lastT = 0L
  val l = new scala.collection.mutable.ListBuffer[NetworkEntityPath]()
  val graph = new NeuronsGraph
  def receive = {
    case NeuronFireResponse(timestamp, path) =>
      if (!l.contains(path))
        l += path
      if (timestamp - lastT > step) {
        graph.refreshSerie(convert(unit, Time(timestamp)), l.indexOf(path))
        if (!(graph isRunning()))
          graph.launch()
        lastT = timestamp
      }
  }
}

/**
 * Will monitor potentials of neurons during the simulation
 *
 * @param step the difference in time between two points
 * @param unit the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class NeuronsPotentialsLogGraph(step: Int = 50, unit: TimeUnitType = MilliSecond) extends Actor {
  val graph = new ValuesGraph
  var lastT = 0L
  def receive = {
    case NeuronPotentialResponse(timestamp, source, value) =>
      if (timestamp - lastT > step) {
        graph.refreshSerie(source.path.name, convert(unit, Time(timestamp)), value)
        if (!(graph isRunning()))
          graph.launch()
        lastT = timestamp
      }
  }
}
