package fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional

import fr.univ_lille.cristal.emeraude.n2s3.core.models.convolutional.SharedNeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.ConvolutionalConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{BuildProperties, NeuronLayer}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.ConvolutionBackPropagationMethod

import scala.collection.mutable

/**
  * Created by falezp on 23/05/16.
  */

class Filter2D(val width : Int, val height : Int, val data : Option[Array[Array[Float]]]) {
  def this(width : Int, height : Int) = this(width, height, None)
}

class ConvolutionalLayerBuilder extends LayerBuilder {

  val filters = mutable.ArrayBuffer[Filter2D]()
  var connectionConstructor : Option[Any => SharedNeuronConnection[Any]] = None
  var sharedObjConstructor : Option[() => Any] = None



  def addFilter(width : Int, height : Int) : this.type = {
    filters += new Filter2D(width, height, None)
    this
  }

  def addFilter(data : Array[Array[Float]]) : this.type = {
    assert(data.nonEmpty && data.tail.forall(f => f.size == data.head.size))
    filters += new Filter2D(data.size, data.head.size, Some(data))
    this
  }

  def addFilters(number : Int, width : Int, height : Int) : this.type = {
    filters ++= (0 until number).map(_ => new Filter2D(width, height, None))
    this
  }

  def setSharedConnectionConstructor[A](connectionConstructor : A => SharedNeuronConnection[A], sharedObjConstructor : () => A) : this.type = {
    this.connectionConstructor = Some(connectionConstructor.asInstanceOf[Any => SharedNeuronConnection[Any]])
    this.sharedObjConstructor = Some(sharedObjConstructor)
    this
  }

  override def before(properties : BuildProperties, previousLayer : Option[NeuronLayer]) : Unit = {
    assert(previousLayer.nonEmpty)
    assert(filters.nonEmpty && filters.tail.forall(f => f.width == filters.head.width && f.height == filters.head.height))
    setShape(Shape(filters.size, previousLayer.get.shape(0)-filters.head.width+1, previousLayer.get.shape(1)-filters.head.height+1))
    assert(connectionConstructor.isDefined && sharedObjConstructor.isDefined, "No shared connection constructor found")
    setDefaultLayerConnectionPolicy(new ConvolutionalConnection[Any](filters, connectionConstructor.get, sharedObjConstructor.get))
    setBackPropagationMethod(new ConvolutionBackPropagationMethod)
  }
}
