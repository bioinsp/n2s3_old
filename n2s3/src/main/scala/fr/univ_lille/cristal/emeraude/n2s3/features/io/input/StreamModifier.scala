package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.PoissonDistribution
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{DimensionalInput, InputSeq, N2S3Input, N2S3InputSpike}
import squants.time.Frequency

import scala.collection.mutable

/**
  * Created by falezp on 05/07/16.
  */

/*
  InputNoiseGenerator
 */
object InputNoiseGenerator {
  def apply(frequency : Frequency) = new InputNoiseGenerator(frequency)
}

class InputNoiseGenerator(frequency : Frequency) extends StreamConverter[InputSeq[N2S3Input], InputSeq[N2S3Input]] {

  val lastTimestamps = mutable.HashMap[Seq[Int], Timestamp]()


  override def dataConverter(in: InputSeq[N2S3Input]): InputSeq[N2S3Input] = {
    val max_timestamp = in.filter(_.isInstanceOf[N2S3InputSpike]).map(_.asInstanceOf[N2S3InputSpike].timestamp).max
    val min_timestamp = in.filter(_.isInstanceOf[N2S3InputSpike]).map(_.asInstanceOf[N2S3InputSpike].timestamp).max
    def recurseShape(remainDim : Seq[Int], index : Seq[Int]): Seq[N2S3Input] = {
      if(remainDim.isEmpty) {
        val times = new PoissonDistribution(min_timestamp, max_timestamp, 0f, frequency.toHertz).applyTo(1.0f)
        times.map(t => N2S3InputSpike(ShapelessSpike, t, index))
      }
      else
        (0 until remainDim.head).flatMap { i =>
          recurseShape(remainDim.tail, index++Some(i))
        }
    }

    val seq = new InputSeq(in++recurseShape(shape.dimensions, Nil))
    seq
  }
}

/*
  StreamOversampling
 */


object StreamOversampling {
  def apply[T <: InputSeq[_ <: DimensionalInput]](factors : Int*) = new StreamOversampling[T](factors:_*)
}

class StreamOversampling[T <: InputSeq[_ <: DimensionalInput]](factors : Int*) extends StreamConverter[T, T] {
  override def dataConverter(in: T): T = {
    def recurse(input : DimensionalInput, remainFactor : Seq[Int], remainIndex : Seq[Int], computedIndex : Seq[Int]) : Seq[DimensionalInput] = {
      if(remainFactor.isEmpty)
        Seq(input.setIndex(computedIndex:_*))
      else
        (0 until remainFactor.head).flatMap { i =>
          recurse(input, remainFactor.tail, remainIndex.tail, computedIndex++Some(remainIndex.head*remainFactor.head+i))
        }
    }
    in.asInstanceOf[InputSeq[DimensionalInput]].transformInputSeq(i => i.flatMap(entry => recurse(entry, factors, entry.getIndex, Seq()))).asInstanceOf[T]
  }
  override def shapeConverter(shape : Shape): Shape = {
    assert(shape.dimensionNumber == factors.size)
    Shape(shape.dimensions.zip(factors).map{case(a, b) => a*b}:_*)
  }
}