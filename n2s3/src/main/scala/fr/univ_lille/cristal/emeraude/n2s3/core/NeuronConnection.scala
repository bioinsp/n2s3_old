/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Connection, NeuronEnds, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * NeuronConnection Companion object
  */
object NeuronConnection {

  /**
    * Class used to manage communication with input and output neurons of a connection
    *
    * @param connection is the current connection object
    * @param neuron is the output neuron, which contains the connection
    * @param currentTimestamp is the timestamp of the current process
    * @param connectionId is the id of the current connection
    */
  class ConnectionEnds(connection : Connection, neuron : Neuron, currentTimestamp : Timestamp, connectionId: Int) {

    var directMessageSent : Int = 0

    /**
      * Send a message to the input neuron
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToInput(timestamp : Timestamp, message : Message) : Unit = {
      if(timestamp >= currentTimestamp) {
        connection.preSyncRef.send(NeuronMessage(timestamp, message, connection.preSyncRef, connection.postSyncRef, None))
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }

    /**
      * Send a message to the input neuron. In case of no delay, the output neuron will immediately process the message
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToOutput(timestamp : Timestamp, message : Message) : Unit = {
      if(timestamp == currentTimestamp) {
        val ends = new NeuronEnds(neuron, timestamp)
        neuron.processSomaMessage(timestamp, message, Some(connectionId), ends)
      }
      else if(timestamp >= currentTimestamp) {
        neuron.thisToSyncRef.send(NeuronMessage(timestamp, message, neuron.thisToSyncRef, neuron.syncToThisRef, Some(connectionId)))
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }
  }
}

/**
  * Basic trait for all neuron connection
  *
  */
trait NeuronConnection extends Serializable{
  import NeuronConnection._

  /**
    * Method used to describe the behavior of the connection model
    *
    * @param timestamp is the current time
    * @param message is the content
    * @param ends is the object which allow communication with ends
    */
  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit
}

