package fr.univ_lille.cristal.emeraude.n2s3.features.learning

/**
  * Created by falezp on 25/05/16.
  */

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Next, WaitEndOfActivity}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, GetProperty, NetworkEntityPath, PropertyValue, _}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronLayer
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.ValuesGraph
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{AbstractActorSystem, LocalActorDeploymentStrategy, PropsBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._
/**
  * Created by falezp on 24/05/16.
  */


object NeuronValue extends Property[Float]

object BackPropagation {

  def compute(system: AbstractActorSystem[ActorRef], synchronizer : NetworkEntityPath, layers : Seq[NeuronLayer], labels : Map[String, NetworkEntityPath], learningRate : Float, displayGraph : Boolean) : Unit = {

    object AskLabel

    implicit val timeout = Config.defaultTimeout

    val labelReceiver = system.actorOf(new PropsBuilder() {
      override def build(): Props = Props(new Actor {

        var currentLabel = ""

        override def receive: Receive = {
          case LabelChangeResponse(startTime, endTime, label) => currentLabel = label
          case AskLabel => sender ! currentLabel
        }
      })
    }, LocalActorDeploymentStrategy)

    val error_graph : Option[ValuesGraph] = if(displayGraph) Some(new ValuesGraph) else None

    var counter = 0
    var avg_error = 0.0
    ExternalSender.sendTo(layers.head.getContainer, Subscribe(LabelChangeEvent, ExternalSender.getReference(labelReceiver)))

    while(ExternalSender.askTo(layers.head.getContainer, AskRemainInput).asInstanceOf[Boolean]) {
      val neuronsValue = mutable.Map[NetworkEntityPath, Float]()
      val neuronsError = mutable.Map[NetworkEntityPath, Float]()
      val synapsesValue = mutable.Map[(NetworkEntityPath, NetworkEntityPath), Float]()
      val forwardConnections = mutable.Map[NetworkEntityPath, mutable.ArrayBuffer[NetworkEntityPath]]()

      try {

        ExternalSender.askTo(synchronizer, Next)
        ExternalSender.askTo(synchronizer, WaitEndOfActivity, 100 seconds)

        val label = Await.result(labelReceiver ? AskLabel, Config.defaultTimeout.duration).asInstanceOf[String]

        // compute output <> expected error
        val outputs = Map(layers.last.toSeq.map { neuron =>
          (neuron, (ExternalSender.askTo(neuron, GetProperty(NeuronValue)) match {
            case PropertyValue(v: Float) => v
          }, if(labels(label) == neuron) 1.0f else 0.0f))
        }:_*)


        layers.tail.reverse.zipWithIndex.foreach { case(layer, index) =>
          layer.executeBackPropagation(neuronsValue, neuronsError, synapsesValue, forwardConnections, learningRate,
            if(index == 0)
              outputs.map{ case (neuron, (computed, expected)) => (neuron, computed-expected)}
            else
              Map(layer.toSeq.map { neuron =>
                (neuron, forwardConnections(neuron).foldLeft(0f) { (acc, to) =>
                  acc + neuronsError(to) * synapsesValue(neuron, to)
                })
              }:_*)
          )
        }

        if(displayGraph) {
          val errors = outputs.map{ case (neuron, (computed, expected)) => computed-expected}

          val total_error = errors.foldLeft(0.0f)((acc, curr) => acc + 0.5f * curr * curr)
          avg_error += total_error
          if (counter == 0) {
            error_graph.get.addSerie("error", 0, total_error)
            error_graph.get.addSerie("avg", counter, (avg_error / (counter + 1).toDouble).toFloat)
            error_graph.get.launch()
          }
          else {
            error_graph.get.refreshSerie("error", counter, total_error)
            error_graph.get.refreshSerie("avg", counter, (avg_error / (counter + 1).toDouble).toFloat)
          }
        }
      }
      catch {
        case e : java.util.concurrent.TimeoutException =>
          println("[Warning] Blocked network ("+e+")")
      }
      counter += 1
    }
  }
}
