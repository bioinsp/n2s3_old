package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NeuronConnection}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{ConnectionSet, ConnectionPolicy, InternalConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.{BackPropagationMethod, DefaultBackPropagationMethod}

import scala.collection.mutable

/**
  * Created by falezp on 23/05/16.
  */
@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
class NeuronLayer(container : Option[NetworkEntityPath], neurons : Seq[NetworkEntityPath], shape : Shape) extends ShapedNeuronIterable(neurons, shape) {

  var backPropagationMethod : BackPropagationMethod = _

  def getContainer: NetworkEntityPath = this.container match {
    case Some(path) => path
    case None => throw new NoSuchElementException
  }

  def executeBackPropagation(neuronsValue : mutable.Map[NetworkEntityPath, Float],
                             neuronsError : mutable.Map[NetworkEntityPath, Float],
                             synapsesValue : mutable.Map[(NetworkEntityPath, NetworkEntityPath), Float],
                             forwardConnections : mutable.Map[NetworkEntityPath, mutable.ArrayBuffer[NetworkEntityPath]],
                             learningRate : Float,
                             neuronsOutputError : Map[NetworkEntityPath, Float]
                            ) = {

    backPropagationMethod.neuronsValue = neuronsValue
    backPropagationMethod.neuronsError = neuronsError
    backPropagationMethod.synapsesValue = synapsesValue
    backPropagationMethod.forwardConnections = forwardConnections
    backPropagationMethod.learningRate = learningRate

    backPropagationMethod.execute(neuronsOutputError, this)
  }


}
