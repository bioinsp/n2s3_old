package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.SetSynchronizer
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, NetworkContainerActor, NetworkEntityActor}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef, NeuronRef, SynchronizerPolicy}

/**
  * Created by guille on 9/1/16.
  */
abstract class NetworkEntityDeploymentPolicy {

  def deployNeuronGroupActor(ref: NeuronGroupRef, n2s3: N2S3)
  def deployNeuronActor(neuronRefs: NeuronRef, n2s3: N2S3)

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuron(synchronizerPolicy: SynchronizerPolicy, prefix : Traversable[Any], identifier : Any, entity : NetworkEntity, parent : Option[NetworkEntityPath], n2s3: N2S3) : NetworkEntityPath
  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuronGroupContainerActor(prefix : Traversable[Any], identifier : Any, entity : NetworkContainer, parent : Option[NetworkEntityPath], n2s3: N2S3) : Option[NetworkEntityPath]
}

class ActorPerNeuronPolicy(clusterNodeSelectionPolicy: ActorDeploymentStrategy) extends NetworkEntityDeploymentPolicy {

  def this(n2S3: N2S3) = this(n2S3.getClusterNodeSelectionPolicy)

  implicit val timeout = Config.defaultTimeout

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuron(synchronizerPolicy: SynchronizerPolicy, prefix : Traversable[Any], identifier : Any, entity : NetworkEntity, parent : Option[NetworkEntityPath], n2s3: N2S3) : NetworkEntityPath = {

    val name = prefix.foldLeft("")((acc, curr) => acc+curr.toString+"_")+identifier.toString

    val neuronPath = synchronizerPolicy.createNeuronSynchronizer(prefix++Some(identifier)) match {
      case Some(syncEntity) =>
        val layer = NetworkEntityPath(n2s3.system.actorOf(NetworkContainerActor, this.clusterNodeSelectionPolicy, name = name))
        ExternalSender.askTo(layer, AddChildEntity(0, entity))
        ExternalSender.askTo(layer, AddChildEntity(1, syncEntity))
        ExternalSender.askTo(layer/0, SetSynchronizer(layer/1))
        layer/0
      case None =>
        val neuron = NetworkEntityPath(n2s3.system.actorOf(NetworkEntityActor.newPropsBuilder().setEntity(entity), this.clusterNodeSelectionPolicy,  name = name))
        ExternalSender.askTo(neuron, SetSynchronizer(synchronizerPolicy.getNeuronSynchronizer(prefix++Some(identifier))))
        neuron
    }

    neuronPath
  }

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuronGroupContainerActor(prefix : Traversable[Any], identifier : Any, entity : NetworkContainer, parent : Option[NetworkEntityPath], n2s3: N2S3) : Option[NetworkEntityPath] = {
    None
  }

  override def deployNeuronGroupActor(ref: NeuronGroupRef, n2s3: N2S3): Unit = {
    //Do nothing
  }

  override def deployNeuronActor(neuronRef: NeuronRef, n2s3: N2S3): Unit = {
    if (neuronRef.actorPath.isEmpty) {
      val actor = n2s3.system.actorOf(NetworkContainerActor.newPropsBuilder(), this.clusterNodeSelectionPolicy, neuronRef.getIdentifier.toString)
      ExternalSender.askTo(NetworkEntityPath(actor), AddChildEntity(neuronRef.getIdentifier, neuronRef.newNeuron()))
      neuronRef.setActor(NetworkEntityPath(actor) / neuronRef.getIdentifier)
    }
  }
}

class ActorPerNeuronGroupPolicy(clusterNodeSelectionPolicy: ActorDeploymentStrategy) extends NetworkEntityDeploymentPolicy {

  def this(n2S3: N2S3) = this(n2S3.getClusterNodeSelectionPolicy)

  implicit val timeout = Config.defaultTimeout

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuron(synchronizerPolicy: SynchronizerPolicy, prefix : Traversable[Any], identifier : Any, entity : NetworkEntity, parent : Option[NetworkEntityPath], n2s3: N2S3) : NetworkEntityPath = {
    assert(parent.isDefined)

    ExternalSender.askTo(parent.get, AddChildEntity(identifier, entity))
    synchronizerPolicy.createNeuronSynchronizer(prefix++Some(identifier)) match {
      case Some(syncEntity) =>
        ExternalSender.askTo(parent.get, AddChildEntity("sync_"+identifier, syncEntity))
        ExternalSender.askTo(parent.get/identifier, SetSynchronizer(parent.get/("sync_"+identifier)))
      case None =>
        ExternalSender.askTo(parent.get/identifier, SetSynchronizer(synchronizerPolicy.getNeuronSynchronizer(prefix++Some(identifier))))
    }
    parent.get/identifier
  }

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuronGroupContainerActor(prefix : Traversable[Any], identifier : Any, entity : NetworkContainer, parent : Option[NetworkEntityPath], n2s3: N2S3) : Option[NetworkEntityPath] = {
    val actor = n2s3.system.actorOf(NetworkEntityActor.newPropsBuilder().setEntity(entity), this.clusterNodeSelectionPolicy, name = prefix.foldLeft("")((acc, curr) => acc+curr.toString+"_")+identifier.toString)
    Some(NetworkEntityPath(actor))
  }

  override def deployNeuronGroupActor(ref: NeuronGroupRef, n2s3: N2S3): Unit = {
    if (ref.actorPath.isEmpty){
      val actor = n2s3.system.actorOf(ref.newActorProps(), this.clusterNodeSelectionPolicy, name = ref.identifier)
      ref.setActor(NetworkEntityPath(actor))
    }
  }

  override def deployNeuronActor(neuronRef: NeuronRef, n2s3: N2S3): Unit = {
    neuronRef.group.ask(AddChildEntity(neuronRef.getIdentifier, neuronRef.newNeuron()))
    neuronRef.setActor(neuronRef.group.actorPath.get / neuronRef.getIdentifier)
  }
}

class SingleActorPolicy(clusterNodeSelectionPolicy: ActorDeploymentStrategy) extends NetworkEntityDeploymentPolicy {

  def this(n2S3: N2S3) = this(n2S3.getClusterNodeSelectionPolicy)

  var actor : Option[NetworkEntityPath] = None

  private def getSingleActor(n2s3: N2S3): NetworkEntityPath = actor getOrElse {
    val newActor = this.createActor(n2s3)
    actor = Some(newActor)
    newActor
  }

  private def createActor(n2s3: N2S3) : NetworkEntityPath = {
    val actor = n2s3.system.actorOf(NetworkContainerActor, this.clusterNodeSelectionPolicy, name = "unique_actor")
    NetworkEntityPath(actor)
  }

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  def createNeuron(synchronizerPolicy: SynchronizerPolicy, prefix : Traversable[Any], identifier : Any, entity : NetworkEntity, parent : Option[NetworkEntityPath], n2s3: N2S3) : NetworkEntityPath =
    parent match {
      case Some(path) =>
        ExternalSender.askTo(path, AddChildEntity(identifier, entity))
        path/identifier
      case None =>
        actor = Some(this.getSingleActor(n2s3))
        ExternalSender.askTo(actor.get, AddChildEntity(identifier, entity))
        actor.get/identifier
    }

  @deprecated("Use the correct deploy methods instead", "1/9/2016")
  override def createNeuronGroupContainerActor(prefix: Traversable[Any], identifier: Any, entity: NetworkContainer, parent: Option[NetworkEntityPath], n2s3: N2S3): Option[NetworkEntityPath] = {
    ExternalSender.askTo(parent.get, AddChildEntity(identifier, new NetworkContainer))
    Some(parent.get/identifier)
  }

  override def deployNeuronGroupActor(ref: NeuronGroupRef, n2s3: N2S3): Unit = {
    ref.setActor(getSingleActor(n2s3))
  }


  override def deployNeuronActor(neuronRef: NeuronRef, n2s3: N2S3): Unit = {
    neuronRef.group.ask(AddChildEntity(neuronRef.getIdentifier, neuronRef.newNeuron()))
    neuronRef.setActor(neuronRef.group.actorPath.get / neuronRef.getIdentifier)
  }
}
