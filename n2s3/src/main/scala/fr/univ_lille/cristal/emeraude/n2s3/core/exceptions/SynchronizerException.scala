package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

/**
 * Exceptions related to the Synchronizer class
 * @author wgouzer & qbailleul
 */
class SynchronizerException extends Exception

/**
 * Exception thrown when a synchronizer receives an unknown message
 * @param msg the message you want to associate with the exception
 */
case class UnknownMessageSynchronizerException(msg: String) extends NeuronException
