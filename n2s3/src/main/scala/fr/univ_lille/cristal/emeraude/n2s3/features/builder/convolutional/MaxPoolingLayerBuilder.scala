package fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional

import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.MaxPoolingConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{BuildProperties, NeuronLayer}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.PoolingBackPropagationMethod

/**
  * Created by falezp on 27/05/16.
  */
class MaxPoolingLayerBuilder extends LayerBuilder {

  var reduceFactor = Seq[Int]()

  def setReduceFactor(n : Int*) : this.type = {
    this.reduceFactor = n
    this
  }

  override def before(properties : BuildProperties, previousLayer : Option[NeuronLayer]) : Unit = {
    assert(previousLayer.nonEmpty)
    if(reduceFactor.isEmpty)
      reduceFactor = previousLayer.get.shape.dimensions.map(_ => 1)
    else
      assert(reduceFactor.size == previousLayer.get.shape.dimensionNumber)

    setDefaultLayerConnectionPolicy(new MaxPoolingConnection(reduceFactor:_*))

    setShape(Shape(previousLayer.get.shape.dimensions.zip(reduceFactor).map{ case (s, f) => s / f}:_*))

    setBackPropagationMethod(new PoolingBackPropagationMethod)
  }

}
