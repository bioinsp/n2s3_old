/**************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille.1
 *    - (add your name here if you contribute to this code).
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.actors

class ShutdownActorSystemReaper extends ActorReaper {
  // Derivations need to implement this method.  It's the
  override def allSoulsReaped(): Unit = {
    context.system.shutdown()
  }
}
