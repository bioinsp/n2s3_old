package fr.univ_lille.cristal.emeraude.n2s3.models.bio

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Inhibition, NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.{LTDUntil, LTPUntil}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._


import scala.math._

/**
  * Created by falezp on 13/07/16.
  */

object LIFNeuron extends NeuronModel {
  override def createNeuron(): Neuron = new LIFNeuron()
}

class LIFNeuron extends core.Neuron {
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/

  private var fixedParameter = false
  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic
  private var membranePotential = 0f
  private var membranePotentialThreshold = 1f
  private var tRefract = 1 MilliSecond
  private var tInhibit = 10 MilliSecond
  private var tLeak = 100 MilliSecond

  var theta = 0f
  var thetaLeak = 1e7 MilliSecond
  var theta_step = 0.05f


  /********************************************************************************************************************
    * Remembered properties
    ******************************************************************************************************************/
  private var tLastTheta : Timestamp = 0
  private var tLastInhib : Timestamp = -tInhibit.timestamp-1
  private var tLastSpike : Timestamp = 0
  private var tLastFire : Timestamp = -tRefract.timestamp-1

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[Float](MembraneThresholdFloat, () => membranePotentialThreshold, membranePotentialThreshold = _)
  addProperty[Time](MembraneLeakTime, () => tLeak, tLeak = _)
  addProperty[Time](MembraneRefractoryDuration, () => tRefract, tRefract = _)

  addProperty[Boolean](FixedParameter, () => fixedParameter, b => {
    fixedParameter = b
    inputConnections.filter(_.connection.isInstanceOf[Synapse]).foreach(_.connection.asInstanceOf[Synapse].setFixed(b))
  })
  addProperty[Int](WeightPrecision, () => 0, i => {
    inputConnections.filter(_.connection.isInstanceOf[ReducedPrecisionSynapse]).foreach(_.connection.asInstanceOf[ReducedPrecisionSynapse].setPrecision(i))
  })
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)


  addConnectionProperty[Float](SynapticWeightFloat, {
    case s : FloatSynapse => Some(s.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case s : FloatSynapse => s.setWeight(value)
      case _ =>
    })
  addConnectionProperty[(Float, Time)](SynapseWeightAndDelay, {
    case s : FixedSynapse => Some((s.getWeight, s.delay))
    case _ => None
  },
    (connection,  e) => connection match {
      case s : FixedSynapse =>
        s.setWeight(e._1)
        s.delay = e._2
    }
  )

  addEvent(NeuronPotentialEvent)
  addEvent(ThetaChangeEvent)
  /********************************************************************************************************************
    * Neuron Initialization
    ******************************************************************************************************************/

  def defaultConnection = new Synapse

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/


  def processSomaMessage(timestamp : Timestamp, message : Message, fromSynapse : Option[Int], ends : NeuronEnds) : Unit = message match {
    case WeightedSpike(weight) =>

      if(timestamp-tLastFire < tRefract.timestamp || timestamp-tLastInhib < tInhibit.timestamp)
        return

      membranePotential = membranePotential*exp(-(timestamp - tLastSpike).toDouble / tLeak.timestamp.toDouble).toFloat + weight

      if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
        this.theta = this.theta * exp(-(timestamp - this.tLastSpike).toDouble / thetaLeak.timestamp.toDouble).toFloat
        this.tLastTheta = timestamp
      }

      triggerEventWith(ThetaChangeEvent, ThetaChangeResponse(timestamp, this.container.self, theta))


      if(membranePotential >= membranePotentialThreshold+theta) {

        if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
          theta += theta_step
        }

        triggerEventWith(ThetaChangeEvent, ThetaChangeResponse(timestamp, this.container.self, theta))
        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        ends.sendToAllInput(timestamp, BackwardSpike)
        ends.sendToAllOutput(timestamp, ShapelessSpike)
        tLastFire = timestamp
      }

      tLastSpike = timestamp
    case Inhibition =>
      tLastInhib = timestamp
      membranePotential = 0

    case LTPUntil(end) =>
      ends.sendToAllInput(timestamp, ApplyLTP(end))
    case LTDUntil(end) =>
      ends.sendToAllInput(timestamp, ApplyLTD(end))
  }
}
