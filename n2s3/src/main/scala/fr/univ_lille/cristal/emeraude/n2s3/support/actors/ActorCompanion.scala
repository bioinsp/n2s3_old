/**************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille1.fr
 *    - (add your name here if you contribute to this code).
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.actors

abstract class ActorCompanion {
    def newPropsBuilder(): PropsBuilder
}
