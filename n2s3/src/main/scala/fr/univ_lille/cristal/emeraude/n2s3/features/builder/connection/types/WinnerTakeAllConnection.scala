package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}

/**
  * Created by falezp on 23/05/16.
  */
class WinnerTakeAllConnection(inhibitoryConnection : () => NeuronConnection) extends ConnectionPolicy {

  override def generate(from : NeuronIterable, to : NeuronIterable) = {
    new OneToOneConnection().generate(from, to)++
      new FullConnection().generate(to, from).map(co => Connection(co.from, co.to, Some(inhibitoryConnection())))
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    new OneToOneConnection().generate(from, to)++
      new FullConnection().generate(to, from).map(co => Connection(co.from, co.to, Some(inhibitoryConnection())))
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = throw new NotImplementedError()
}
