package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronLayer

import scala.collection.mutable

/**
  * Created by falezp on 31/05/16.
  */
trait BackPropagationMethod {

  var neuronsValue = mutable.Map[NetworkEntityPath, Float]()
  var neuronsError = mutable.Map[NetworkEntityPath, Float]()
  var synapsesValue = mutable.Map[(NetworkEntityPath, NetworkEntityPath), Float]()
  var forwardConnections = mutable.Map[NetworkEntityPath, mutable.ArrayBuffer[NetworkEntityPath]]()
  var learningRate = 1.0f

  def getNeuronValue(path : NetworkEntityPath) = neuronsValue.getOrElseUpdate(path,
    ExternalSender.askTo(path, GetProperty(NeuronValue)) match {
      case PropertyValue(v: Float) => v
    }
  )

  def getNeuronError(path : NetworkEntityPath) = neuronsError(path)
  def setNeuronError(path : NetworkEntityPath, value : Float) = neuronsError += path -> value

  def getSynapseValue(from : NetworkEntityPath, to : NetworkEntityPath) = synapsesValue.getOrElseUpdate(from -> to,
    ExternalSender.askTo(to, GetConnectionProperty(SynapticWeightFloat, to)) match {
      case PropertyValue(v: Float) => v
    }
  )

  def setSynapseValue(from : NetworkEntityPath, to : NetworkEntityPath, value : Float) = synapsesValue += (from -> to) -> value

  def getConnectionsFrom(from : NetworkEntityPath) : Traversable[NetworkEntityPath] = forwardConnections.getOrElse(from, None)
  def addConnection(from : NetworkEntityPath, to : NetworkEntityPath) : Unit = {
    forwardConnections.getOrElseUpdate(from, mutable.ArrayBuffer[NetworkEntityPath]()) += to
  }

  def execute(neuronsError : Map[NetworkEntityPath, Float], layer : NeuronLayer) : Unit

}

class DefaultBackPropagationMethod extends BackPropagationMethod {

  def execute(neuronsError : Map[NetworkEntityPath, Float], layer : NeuronLayer) : Unit = {

    neuronsError.foreach{ case (neuron, outputError) =>

        val currentNeuronValue = getNeuronValue(neuron)

        val dev_net = currentNeuronValue * (1.0f - currentNeuronValue)
        setNeuronError(neuron, outputError * dev_net)

        val current_synapses = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
          case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values
        }

        current_synapses.foreach { case (from, value) =>
          setSynapseValue(from, neuron, value)
          addConnection(from, neuron)
        }

        ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapticWeightFloat, current_synapses.map { case (path, value) =>
          val dev_w = getNeuronValue(path)
          val prime = outputError * dev_net * dev_w
          (path, value - learningRate * prime)
        }))
    }
  }
}

class ConvolutionBackPropagationMethod extends BackPropagationMethod {

  def execute(neuronsError : Map[NetworkEntityPath, Float], layer : NeuronLayer) : Unit = {

    assert(layer.shape.dimensionNumber == 3)

    val backwardConnections = neuronsError.map { case (neuron, _) =>
      (neuron, (ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values
      }).map { case (from, value) =>
        setSynapseValue(from, neuron, value)
        addConnection(from, neuron)
        from
      })
    }

    assert(backwardConnections.nonEmpty && backwardConnections.forall(_._2.size == backwardConnections.head._2.size))
    for (layer_index <- 0 until  layer.shape(0)) {
      for(weight_index <- backwardConnections.head._2.indices) {
        val delta = (for {
          x <- 0 until layer.shape(1)
          y <- 0 until layer.shape(2)
        } yield {
          val out_neuron = layer(layer_index, x, y)
          val dev_out = neuronsError(out_neuron)
          val dev_net = getNeuronValue(out_neuron) * (1.0f - getNeuronValue(out_neuron))
          setNeuronError(out_neuron, dev_out * dev_net)
          val dev_w = getNeuronValue(backwardConnections(out_neuron)(weight_index))
          - learningRate * dev_out * dev_net * dev_w
        }).sum
        val out_neuron = layer(layer_index, 0, 0)
        val in_neuron = backwardConnections(out_neuron)(weight_index)
        ExternalSender.askTo(out_neuron, SetConnectionProperty(SynapticWeightFloat, in_neuron, getSynapseValue(in_neuron, out_neuron)+delta))
      }
    }
  }
}

class PoolingBackPropagationMethod extends BackPropagationMethod {

  def execute(neuronsError : Map[NetworkEntityPath, Float], layer : NeuronLayer) : Unit = {
    neuronsError.foreach{ case (neuron, outputError) =>

      setNeuronError(neuron, outputError)
      val current_synapses = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
        case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values
      }

      current_synapses.foreach { case (from, value) =>
        setSynapseValue(from, neuron, 1.0f)
        addConnection(from, neuron)
      }
    }
  }
}