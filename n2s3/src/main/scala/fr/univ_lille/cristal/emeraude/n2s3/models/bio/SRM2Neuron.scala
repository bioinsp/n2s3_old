package fr.univ_lille.cristal.emeraude.n2s3.models.bio

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronEnds, NeuronFireEvent, NeuronFireResponse, NeuronMessage}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneThresholdFloat, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

import scala.collection.mutable

/**
  * Created by falezp on 19/10/16.
  */

object TauMembrane extends Property[Time]
object TauSynapse extends Property[Time]

case class ForecastFire(id : Int) extends Message

object SynapseWeightAndDelay extends ConnectionProperty[(Float, Time)]

object SRM2Neuron extends NeuronModel {
  override def createNeuron(): Neuron = new SRM2Neuron()
}

class SRM2Neuron extends core.Neuron {

  var tau_m = 50 MilliSecond
  var tau_s = 0.2 MilliSecond
  var threshold = 1.0

  var fire_id = 0

  private val spikeHistory = mutable.HashMap[Int, (Timestamp, Float)]()

  addProperty[Time](TauMembrane, () => tau_m, tau_m = _)
  addProperty[Time](TauSynapse, () => tau_s, tau_s = _)
  addProperty[Float](MembraneThresholdFloat, () => threshold.toFloat, threshold = _)

  addConnectionProperty[Float](SynapticWeightFloat, {
    case s : FloatSynapse => Some(s.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case s : FloatSynapse => s.setWeight(value)
      case _ =>
    })

  addConnectionProperty[(Float, Time)](SynapseWeightAndDelay, {
    case s : FixedSynapse => Some((s.getWeight, s.delay))
    case _ => None
  },
    (connection,  e) => connection match {
      case s : FixedSynapse =>
        s.setWeight(e._1)
        s.delay = e._2
    }
  )

  /**
    * Allow to use a default connection object in case of no explicit constructor is given on the creation of a new connection
    *
    * @return a neuron connection
    */
  override def defaultConnection: NeuronConnection = new Synapse

  /**
    * Method used to describe the behavior of the neuron model
    *
    * @param timestamp is the current time
    * @param message   is the content
    */
  override def processSomaMessage(timestamp: Timestamp, message: Message, fromSynapse: Option[Int], ends: NeuronEnds): Unit = message match {
    case WeightedSpike(weight) =>
/*
      val spikeResponse : Long => Double = { delta =>
        if(delta <= 0.0)
        0.0
        else
        math.exp(-delta.toDouble/tau_m.timestamp.toDouble)-math.exp(-delta.toDouble/tau_s.timestamp.toDouble)
      }*/

      val tau = 7 MilliSecond

      val spikeResponse : Long => Double = { delta =>
        if(delta <= 0.0)
        0.0
        else
        (delta.toDouble/tau.timestamp.toDouble)*math.exp(1.0-delta.toDouble/tau.timestamp.toDouble)
      }

      if(fromSynapse.isDefined) {
        if(spikeHistory.contains(fromSynapse.get))
          spikeHistory(fromSynapse.get) = timestamp -> weight
        else
          spikeHistory += fromSynapse.get -> (timestamp, weight)
      }
      else {
        println("Warning : unknown synapse in neuron"+getNetworkAddress)
      }

      val membrane = spikeHistory.foldLeft(0.0){ case(acc, (id, (t, w))) =>
        acc+w*spikeResponse(timestamp-t)
      }

      if(membrane >= threshold) {
        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        ends.sendToAllOutput(timestamp, ShapelessSpike)
        spikeHistory.clear()
        fire_id += 1
      }
      else {
        // futur fire ?
        var d: Timestamp = 0
        var ok = false
        while (!ok && d <= (1.1 MilliSecond).timestamp) {
          val t2 = timestamp + d
          val membraneAtMax = spikeHistory.foldLeft(0.0) { case (acc, (id, (t, w))) =>
            acc + w * spikeResponse(t2 - t)
          } // tmp

          if (membraneAtMax >= threshold) {
            ok = true
            this.thisToSyncRef.send(NeuronMessage(t2, ForecastFire(fire_id), thisToSyncRef, syncToThisRef, None))
          }
          d += (0.05 MilliSecond).timestamp
        }
      }

    case ForecastFire(id) =>
        if(id == fire_id) {
          triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
          ends.sendToAllOutput(timestamp, ShapelessSpike)
          spikeHistory.clear()
          fire_id += 1
        }

  }
}
