package fr.univ_lille.cristal.emeraude.n2s3.models.ternary_synapse

/**
  * Created by falezp on 30/06/16.
  */

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Inhibition, NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties._
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.{LTDUntil, LTPUntil}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.{NeuronPotentialEvent, NeuronPotentialResponse, QBGParameters}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.math._

object Neuron extends NeuronModel {
  override def createNeuron(): Neuron = new Neuron()
}

class Neuron extends core.Neuron {
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/
  private var v = 0f
  private val g = 1f
  private var c = 10 MilliSecond
  private var v_th = 1.0f
  private val v_rest = 0f
  private var tRefract = 1 MilliSecond
  private var tInhibit = 10 MilliSecond

  var theta = 0f
  var thetaLeak = 1e7 MilliSecond
  var theta_step = 0.05f
  private var tLastTheta : Timestamp = 0
  private var tLastInhib : Timestamp = -tInhibit.timestamp-1
  private var tLastFire : Timestamp = -tRefract.timestamp-1
  private var lastTimestamp : Timestamp = 0

  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/

  addProperty[Float](MembraneThresholdFloat, () => v_th, v_th = _)
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)

  addConnectionProperty[Float](SynapticWeightFloat, {
    case synapse : FloatSynapse => Some(synapse.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case synapse : FloatSynapse => synapse.setWeight(value)
      case _ =>
    })
  addProperty[Time](MembraneLeakTime, () => c, c = _)

  addProperty[Time](MembraneRefractoryDuration, () => tRefract, tRefract = _)

  addEvent(NeuronPotentialEvent)

  /********************************************************************************************************************
    * Neuron Initialization
    ******************************************************************************************************************/

  def defaultConnection = new Synapse

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/

  def processSomaMessage(timestamp : Timestamp, message : Message, fromSynapse : Option[Int], ends : NeuronEnds) : Unit = message match {
    case WeightedSpike(weight) =>
      if(timestamp-tLastFire < tRefract.timestamp || timestamp-tLastInhib < tInhibit.timestamp)
        return

      //println(this.getNetworkAddress+" "+timestamp+" "+weight+" "+fromSynapse+(if(fromSynapse.isDefined) "="+inputConnections(fromSynapse.get).path else ""))

      v = (if(c.timestamp != 0L) v*math.exp(-g*(timestamp-lastTimestamp)/c.timestamp).toFloat else if(timestamp>lastTimestamp) 0f else v)+weight/g+v_rest
      triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, v))

      if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
        this.theta = this.theta * exp(-(timestamp - this.tLastTheta).toDouble / QBGParameters.thetaLeak.timestamp.toDouble).toFloat
        this.tLastTheta = timestamp
      }

      lastTimestamp = timestamp


      if(v >= v_th) {

        if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
          this.theta += this.theta_step
        }

        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        v = v_rest
        tLastFire = timestamp
        ends.sendToAllOutput(timestamp, ShapelessSpike)
        ends.sendToAllInput(timestamp, BackwardSpike)
      }
    case Inhibition =>
      v = v_rest
      tLastInhib = timestamp
    case LTPUntil(end) =>
      ends.sendToAllInput(timestamp, ApplyLTP)
    case LTDUntil(end) =>
      ends.sendToAllInput(timestamp, ApplyLTD)
  }      //s.actor ! Done
}