package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Simple Neuron Model used inside an [[fr.univ_lille.cristal.emeraude.n2s3.core.InputLayer]]
  * This neuron model forwards spikes to its connected neurons without any intermediate treatment.
  */
class InputNeuron extends Neuron {

  addConnectionProperty[Float](SynapticWeightFloat,
    _ => None,
    (connection,  value) => connection match {
      case _ =>
    }
  )

  /**
    * Cancelled method.
    * Check the superclass [[Neuron]] and sibblings to see usages.
    * @throws UnsupportedOperationException
    */
  def defaultConnection = throw new UnsupportedOperationException

  /**
    * Re-sends the received message to all output neurons.
    */
  def processSomaMessage(timestamp: Timestamp, message: Message, fromSynapse : Option[Int], ends : NeuronEnds): Unit = {
    triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
    ends.sendToAllOutput(timestamp, message)
  }

}
