package fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{ConnectionPolicy, InternalConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.features.learning.{BackPropagationMethod, DefaultBackPropagationMethod}

import scala.collection.mutable

/**
  * Created by falezp on 17/05/16.
  */
object LayerBuilder {

  var unnamedCounter = 0

  def nextUnnamedIdentifier : Any = {
    unnamedCounter += 1
    "unnamed_layer_"+unnamedCounter.toString
  }
}

@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
abstract class LayerBuilder extends NetworkBuilder {

  var flatHierarchy = false
  var shape = Shape()
  var connectionPolicy : Option[ConnectionPolicy] = None
  var layerConnectionPolicy : Option[ConnectionPolicy] = None
  var layerRootConstructor : () => NetworkContainer = defaultContainerConstructor

  val internalConnections = mutable.ArrayBuffer[InternalConnectionPolicy]()
  var backPropagationMethod : BackPropagationMethod = new DefaultBackPropagationMethod

  def createShape(prefix : Traversable[Any],  properties : BuildProperties, parent : Option[NetworkEntityPath], dimensions: Traversable[Int], n2s3: N2S3) : Seq[NetworkEntityPath] = {

    val networkEntitiesPolicy = getDefaultNetworkEntityPolicy.orElse(Some(properties.getNetworkEntitiesPolicy)).get
    val SynchronizerPolicy = properties.getSynchronizerPolicy

    def innerCreateShape(prefix : Traversable[Any], shapePrefix : Traversable[Int], currentIndex : Int, parent : Option[NetworkEntityPath], remainDimension : Traversable[Int]) : Seq[NetworkEntityPath] = {
      if(remainDimension.isEmpty)
        Seq()
      else if(remainDimension.size == 1)
        for (i <- 0 until remainDimension.head) yield {
          val path = networkEntitiesPolicy.createNeuron(SynchronizerPolicy, prefix++shapePrefix, currentIndex+i, getDefaultNeuronConstructor(), parent, n2s3)
          path
        }
      else
        (for (i <- 0 until remainDimension.head) yield {
          innerCreateShape(
            prefix, shapePrefix++Some(i),
            if(flatHierarchy) currentIndex+remainDimension.tail.product*i else 0,
            if(flatHierarchy) parent else networkEntitiesPolicy.createNeuronGroupContainerActor(prefix++shapePrefix, i, getDefaultContainerConstructor(), parent, n2s3),
            remainDimension.tail)
        }).flatten
    }


    innerCreateShape(prefix, None,  0, parent, dimensions)
  }

  def createAndConnect(prefix : Traversable[Any], properties : BuildProperties, parent : Option[NetworkEntityPath] = None, previousLayer : Option[NeuronLayer] = None, n2s3: N2S3) : NeuronLayer = {
    before(properties, previousLayer)
    val thisLayer = create(properties, prefix, parent, n2s3)
    if(previousLayer.nonEmpty) {
      connectionPolicy.orElse(layerConnectionPolicy).getOrElse(new FullConnection).create(previousLayer.get, thisLayer)
    }
    thisLayer
  }

  def create(properties : BuildProperties, prefix : Traversable[Any] = None, parent : Option[NetworkEntityPath] = None, n2s3: N2S3) : NeuronLayer = {
//    getDefaultNetworkEntityPolicy.foreach(_.initialize(properties.system))
    assert(shape.nonEmpty, "No shapeDimensions defined")

    val id = identifier.getOrElse(LayerBuilder.nextUnnamedIdentifier)
    println(s"[DEBUG] Creating $id")

    val layerRoot = getDefaultNetworkEntityPolicy.getOrElse(properties.getNetworkEntitiesPolicy).createNeuronGroupContainerActor(prefix, id, layerRootConstructor(), parent, n2s3)

    val thisLayer = new NeuronLayer(layerRoot, createShape(prefix++Some(id), properties, layerRoot, shape.dimensions, n2s3), shape)
    internalConnections.foreach(_.create(thisLayer))

    after(properties, thisLayer)
    thisLayer.backPropagationMethod = this.backPropagationMethod
    println(s"[DEBUG] Success creation of $id:$shape")
    thisLayer
  }

  def before(properties : BuildProperties, previousLayer : Option[NeuronLayer]) : Unit = {}
  def after(properties : BuildProperties, currentLayer : NeuronLayer) : Unit = {}

  def setFlatHierarchy(bool : Boolean) : this.type = {
    flatHierarchy = bool
    this
  }

  def setShape(shape : Shape) : this.type = {
    this.shape = shape
    this
  }

  def setLayerRootConstructor(constructor : () => NetworkContainer) : this.type = {
    this.layerRootConstructor = constructor
    this
  }

  def setConnectionPolicy(connectionPolicy : ConnectionPolicy) : this.type = {
    this.connectionPolicy = Some(connectionPolicy)
    this
  }

  protected def setDefaultLayerConnectionPolicy(connectionPolicy : ConnectionPolicy) : this.type = {
    this.layerConnectionPolicy = Some(connectionPolicy)
    this
  }

  def addInternalConnection(internalConnection : InternalConnectionPolicy) : this.type = {
    this.internalConnections += internalConnection
    this
  }

  def setBackPropagationMethod(method : BackPropagationMethod) : this.type = {
    this.backPropagationMethod = method
    this
  }
}