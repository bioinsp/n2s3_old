package fr.univ_lille.cristal.emeraude.n2s3.core.exceptions

import fr.univ_lille.cristal.emeraude.n2s3.support.event.Event

/**
  * Created by falezp on 06/06/16.
  */

/**
  * Exception throw when an event can't be found in a class
  *
  * @param event
  * @param subclass
  */
class EventNotFoundException(val event : Event[_], val subclass : Class[_])
  extends RuntimeException("Event \"" + event.getClass + "\" can't be found in " + subclass) {
}