package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

/**
  * Created by falezp on 27/05/16.
  */
class MaxPoolingConnection(reduceFactors : Int*) extends ConnectionPolicy {

  override def generate(from : NeuronIterable, to : NeuronIterable) : Traversable[Connection] = {
    assert(from.shape.dimensionNumber == to.shape.dimensionNumber && to.shape.dimensions.zip(reduceFactors).map(e => e._1*e._2) == from.shape)

    def innerConnection(remainDim : Shape, fromIndex : Seq[Int], toIndex : Seq[Int], remainReduceFactor : Seq[Int]) : Traversable[Connection] = {
      if(remainDim.isEmpty) {
        Some(Connection(from(fromIndex:_*), to(toIndex:_*))) // TODO all connection weight = 1.0
      }
      else {
        (0 until remainDim.head).flatMap { currentInputDim =>
          innerConnection(remainDim.tail, fromIndex ++ Some(currentInputDim), toIndex ++ Some(currentInputDim / remainReduceFactor.head), remainReduceFactor.tail)
        }
      }
    }

    innerConnection(from.shape, Nil, Nil, reduceFactors)
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    assert(from.shape.dimensionNumber == to.shape.dimensionNumber && to.shape.dimensions.zip(reduceFactors).map(e => e._1*e._2) == from.shape)

    def innerConnection(remainDim : Shape, fromIndex : Seq[Int], toIndex : Seq[Int], remainReduceFactor : Seq[Int]) : Traversable[Connection] = {
      if(remainDim.isEmpty) {
        Some(Connection(from.neuronPaths(fromIndex.head), to.neuronPaths(toIndex.head))) // TODO all connection weight = 1.0
      }
      else {
        (0 until remainDim.head).flatMap { currentInputDim =>
          innerConnection(remainDim.tail, fromIndex ++ Some(currentInputDim), toIndex ++ Some(currentInputDim / remainReduceFactor.head), remainReduceFactor.tail)
        }
      }
    }

    innerConnection(from.shape, Nil, Nil, reduceFactors)
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = throw new NotImplementedError()
}