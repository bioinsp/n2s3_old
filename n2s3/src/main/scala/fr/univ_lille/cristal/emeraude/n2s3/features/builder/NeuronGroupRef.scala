package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.SetSynchronizer
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.ConnectionPolicy
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.FixedParameter
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{NetworkEntityDeploymentPolicy, PropsBuilder}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by guille on 6/2/16.
  */
class NeuronGroupRef(n2s3: N2S3) extends NetworkEntityRef {


  var identifier: String = ""
  var defaultNeuronConstructor: () => Neuron = _
  var defaultNeuronModel: (NeuronModel, Seq[(Property[_], _)]) = _
  var connections = new ArrayBuffer[ConnectionRef]()
  var shape = Shape()
  var actorPolicy: Option[NetworkEntityDeploymentPolicy] = None
  var flatHierarchy = false

  /********************************************************************************************************************
    * Accessing
    ******************************************************************************************************************/

  def getActor = this
  def getContainer: NetworkEntityPath = this.actorPath.get
  def neurons = this.getNeuronRefs
  def neuronPaths = this.neurons.map(_.actorPath.get)
  var neuronRefs: Option[Seq[NeuronRef]] = None
  def getActorPolicy = actorPolicy.getOrElse(n2s3.getNetworkEntitiesPolicy)

  def getIdentifierOf(ref: NeuronRef): Any = this.identifier + ref.index
  def hasIdentifier = this.identifier.nonEmpty

  def getNeuronRefs = {
    if (neuronRefs.isEmpty){
      var index = 0
      neuronRefs = Some(for (
        dimensionIndex <- 0 until shape.getNumberOfPoints()
      ) yield {
        val neuron = new NeuronRef(this, index)
        index = index + 1
        neuron
      })
    }
    neuronRefs.get
  }

  def isEmpty = shape.isEmpty
  def isConnected(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = {
    this.connections.exists( c => c.connects(aNeuron, anotherNeuron))
  }

  /********************************************************************************************************************
    * Neuron Group Construction
    ******************************************************************************************************************/

  def setIdentifier(identifier: String) = {
    this.identifier = identifier
    this
  }

  def getIdentifier = this.identifier

  def setNumberOfNeurons(i: Int) = {
    this.shape = Shape(i)
    this
  }

  def setNeuronModel(modelKind: NeuronModel, properties: Seq[(Property[_], _)] = Seq()) = {
    this.defaultNeuronModel = (modelKind, properties)
    this.defaultNeuronConstructor = () => {
      val neuron: Neuron = modelKind.createNeuron()
      for ((k, v) <- properties) {
        neuron.setProperty(k, v)
      }
      neuron
    }
    this
  }

  def setActorPolicy(policy: NetworkEntityDeploymentPolicy) = {
    this.actorPolicy = Some(policy)
    this
  }

  def connectToItselfUsing(connectionType: ConnectionPolicy = new FullConnection) : ConnectionRef = {
    this.connectTo(this, connectionType)
  }

  def connectTo(layer: NeuronGroupRef, connectionType: ConnectionPolicy = new FullConnection) : ConnectionRef = {
    val ref = new ConnectionRef(this, layer, connectionType)
    this.connections += ref
    ref
  }

  def setSynchronizer(synchronizer: NetworkEntityPath): Unit = {
    this.actorPath match {
      case None =>
      case Some(theActorPath) => this.ask(SetSynchronizer(synchronizer))
    }
  }

  /********************************************************************************************************************
    * Simulation Interaction/Actions
    ******************************************************************************************************************/

  def fixNeurons() = {
    neurons.foreach( neuron => neuron.ask(SetProperty(FixedParameter, true)) )
  }

  /********************************************************************************************************************
    * Deployment
    ******************************************************************************************************************/

  def ensureActorDeployed(n2s3: N2S3): Unit = {
    if (!this.isDeployed){
      this.deployActors(n2s3)
    }
  }

  /**
    * Hook to deploy actors in a given simulation.
    * This method will be called if the [[InputNeuronGroupRef]] is not yet deployed
    *
    * @param n2s3 the current simulation object
    */
  protected def deployActors(n2s3: N2S3): Unit = {
    this.getActorPolicy.deployNeuronGroupActor(this, n2s3)
    this.getNeuronRefs.foreach{ this.getActorPolicy.deployNeuronActor(_, n2s3) }
  }

  def ensureConnectionsDeployed(n2s3: N2S3) = {
    this.connections.foreach( connection => connection.ensureDeployed(n2s3) )
  }

  def newActorProps(): PropsBuilder = NetworkEntityActor.newPropsBuilder().setEntity(new NetworkContainer)
}
