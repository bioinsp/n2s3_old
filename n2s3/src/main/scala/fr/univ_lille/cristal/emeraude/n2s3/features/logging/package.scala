package fr.univ_lille.cristal.emeraude.n2s3.features

/**
 * [STILL IN PROGRESS]
 * Add the possibility to monitor the actors (e.g. neurons, synapses...) and log their changes into
 * the console (stdout) or a directly into a log file.
 *
 * See http://doc.akka.io/docs/akka/current/scala/logging.html
 *
 * @author pboulet
 */
package object logging
