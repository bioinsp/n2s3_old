package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.{ActorRef, ActorSystem, Deploy, Props}
import akka.remote.RemoteScope

import scala.concurrent.ExecutionContextExecutor

/**
  * Created by falezp on 13/05/16.
  */
trait AbstractActorSystem[T <: ActorRef] {
  def actorOf(props: Props, selector: ActorDeploymentStrategy, name: String = "") : T

  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy) : T
  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy, name : String) : T

  def actorOf(companion : ActorCompanion, selector: ActorDeploymentStrategy) : T = this.actorOf(companion.newPropsBuilder(), selector)
  def actorOf(companion : ActorCompanion, selector: ActorDeploymentStrategy, name : String) : T = this.actorOf(companion.newPropsBuilder(), selector, name)

  def dispatcher : ExecutionContextExecutor

  protected def basicCreateActor(system: ActorSystem, props: Props, name: String) ={
    if (name.isEmpty){
      system.actorOf(props)
    } else {
      system.actorOf(props, name)
    }
  }

}

class N2S3ActorSystem(val system : ActorSystem) extends AbstractActorSystem[ActorRef] {
  def actorOf(props: Props, selector: ActorDeploymentStrategy, name: String = ""): ActorRef = {
    this.basicCreateActor(system, props, name)
  }
  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy) = this.actorOf(propsBuilder.build(), selector)
  def actorOf(propsBuilder: PropsBuilder, selector: ActorDeploymentStrategy, name : String) = this.actorOf(propsBuilder.build(), selector, name)

  def dispatcher = system.dispatcher
}
