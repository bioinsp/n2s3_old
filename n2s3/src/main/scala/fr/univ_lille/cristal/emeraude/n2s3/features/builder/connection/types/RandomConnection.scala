package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}

import scala.util.Random

/**
  * Created by falezp on 07/07/16.
  */
class RandomConnection(connectionRate : Float, connectionConstructor : () => NeuronConnection) extends ConnectionPolicy {
  override def generate(from : NeuronIterable, to : NeuronIterable) = {
    for {
      in <- from
      out <- to
      rand = Random.nextFloat()
      if in != out && rand <= connectionRate
    } yield Connection(in, out, Some(connectionConstructor()))
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    for {
      in <- from.neuronPaths
      out <- to.neuronPaths
      rand = Random.nextFloat()
      if in != out && rand <= connectionRate
    } yield Connection(in, out, Some(connectionConstructor()))
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}