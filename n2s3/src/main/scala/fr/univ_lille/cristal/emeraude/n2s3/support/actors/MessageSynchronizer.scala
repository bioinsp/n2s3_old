package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.ImplicitSenderRoutedMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath

import scala.collection.mutable
import scala.concurrent.duration.{FiniteDuration, _}
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Created by guille on 5/12/16.
  */
class MessageSynchronizer(duration: FiniteDuration = 1 second)(implicit executor: ExecutionContext) {
  var timeout = Timeout(duration)
  var futures = mutable.ArrayBuffer.empty[Future[Any]]

  this.reset()

  def reset(): Unit = {
    futures = mutable.ArrayBuffer.empty[Future[Any]]
  }

  def scheduleMessage(actor: ActorRef, message: Any): Unit = {
    this.futures += (actor ? message) (this.timeout)
  }

  def scheduleMessage(actor: ActorRef, message: ImplicitSenderRoutedMessage): Unit = {
    //message.doExpectResponse()
    this.scheduleMessage(actor, message.asInstanceOf[Any])
  }

  def scheduleMessage(destination : NetworkEntityPath, message: Message): Unit = {
    //message.doExpectResponse()
    this.scheduleMessage(destination.actor, ImplicitSenderRoutedMessage(destination.local, message.setResponseMode()))
  }

  def waitForAll() = {
    Await.result(Future.sequence(this.futures), this.timeout.duration)
    this.reset()
  }
}
