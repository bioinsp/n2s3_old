/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

class EvaluationMethod(evaluationFunction : InputCrossInfo => Option[NetworkEntityPath], labels : Seq[String], neurons : Seq[NetworkEntityPath], inputs : Seq[InputCrossInfo]) {

  val labelAssoc = Map(neurons.map { actor =>
    (actor, labels.maxBy(label => inputs.count(info => isCorrectInput(evaluationFunction, info, actor, label) )))
  }:_*)

  val scorePerNeuron = Map(neurons.map { actor =>
    (actor, inputs.count(input => isCorrectInput(evaluationFunction, input, actor, labelAssoc(actor))))
  }:_*)

  val scorePerLabel = Map(labels.map { label =>
    (label, neurons.foldLeft(0)((acc, actor) => acc+inputs.count(input => isCorrectInput(evaluationFunction, input, actor, label))))
  }:_*)

  val crossScore = labels.map { label =>
    neurons.map { actor =>
      inputs.count(input => isCorrectInput(evaluationFunction, input, actor, label))
    }
  }

  val score = inputs.count { input => evaluationFunction(input) match {
    case Some(ref) => labelAssoc(ref) == input.label
    case None => false
  }
  }

  private def isCorrectInput(f : InputCrossInfo => Option[NetworkEntityPath], input :InputCrossInfo, target : NetworkEntityPath, label : String) : Boolean = {
    input.label == label && (f(input) match {
      case Some(ref) => ref == target
      case None => false
    })
  }
}

class BenchmarkResult(val labels : Seq[String], val neurons : Seq[NetworkEntityPath], inputs : Seq[InputCrossInfo]) {

  implicit val timeout = Config.defaultTimeout

  val inputNumber = inputs.size

  val rawLabelAssoc = Map(neurons.map { actor =>
    (actor, labels.maxBy(label => inputs.filter(input => input.label == label).foldLeft(0)((acc, input) =>
      acc+input.spikeList.count(entry => entry._1 == actor))))
  }:_*)

  val evaluationByMaxSpiking = new EvaluationMethod(input => {
    val group = input.spikeList.groupBy(_._1)
    if(group.isEmpty)
      None
    else {
      val max = group.maxBy(_._2.size)._1
      if (group.exists(entry => entry._2.size == group(max).size && rawLabelAssoc(entry._1) != rawLabelAssoc(max))) // avoid egality
        None
      else
        Some(max)
    }
  }, labels, neurons, inputs)

  val evaluationByOneSpiking = new EvaluationMethod(input => {
    val group = input.spikeList.groupBy(_._1)
    if(group.isEmpty)
      None
    else {
      val max = group.maxBy(_._2.size)._1
      if (group.forall(entry => rawLabelAssoc(entry._1) == rawLabelAssoc(max)))
        Some(max)
      else
        None
    }
  }, labels, neurons, inputs)

  val evaluationByFirstSpiking = new EvaluationMethod(input => {
    val group = input.spikeList.groupBy(_._1)
    if(group.isEmpty)
      None
    else {
      val firsts = group.map { case(neuron, list) =>
        (neuron, list.minBy(_._2)._2)
      }

      val min_first = firsts.minBy(_._2)._2
      val mins = firsts.filter(_._2 == min_first)

      if (mins.size == 1)
        Some(mins.head._1)
      else
        None
    }
  }, labels, neurons, inputs)

  val labelCount = inputs.groupBy(_.label).map(entry => (entry._1, entry._2.size))

  val neuronActivity = Map(neurons.map(actor => (actor, inputs.foldLeft(0)((acc, input) => acc + input.spikeList.count(_._1 == actor)))):_*)

  val quietInputCount = inputs.count(_.spikeList.isEmpty)
  val quietLabelCount = inputs.groupBy(_.label).map(entry => (entry._1, entry._2.count(_.spikeList.isEmpty)))

  val startTimestamp : Timestamp = inputs.headOption match {
    case Some(input) => input.startTime
    case none => 0
  }

  val endTimestamp : Timestamp = inputs.lastOption match {
    case Some(input) => input.endTime
    case none => 0
  }

  //TODO get all properties ?
  val theta = Map(neurons.map(path => (path, 0f/*ExternalSender.askTo(path, GetProperty(Theta)) match { case PropertyValue(v : ElectricPotential) => v.toVolts})*/)):_*)

  val avgSynapsticWeight = Map(neurons.map(path => (path, ExternalSender.askTo(path, GetAllConnectionProperty(SynapticWeightFloat)) match { case PropertyValue(list : Seq[(NetworkEntityPath, Float)] @unchecked) => list.map(_._2).sum/list.size})):_*)
}