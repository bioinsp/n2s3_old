package fr.univ_lille.cristal.emeraude.n2s3.support.actors

/******************************************************************************************************
 * In N2S3 actors are used to implements neurons. Neurons can exchanged messages between each other.
 *****************************************************************************************************/
abstract class Message extends Serializable {
  var expectResponse = false

  def doExpectResponse() = this.expectResponse = true

  def setResponseMode() : this.type = {
    expectResponse = true
    this
  }
}
