package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Basic class for all spike message type
  */
abstract class Spike extends Message

/**
  * Spike with no extra information
  */
object ShapelessSpike extends Spike

/**
  * Spike containing a weight
  */
case class WeightedSpike(weight : Float) extends Spike
