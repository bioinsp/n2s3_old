package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import akka.actor.{ActorRef, Props}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config

import scala.concurrent.Await

/**
  * Created by falezp on 25/03/16.
  */
abstract class Benchmark[Network, Output](runNumber : Int) {

  implicit val timeout = Config.longTimeout

  class BenchmarkVariable(val name : String, val value : Any, val initialize : (Network, Any) => Unit) {
    def apply(network : Network) = initialize(network, value)
  }
  class BenchmarkVariableSet[A](val name : String, val list : Seq[A], val initialize : (Network, A) => Unit)

  var listVariable = List[BenchmarkVariableSet[_]]()
  val listResult = scala.collection.mutable.ArrayBuffer[BenchmarkSavedResult[Output]]()

  def addBenchmarkVariable[A](name : String, list : Seq[A], intialize : (Network, A) => Unit) : Unit = {
    listVariable = new BenchmarkVariableSet(name, list, intialize) :: listVariable
  }

  def initializeRun() : Network

  def outputNeurons(network : Network) : Seq[ActorRef]
  def labels(network : Network) : Seq[String]


  def processPreMonitor(network : Network) : Unit
  def processMonitor(network : Network) : Unit
  def saveSetResult(variables : Seq[BenchmarkVariable], benchmarkSetResult: BenchmarkSetResult) : Output
  def saveResult(variables : Seq[BenchmarkVariable], run : Int, benchmarkResult: BenchmarkResult) : Unit = {}

  def destroyRun(network : Network) : Unit



  def exectute() : Seq[BenchmarkSavedResult[Output]] = {

    def generateVariableCombinaison(remainVariable : Seq[BenchmarkVariableSet[_]], input : Seq[List[BenchmarkVariable]]) : Seq[List[BenchmarkVariable]] = {
      remainVariable match {
        case head :: tail => generateVariableCombinaison(tail,
          if (input.isEmpty)
            for (currentValue <- head.list)
              yield new BenchmarkVariable(head.name, currentValue, head.initialize.asInstanceOf[(Network, Any) => Unit]) :: Nil
          else
            for (currentInput <- input; currentValue <- head.list)
              yield new BenchmarkVariable(head.name, currentValue, head.initialize.asInstanceOf[(Network, Any) => Unit]) :: currentInput)
        case Nil => input
      }
    }

      generateVariableCombinaison(listVariable, Nil).foreach { entry =>

      val resultRuns = scala.collection.mutable.ListBuffer[BenchmarkResult]()

      val startTime = System.nanoTime()

      for(run <- 0 until runNumber) {
        print("### ")
        entry.foreach { entry2 =>
          print(entry2.name+"="+entry2.value.toString+" ")
        }
        println("//// Run "+(run+1)+"/"+runNumber)

        val net = initializeRun()
        entry.foreach { entry2 =>
          entry2(net)
        }

        processPreMonitor(net)
/*
        val benchmarkMonitor = net.system.actorOf(Props(new BenchmarkMonitor(
          labels(net), outputNeurons(net).map(e =>new NetworkEntityPath(e))
        )), name = "benchmark_monitor")
        benchmarkMonitor ! Subscribe(LabelChangeEvent, ExternalSender.getReference(net.input))
        Await.result(ask(benchmarkMonitor, Initialize()), timeout.duration)

        processMonitor(net)

        resultRuns += Await.result(ask(benchmarkMonitor, GetResult()), timeout.duration).asInstanceOf[BenchmarkResult]
*/
        destroyRun(net)
      }
      listResult += new BenchmarkSavedResult[Output](Map(entry.map(e => e.name -> e.value):_*), saveSetResult(entry, new BenchmarkSetResult(resultRuns)), (System.nanoTime()-startTime).toFloat/1e9f/runNumber)
    }

    listResult
  }

}

class BenchmarkSavedResult[A](val variables : Map[String, Any], val value : A, val time : Float)