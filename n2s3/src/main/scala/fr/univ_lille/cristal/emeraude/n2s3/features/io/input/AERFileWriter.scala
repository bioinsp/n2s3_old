package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 26/07/16.
  */

import java.io.File
import java.io.FileOutputStream
import java.nio.ByteBuffer

/**
  * Writes [[AEREvent]]-like information to a file.
  * @param filename
  */
class AERFileWriter(filename : String) {

  private val file = new File(filename)
  private val writer = new FileOutputStream(file, false).getChannel

  def appendEvent(aEREvent: AEREvent) : Unit = this.appendEvent(aEREvent.address, aEREvent.Timestamp)

  def appendEvent(address : Short, timestamp : Int) : Unit = {
    val buffer = ByteBuffer.allocate(6)
    buffer.putShort(address)
    buffer.putInt(timestamp)
    buffer.flip()
    writer.write(buffer)
  }

  def close() : Unit = {
    writer.close()
  }
}
