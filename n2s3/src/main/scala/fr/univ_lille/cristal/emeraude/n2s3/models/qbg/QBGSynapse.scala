/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre 08/06/16.
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.Inhibition
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

/***********************************************************************************************************
 * 																				 QBGNeuronConnection
 ***********************************************************************************************************/
class QBGNeuronConnection(stdpWindow : Time = QBGParameters.stdpWindow) extends FloatSynapse(math.max(0f, math.min(1f, QBGParameters.init_mean + Random.nextGaussian().toFloat * QBGParameters.init_std))) {
  var lastPreSpikeTimestamp : Timestamp = Long.MinValue
  var lastPostSpikeTimestamp : Timestamp = Long.MinValue

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      /*weight = if(lastPostSpikeTimestamp < timestamp - QBGParameters.stdpWindow.timestamp) {
        QBGFormula.decreaseWeight(weight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
      }
      else {
        QBGFormula.increaseWeight(weight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
      }*/
      lastPreSpikeTimestamp = timestamp
      ends.sendToOutput(timestamp, WeightedSpike(weight))
    case BackwardSpike =>
      weight = if(lastPreSpikeTimestamp < timestamp - stdpWindow.timestamp) {
          QBGFormula.decreaseWeight(weight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
        }
        else {
            QBGFormula.increaseWeight(weight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
        }
      lastPostSpikeTimestamp = timestamp
  }

  def reduce(weight : Float, number : Int) : Float = {
    math.max(0f, math.min(1f, (if(number == 0) weight else math.round(weight.toDouble*number.toDouble)/number.toDouble).toFloat))
  }

  def setPrecision(precision : Int) : Unit = {
    weight = reduce(weight, precision)
  }

}

/***********************************************************************************************************
 *  														QBGNeuronConnectionWithNegative
 ***********************************************************************************************************/
class QBGNeuronConnectionWithNegative(stdpWindow : Time = QBGParameters.stdpWindow) extends FloatSynapse(math.max(0f, math.min(1f, QBGParameters.init_mean+Random.nextGaussian().toFloat*QBGParameters.init_std))) {
  var lastPreSpikeTimestamp : Timestamp = Long.MinValue

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      lastPreSpikeTimestamp = timestamp
      ends.sendToOutput(timestamp, WeightedSpike(weight))
    case BackwardSpike =>
      weight = if(lastPreSpikeTimestamp < timestamp - stdpWindow.timestamp) {
            QBGFormula.decreaseWeightWithNeg(weight, QBGParameters.alf_m, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_m)
          }
          else {
            QBGFormula.increaseWeightWithNeg(weight, QBGParameters.alf_p, QBGParameters.g_min, QBGParameters.g_max, QBGParameters.beta_p)
          }
  }

  def reduce(weight : Float, number : Int) : Float = {
    math.max(-1f, math.min(1f, (if(number == 0) weight else math.round((weight.toDouble+1.0)*0.5*number.toDouble)/number.toDouble*2.0-1.0).toFloat))
  }

  def setPrecision(precision : Int) : Unit = {
    weight = reduce(weight, precision)
  }
}

/***********************************************************************************************************
 *  														QBGInhibitorConnection
 ***********************************************************************************************************/
class QBGInhibitorConnection extends NeuronConnection {
  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      ends.sendToOutput(timestamp, Inhibition)
    case BackwardSpike => // nothing to do
  }
}