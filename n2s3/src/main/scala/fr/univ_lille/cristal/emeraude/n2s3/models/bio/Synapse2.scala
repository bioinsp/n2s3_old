package fr.univ_lille.cristal.emeraude.n2s3.models.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse

import scala.util.Random

/**
  * Created by falezp on 16/08/16.
  */
/*class Synapse2 extends FloatSynapse(math.max(0f, math.min(1f, 0.5f+Random.nextGaussian().toFloat*0.016f))) {

  var delta = 0f
  var beta = 1f

  var fixed = false

  def setFixed(state : Boolean) = this.fixed = state

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      if(!fixed) {
        if (timestamp - t_last_post <= 7*tau_exc.timestamp && t_last_post > t_last_pre)
          weight = math.max(-1, weight - a_inh * math.exp(-(timestamp - t_last_post).toDouble / tau_inh.timestamp.toDouble)).toFloat
        t_last_pre = timestamp
      }
      ends.sendToOutput(timestamp, WeightedSpike(weight))
    case BackwardSpike =>
      if(!fixed) {
        if (timestamp - t_last_pre <= 7*tau_inh.timestamp && t_last_pre >t_last_post)
          weight = math.min(1, weight + a_exc * math.exp(-(timestamp - t_last_pre).toDouble / tau_exc.timestamp.toDouble)).toFloat
        t_last_post = timestamp
      }
  }
}*/