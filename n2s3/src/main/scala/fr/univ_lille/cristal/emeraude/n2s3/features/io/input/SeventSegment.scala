package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStream

/**
  * Created by falezp on 31/08/16.
  */
object SevenSegmentEntry {

  def apply() = new StreamEntry[SampleInput](Shape(7, 2))
}

class SevenSegmentInputStream extends InputStream[SampleInput] {


  val characters = List(
    List(1, 1, 1, 0, 1, 1, 1), // 8
    List(0, 0, 1, 0, 0, 1, 0), // 1
    List(1, 0, 1, 1, 1, 0, 1), // 2
    List(1, 0, 1, 1, 0, 1, 1), // 3
    List(0, 1, 1, 1, 0, 1, 0), // 4
    List(1, 1, 0, 1, 0, 1, 1), // 5
    List(1, 1, 0, 1, 1, 1, 1), // 6
    List(1, 0, 1, 0, 0, 1, 0), // 7
    List(1, 1, 1, 1, 1, 1, 1), // 8
    List(1, 1, 1, 1, 0, 1, 1)  // 9
  )

  val label = List("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")

  var cursor = 0

  def atEnd() = cursor >= characters.size
  def next() = {
    val seq = for {
      i <- 0 until 7
    } yield {
      if(characters(cursor)(i) == 1)
        SampleUnitInput(1f, i, 0)
      else
        SampleUnitInput(1f, i, 1)
    }
    val next = SampleInput(seq, label(cursor))
    cursor += 1
    next
  }

  def reset() = {
    cursor = 0
  }

  def shape = Shape(7, 2)

  override def toString = "SevenSegment"
}
