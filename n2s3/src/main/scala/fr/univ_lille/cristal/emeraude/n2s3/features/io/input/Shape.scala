package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

/**
  * Created by falezp on 06/07/16.
  */

case class Shape(dimensions : Int*) {

  def dimensionNumber = dimensions.size

  def isEmpty = dimensions.isEmpty
  def nonEmpty = dimensions.nonEmpty

  def tail : Shape = Shape(dimensions.tail:_*)
  def head : Int = dimensions.head

  def product : Int = dimensions.product

  def indices = dimensions.indices
  def drop(n : Int) = Shape(dimensions.drop(n):_*)

  def apply(n : Int) = dimensions(n)

  /*
   * Operator
   */
  def *(n : Int) = Shape(dimensions.map(_*n):_*)
  def /(n : Int) = Shape(dimensions.map(_/n):_*)

  def getNumberOfPoints(): Int = dimensions.product

  override def toString = "("+dimensions.mkString(", ")+")"
}
