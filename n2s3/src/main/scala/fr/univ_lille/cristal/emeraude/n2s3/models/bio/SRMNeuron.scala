package fr.univ_lille.cristal.emeraude.n2s3.models.bio

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Inhibition, NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneThresholdFloat, MembraneThresholdType, MembraneThresholdTypeEnum, SynapticWeightFloat}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.mutable
import scala.math._

/**
  * Created by falezp on 13/07/16.
  */
object BackwardSpike extends Message

object SRMNeuron extends NeuronModel {
  override def createNeuron(): Neuron = new SRMNeuron()
}

class SRMNeuron extends core.Neuron {
  /********************************************************************************************************************
    * Model Parameters
    ******************************************************************************************************************/

  private val tRefract = 1 MilliSecond

  private var T = 1.0 // Neuron threshold
  private val K1 = 2.0
  private val K2 = 4.0
  private val alpha = 0.25

  private val K = 2.0 // 1/max(exp(-x/tau_m)-exp(-x/tau_s))

  private val tau_m = 10 MilliSecond // Membrane time constant
  private val tau_s = 2.5 MilliSecond // Synaptic time constant

    var theta = 0f
    var thetaLeak = 1e7 MilliSecond
    var theta_step = 0.05f


  /********************************************************************************************************************
    * Remembered properties
    ******************************************************************************************************************/
  private var tLastTheta : Timestamp = 0
  private var tLastSpike : Timestamp = -7*tau_m.timestamp-1
  private var tLastFire : Timestamp = -7*tau_m.timestamp-1

  private var fixedParameter = false
  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic

  private val lastExcSpikes = mutable.HashMap[Int, (Timestamp, Float)]()
  private val lastInhSpikes = mutable.HashMap[Int, Timestamp]()

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/
  addProperty[Float](MembraneThresholdFloat, () => T.toFloat, T = _)
  addProperty[Boolean](FixedParameter, () => fixedParameter, b => {
    fixedParameter = b
    inputConnections.filter(_.connection.isInstanceOf[Synapse]).foreach(_.connection.asInstanceOf[Synapse].setFixed(b))
  })
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)


  addConnectionProperty[Float](SynapticWeightFloat, {
    case s : FloatSynapse => Some(s.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case s : FloatSynapse => s.setWeight(value)
      case _ =>
    })


  addEvent(NeuronPotentialEvent)
  addEvent(ThetaChangeEvent)
  /********************************************************************************************************************
    * Neuron Initialization
    ******************************************************************************************************************/

  def defaultConnection = new Synapse

  /********************************************************************************************************************
    * Neuron Behavior
    ******************************************************************************************************************/


  private def theta(delta_t : Timestamp) : Double = {
    if(delta_t >= 0)
      1.0
    else
      0.0
  }

  private def epsilon(delta_t : Timestamp) : Double = {
    if(delta_t > 7*tau_m.timestamp)
      0.0
    else
      K*(math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-math.exp(-delta_t.toDouble/tau_s.timestamp.toDouble))*theta(delta_t)
  }

  private def eta(delta_t : Timestamp) : Double = {
    if(delta_t > 7*tau_m.timestamp)
      0.0
    else
      T*(K1*math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-K2*(math.exp(-delta_t.toDouble/tau_m.timestamp.toDouble)-math.exp(-delta_t.toDouble/tau_s.timestamp.toDouble)))*theta(delta_t)
  }

  private def mu(delta_t : Timestamp) : Double = {
    if(delta_t > 7*tau_m.timestamp)
      0.0
    else
      -alpha*T*epsilon(delta_t)
  }


  def computeMembranePotential(timestamp : Timestamp) : Double = {
    eta(timestamp-tLastFire)+lastExcSpikes.foldLeft(0.0) { case (acc, (_, (t, w))) =>
      acc+w*epsilon(timestamp-t)
    }/*+lastInhSpikes.foldLeft(0.0) { case (acc, (_, t)) =>
      acc+mu(timestamp-t)
    }*/
  }


  def processSomaMessage(timestamp : Timestamp, message : Message, fromSynapse : Option[Int], ends : NeuronEnds) : Unit = message match {
    case WeightedSpike(weight) =>

      if(timestamp-tLastFire < tRefract.timestamp)
        return

      if(fromSynapse.isDefined) {
        if(lastExcSpikes.contains(fromSynapse.get))
          lastExcSpikes(fromSynapse.get) = timestamp -> weight
        else
          lastExcSpikes += fromSynapse.get -> (timestamp, weight)
      }

      val v = computeMembranePotential(timestamp)

      if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
        this.theta = this.theta * exp(-(timestamp - this.tLastSpike).toDouble / thetaLeak.timestamp.toDouble).toFloat
        this.tLastTheta = timestamp
      }
      triggerEventWith(ThetaChangeEvent, ThetaChangeResponse(timestamp, this.container.self, theta))


      if(v >= T) {

        if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
          theta += theta_step
        }

        triggerEventWith(ThetaChangeEvent, ThetaChangeResponse(timestamp, this.container.self, theta))
        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        ends.sendToAllInput(timestamp, BackwardSpike)
        ends.sendToAllOutput(timestamp, ShapelessSpike)
        tLastFire = timestamp
        lastExcSpikes.clear()
        lastInhSpikes.clear()

        triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, computeMembranePotential(timestamp).toFloat))
      }
      else
        triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, v.toFloat))
      tLastSpike = timestamp
    case Inhibition =>
      tLastSpike = timestamp

      lastExcSpikes.clear()

      if(fromSynapse.isDefined)
        lastInhSpikes += fromSynapse.get -> timestamp
  }
}
