package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{CreateNeuronConnectionWith, RemoveNeuronConnectionWith}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef, SynchronizerPolicy}

import scala.concurrent.Await

/**
  * Created by falezp on 19/05/16.
  */
case class Connection(from : NetworkEntityPath, to : NetworkEntityPath, connectionType : Option[NeuronConnection] = None)

class ConnectionSet(list : Traversable[(NetworkEntityPath, NetworkEntityPath)]) {

  def disconnect() : Unit = {
    list.foreach{ case (from, to) =>
      ExternalSender.askTo(from, RemoveNeuronConnectionWith(to))
    }
  }

  def size = list.size
}

trait AbstractConnectionPolicy {

  var defaultConnectionConstructor : Option[() => NeuronConnection] = None

  protected final def construct(connections : Traversable[Connection]) : ConnectionSet = {
    val size = connections.size
    new ConnectionSet(connections.toList.zipWithIndex.map{ case (entry, index) =>
      val from = entry.from
      val to = entry.to
      if(index%10000==0)
        println(index+"/"+size)

      ExternalSender.askTo(from, CreateNeuronConnectionWith(to, entry.connectionType.orElse(defaultConnectionConstructor.map(_()))))
      (from, to)
    })
  }

  def setDefaultConnectionConstructor(constructor : () => NeuronConnection) : this.type = {
    this.defaultConnectionConstructor = Some(constructor)
    this
  }
}

trait ConnectionPolicy extends AbstractConnectionPolicy {

  final def create(from : NeuronGroupRef, to : NeuronGroupRef) : ConnectionSet = {
    construct(generate(from, to))
  }

  final def create(from : NeuronIterable, to : NeuronIterable) : ConnectionSet = {
    construct(generate(from, to))
  }

  def generate(from : NeuronGroupRef, to : NeuronGroupRef) : Traversable[Connection]
  def generate(from : NeuronIterable, to : NeuronIterable) : Traversable[Connection] = {
    // Throw exception by default
    // This is to allow cleaning subclasses from non used behaviour and remove static dependencies
    throw new UnsupportedOperationException()
  }

 /********************************************************************************************************************
  * Testing
  ******************************************************************************************************************/

 def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean
}

@deprecated("Use [[ConnectionPolicy]]", "4/8/2016")
trait InternalConnectionPolicy extends AbstractConnectionPolicy {
  final def create(layer : NeuronGroupRef) : ConnectionSet = {
    construct(generate(layer))
  }

  final def create(layer : NeuronIterable) : ConnectionSet = {
    construct(generate(layer))
  }

  def generate(layer : NeuronGroupRef) : Traversable[Connection]
  def generate(layer : NeuronIterable) : Traversable[Connection]
}