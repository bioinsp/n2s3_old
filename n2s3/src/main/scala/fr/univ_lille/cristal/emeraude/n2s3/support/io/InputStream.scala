package fr.univ_lille.cristal.emeraude.n2s3.support.io

/**
  * Created by guille on 5/20/16.
  */
object InputStream{
  def apply[T](sequence: Seq[T]) = new SequenceInputStream(sequence)
}

abstract class InputStream[T]{

  def next(): T
  def next(n: Int): Seq[T] = for (i<- 0 until n) yield this.next()
  def reset()
  def atEnd(): Boolean

}

abstract class InputStreamDecorator[T,R](decoree: InputStream[R]) extends InputStream [T] {
  def reset() = decoree.reset()
  override def toString = decoree.toString
}

class EmptyInputStream[T] extends InputStream[T] {
  override def next() = throw new UnsupportedOperationException()
  override def atEnd(): Boolean = true
  override def reset(): Unit = {}
}

class SequenceInputStream[T](sequence: Seq[T]) extends InputStream[T] {
  var index = 0
  override def next(): T = {
    val next = sequence(index)
    index += 1
    next
  }
  override def atEnd(): Boolean = index >= sequence.size
  override def reset(): Unit = index = 0
}
