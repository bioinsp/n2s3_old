/************************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille1.fr
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import akka.actor.{ActorRef, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupObserverRef, NeuronGroupRef}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.concurrent.Await

class BenchmarkMonitorRef(neuronGroup: NeuronGroupRef) extends NeuronGroupObserverRef {

  implicit val timeout = Config.longTimeout
  var actor: Option[ActorRef] = None
  def getActor = this.actor.get

  override protected def deploy(n2S3: N2S3): Unit = {
    actor = Some(n2S3.system.actorOf(Props(new BenchmarkMonitor(neuronGroup.neuronPaths)), LocalActorDeploymentStrategy))
    ExternalSender.askTo(n2S3.inputLayerRef.get.getContainer, Subscribe(LabelChangeEvent, ExternalSender.getReference(actor.get)))
  }

  def getResult: BenchmarkResult = {
    Await.result(akka.pattern.ask(actor.get, GetResult()), timeout.duration).asInstanceOf[BenchmarkResult]
  }

  def exportToHtmlView(filename: String) = {
    new BenchmarkHTMLView(this.getResult).save(filename)
  }

  override def toString: String = {
    val result = this.getResult
    s"Score = ${result.evaluationByMaxSpiking.score} / ${result.inputNumber}"
  }

  def waitProcess() : Unit = {
    Await.result(actor.get ? Done, timeout.duration)
  }
}
