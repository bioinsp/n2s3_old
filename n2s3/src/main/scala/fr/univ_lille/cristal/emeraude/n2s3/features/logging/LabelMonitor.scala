package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await
/**
  * Created by falezp on 19/10/16.
  */
class LabelMonitor(n2s3 : N2S3, input : NetworkEntityPath) {

  implicit val timeout = Config.defaultTimeout

  object GetLabels
  object ClearLabels

  val listener = n2s3.system.actorOf(Props(new Actor {

    val list = ArrayBuffer[(String, Timestamp, Timestamp)]()

    override def receive: Receive = {
      case LabelChangeResponse(start, end, label) =>
        list += ((label, start, end))
      case Done =>
        sender ! Done
      case GetLabels =>
        sender ! list
      case ClearLabels =>
        list.clear()
        sender ! Done
    }
  }), LocalActorDeploymentStrategy)

  ExternalSender.askTo(input, Subscribe(LabelChangeEvent, ExternalSender.getReference(listener)))

  def getLabels : Seq[(String, Timestamp, Timestamp)] = {
    Await.result(listener ? GetLabels, timeout.duration).asInstanceOf[Seq[(String, Timestamp, Timestamp)]]
  }

  def clearLabels() : Unit = {
    Await.result(listener ? ClearLabels, timeout.duration)
  }
}