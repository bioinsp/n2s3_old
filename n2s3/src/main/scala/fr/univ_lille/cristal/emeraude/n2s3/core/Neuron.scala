/************************************************************************************************************
 * Contributors: 
 * 		- Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntity.AskReference
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Done, SynchronizedMessage}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.event.{Event, EventResponse, Observable, ObservableMessage}

import scala.collection.mutable
import scala.language.postfixOps

/**
  * Neuron companion object
  */
object Neuron {

  /**
    * Message send to create connection with an output neuron, designated by target.
    * If no connection is given in this message, neuron will use his default constructor to create the connection object.
    *
    * @param target is the output neuron path
    * @param connection is an optional parameter for specify the connection type
    */
  case class CreateNeuronConnectionWith(target : NetworkEntityPath, connection : Option[NeuronConnection] = None) extends Message

  /**
    * Message send to create a connection with an input neuron, designated by path.
    * If no connection is given in this message, neuron will use his default constructor to create the connection object.
    *
    * @param path is target of the connection
    * @param synchronizer is entity of output neuron which manage synchronization
    * @param connection is an optional parameter for specify the connection type
    */
  case class CreateNeuronInputConnectionWith(path: NetworkEntityPath, synchronizer : NetworkEntityPath, syncToInputRef : Option[NetworkEntityReference], connection: Option[NeuronConnection]) extends Message

  /**
    * Response to CreateNeuronInputConnectionWith message
    *
    * @param id is the getIdentifier of the new connection
    * @param synchronizer is the entity of input neuron which manage synchronization
    */
  case class ConnectionCreationResponse(id : Int, synchronizer : NetworkEntityPath) extends Message

  /**
    * Message send by to a neuron for remove connection with another output neuron
    *
    * @param target is the output neuron path
    */
  case class RemoveNeuronConnectionWith(target : NetworkEntityPath) extends Message

  case class RemoveInputRemoveNeuronConnection(connectionId : Int) extends Message

  /**
    * Message used to communicate between neurons.
    *
    * @param timestamp of the message
    * @param message is the content
    */
  class NeuronMessage(val timestamp: Timestamp, val message : Message, val preSync : NetworkEntityReference, val postSync : NetworkEntityReference, val entryConnection : Option[Int]) extends SynchronizedMessage {
    override def toString = "NeuronMessage("+timestamp+", "+message+", "+preSync+", "+postSync+", "+entryConnection+")"
  }
  object NeuronMessage {
    def apply(timestamp: Timestamp, message : Message, preSync : NetworkEntityReference, postSync : NetworkEntityReference, entryConnection : Option[Int]) = new NeuronMessage(timestamp, message, preSync, postSync, entryConnection)
  }

  case class ConnectionMessage(override val timestamp: Timestamp, override val message : Message, override val preSync : NetworkEntityReference, override val postSync : NetworkEntityReference, connectionId : Int) extends NeuronMessage(timestamp, message, preSync, postSync, None)
  /**
    * Represent an inhibition spike
    */
  object Inhibition extends Message

  /**
    * Allow to set a synchronizer to a neuron
    */
  case class SetSynchronizer(synchronizer: NetworkEntityPath) extends Message

  abstract class Connection {
    val path : NetworkEntityPath
    val preSyncRef : NetworkEntityReference
    val postSyncRef : NetworkEntityReference
  }

  /**
    * Class used to store data of an incoming connection
    *
    * @param connection is the object which manage the connection
    * @param path of the input neuron
    * @param preSyncRef is the reference of the input synchronizer according to current neuron
    * @param postSyncRef is the reference of the input neuron according input synchronizer
    */
  case class InputConnection(connection : NeuronConnection, override val path : NetworkEntityPath, override val preSyncRef : NetworkEntityReference, override val postSyncRef : NetworkEntityReference) extends Connection

  /**
    * Class use to store data of an outgoing connection
    *
    * @param connectionId is the id of the connection
    * @param path of the input neuron
    * @param preSyncRef is the reference of the output synchronizer according to current neuron
    * @param postSyncRef is the reference of the output neuron according output synchronizer
    */
  case class OutputConnection(connectionId : Int, override val path : NetworkEntityPath, override val preSyncRef : NetworkEntityReference, override val postSyncRef : NetworkEntityReference) extends Connection

  /**
    * Class used to manage communication of a neuron with input and output connected synapses
    *
    * @param neuron is the current neuron
    * @param currentTimestamp is the timestamp of the current process
    */
  class NeuronEnds(neuron : Neuron, currentTimestamp : Timestamp) {


    /**
      * Send a message at a timestamp to all incoming connections.
      * In the case of no delay, the message will be process immediately by all the input connection
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToAllInput(timestamp : Timestamp, message : Message) : Unit = {
      if(timestamp == currentTimestamp) {
        neuron.inputConnections.zipWithIndex.foreach { entry  =>
          if(entry._1 != null) {
            val ends = new ConnectionEnds(entry._1, neuron, timestamp, entry._2)
            entry._1.connection.processConnectionMessage(timestamp, message, ends)
          }
        }
      }
      else if(timestamp > currentTimestamp) {
        neuron.inputConnections.zipWithIndex.foreach { entry =>
          if (entry._1 != null) {
            neuron.thisToSyncRef.send(
              ConnectionMessage(timestamp, message, neuron.thisToSyncRef, neuron.syncToThisRef, entry._2)
            )
          }
        }
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }


    /**
      * Send a message at a timestamp to all outgoing connections.
      *
      * @param timestamp is the timestamp at which the message will be processed
      * @param message is the content which will be sent
      */
    def sendToAllOutput(timestamp : Timestamp, message : Message): Unit = {
      if(timestamp >= currentTimestamp) {
        neuron.outputConnections.zipWithIndex.foreach { case (connection, id) =>
          connection.preSyncRef.send(ConnectionMessage(timestamp, message, connection.preSyncRef, connection.postSyncRef, connection.connectionId))
        }
      }
      else
        throw new RuntimeException("can't send message with past timestamp")
    }
  }

 /********************************************************************************************************************
  * Events
  ******************************************************************************************************************/

 object NeuronFireEvent extends Event[NeuronFireResponse]
  case class NeuronFireResponse(timestamp : Timestamp, source : NetworkEntityPath) extends EventResponse
}

trait NeuronModel {
  def createNeuron(): Neuron
}

/**
  * Basic trait for all neuron models
  * This class manage the interconnections between two neurons.
  * The connection object is always stored in the output neuron.
  * All connection are managed by a synchronizer
  */
trait Neuron extends NetworkEntity with Observable with PropertyHolder with Serializable {

  import Neuron._

  addEvent(NeuronFireEvent)

  var thisToSyncRef : NetworkEntityReference = _
  var syncToThisRef : NetworkEntityReference = _
  var synchronizerPath : NetworkEntityPath = _

  /**
    * List all incoming connection from input neurons to this neuron
    */
  protected[Neuron] val inputConnections = mutable.ArrayBuffer[InputConnection]()

  /**
    * Index the input connection by path
    * the value attribute is the index in inputConnections of the associated connection
    */
  protected[Neuron] val inputConnectionsPathIndex = mutable.HashMap[NetworkEntityPath, InputConnection]()

  /**
    * List all outgoing connection from this neuron to output neurons
    */
  protected[Neuron] val outputConnections = mutable.Set[OutputConnection]()

  /**
    * Index the output connection by path
    * the value attribute is the index in outputConnections of the associated connection
    */
  protected[Neuron] val outputConnectionsPathIndex = mutable.HashMap[NetworkEntityPath, OutputConnection]()

  /**
    * Retain the next id available for aa new connection
    */
  var nextFreeId = 0

  /****************************************************************************************************
   * 																			SETUP CONNECTION
   ****************************************************************************************************/

  /**
    * Allow to use a default connection object in case of no explicit constructor is given on the creation of a new connection
 *
    * @return a neuron connection
    */
  def defaultConnection : NeuronConnection

  /**
    * generate the next id of connection
 *
    * @return a free connection id
    */
  def nextConnectionID = {
    val nextId = this.nextFreeId
    this.nextFreeId += 1
    nextId
  }

  /**
    * Create an incoming connection from sender with the specified synchronizer
    * This method will resolve the reference with the output neuron and the synchronizer.
    * It will create entry in inputConnections and inputConnectionsPathIndex attributes for the new connection
 *
    * @param sender is the input neuron path of the incoming connection
    * @param synchronizer is the synchronizer path in charge of this connection
    * @param connection is an optional parameter with the connection object. if no connection are provided, the connection returned by defaultConnection will be used
    * @return the id of the new connection
    */
  def addInputConnection(sender : NetworkEntityPath, synchronizer : NetworkEntityPath, syncToInputRef : Option[NetworkEntityReference], connection : Option[NeuronConnection]) : Int = {
    val newConnectionID = this.nextConnectionID
    val connectionObject = connection.getOrElse(defaultConnection)
    val synchronizerReference = getReferenceOf(synchronizer)
    val synchronizerToInputNeuron = syncToInputRef.getOrElse(
      synchronizerReference.ask(AskReference(sender)).asInstanceOf[NetworkEntityReference]
    )
    val connectionObj = InputConnection(
      connectionObject,
      sender,
      synchronizerReference,
      synchronizerToInputNeuron
    )

    this.inputConnections += connectionObj
    this.inputConnectionsPathIndex += sender -> connectionObj

    newConnectionID
  }

  /**
    * Create an outgoing connection to neuron with the specified synchronizer
 *
    * @param neuron is the output neuron path of the outgoing connection
    * @param synchronizer is the synchronizer path in charge of this connection
    * @param connectionId is the id of the connection stored in the output neuron
    */
  def addOutputConnection(neuron : NetworkEntityPath, synchronizer : NetworkEntityPath, connectionId : Int): Unit = {
    val syncRef = getReferenceOf(synchronizer)
    val connectionObj = OutputConnection(
      connectionId,
      neuron,
      syncRef,
      syncRef.ask(AskReference(neuron)).asInstanceOf[NetworkEntityReference]
    )
    this.outputConnections += connectionObj
    this.outputConnectionsPathIndex += neuron -> connectionObj
  }

  def removeInputConnection(connectionId : Int) : Unit = {
    val path = this.inputConnections(connectionId).path
    this.inputConnections.update(connectionId, null)
    this.inputConnectionsPathIndex.remove(path)
  }

  def removeOutputConnection(target : NetworkEntityPath) : Int = {
    this.outputConnections.retain(_.path != target)
    this.outputConnectionsPathIndex.remove(target).get.connectionId
  }

  /**
    * @return the number of incoming connections of this neuron
    */
  def getNumberOfInputConnections : Int = this.inputConnections.size

  /**
    * @return the number of outgoing connections of this neuron
    */
  def getNumberOfOutputConnections : Int = this.outputConnections.size


  def getOutputConnectionById(index: Int) = this.outputConnections.find(_.connectionId == index).get
  def getOutputConnectionByPath(path: NetworkEntityPath) = this.outputConnectionsPathIndex(path)

  def getInputConnectionById(index: Int) = this.inputConnections(index)
  def getInputConnectionByPath(path: NetworkEntityPath) = this.inputConnectionsPathIndex(path)

  /********************************************************************************************************************
    * Neuron Initialization
  	* Hook to initialize the neurons and its connected synapses.
    ******************************************************************************************************************/
    initializePropertyHolder(inputConnections, inputConnectionsPathIndex)

  /********************************************************************************************************************
    * Spike Processing
    * *****************************************************************************************************************/

  /**
    * Redirect ObservableMessage and PropertyMessage to their process method
    * Handle CreateNeuronConnectionWith and CreateNeuronInputConnectionWith messages for the connection creation
    * manage NeuronMessage by redirect content to the processSomaMessage method
 *
    * @param message is the content to be processed
    * @param sender is a reference of the sender, which can be used to send response
    */
  def receiveMessage(message : Message, sender : NetworkEntityReference) : Unit = message match {
    case m : ObservableMessage => processEventHolderMessage(m)
    case m : PropertyMessage => processPropertyMessage(m, sender)

    /********************************************************************************************************************
      * Connection messages
      ******************************************************************************************************************/

    case CreateNeuronConnectionWith(targetPath, connection) =>
      val response = getReferenceOf(targetPath).ask(
        CreateNeuronInputConnectionWith(
          getNetworkAddress,
          synchronizerPath,
          if(isLocalReference(synchronizerPath)) Some(getReferenceOf(synchronizerPath).ask(AskReference(getNetworkAddress)).asInstanceOf[NetworkEntityReference]) else None,
          connection)
      ).asInstanceOf[ConnectionCreationResponse]
      addOutputConnection(targetPath, response.synchronizer, response.id)

    case CreateNeuronInputConnectionWith(origin, synchronizer, syncToInputRef, connection) =>
      sender.answer(ConnectionCreationResponse(addInputConnection(origin, synchronizer, syncToInputRef, connection), synchronizerPath))

    case RemoveNeuronConnectionWith(target) =>
      getReferenceOf(target).ask(RemoveInputRemoveNeuronConnection(removeOutputConnection(target)))

    case RemoveInputRemoveNeuronConnection(id) =>
      removeInputConnection(id)
      sender.send(Done)

    case m : ConnectionMessage =>
      val ends = new ConnectionEnds(inputConnections(m.connectionId), this, m.timestamp, m.connectionId)
      inputConnections(m.connectionId).connection.processConnectionMessage(m.timestamp, m.message, ends)
      sender.send(Done)

    case m : NeuronMessage =>
      val ends = new NeuronEnds(this, m.timestamp)
      processSomaMessage(m.timestamp, m.message, m.entryConnection, ends)
      sender.send(Done)
    case SetSynchronizer(path) =>
      synchronizerPath = path
      thisToSyncRef = getReferenceOf(path)
      syncToThisRef = thisToSyncRef.ask(AskReference(getNetworkAddress)).asInstanceOf[NetworkEntityReference]
  }

  /**
    * Method used to describe the behavior of the neuron model
    *
    * @param timestamp is the current time
    * @param message is the content
    */
  def processSomaMessage(timestamp : Timestamp, message : Message, fromSynapse : Option[Int], ends : NeuronEnds) : Unit
}

