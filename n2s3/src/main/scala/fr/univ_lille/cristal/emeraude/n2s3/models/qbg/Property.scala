/************************************************************************************************************
 * Contributors: 
 * 		- created by pierre
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import squants.electro.ElectricPotential


/********************************************************************************************************************
  * Label property
  ******************************************************************************************************************/
object Label extends Property[String]


 /********************************************************************************************************************
  * Neuron properties
  ******************************************************************************************************************/
object Theta extends Property[ElectricPotential]
object NeuronInhibitRefractory extends Property[Time]


 /********************************************************************************************************************
  * Synapses properties 
  ******************************************************************************************************************/
// The common synapses properties (shared by all synapses)
object FixedParameter extends Property[Boolean]
object SynapseLTP extends Property[Time]
object AlphaPlus extends Property[Float]
object AlphaMinus extends Property[Float]
object BetaPlus extends Property[Float]
object BetaMinus extends Property[Float]
object WeightPrecision extends Property[Int]

