package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Next, WaitEndOfActivity}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.NeuronLayer
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.ValuesGraph
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{AbstractActorSystem, LocalActorDeploymentStrategy, PropsBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by falezp on 24/05/16.
  */
object SquareError {

  def compute(system: AbstractActorSystem[ActorRef], synchronizer : NetworkEntityPath, inLayer : NeuronLayer, outLayer : NeuronLayer, labels : Map[String, NetworkEntityPath]) : Unit = {

    object AskLabel

    implicit val timeout = Config.defaultTimeout

    val labelReceiver = system.actorOf(new PropsBuilder() {
      override def build(): Props = Props(new Actor {

        var currentLabel = ""

        override def receive: Receive = {
          case LabelChangeResponse(startTime, endTime, label) => currentLabel = label
          case AskLabel => sender ! currentLabel
        }
      })
    }, LocalActorDeploymentStrategy)

    val error_graph = new ValuesGraph

    var counter = 0
    var avg_error = 0.0
    ExternalSender.sendTo(inLayer.getContainer, Subscribe(LabelChangeEvent, ExternalSender.getReference(labelReceiver)))

    while(ExternalSender.askTo(inLayer.getContainer, AskRemainInput).asInstanceOf[Boolean]) {

      val currentNeuronValues = mutable.Map[NetworkEntityPath, Float]()

      def getNeuronValue(path : NetworkEntityPath) = currentNeuronValues.getOrElseUpdate(path,
        ExternalSender.askTo(path, GetProperty(NeuronValue)) match {
          case PropertyValue(v: Float) => v
        }
      )

      try {

        ExternalSender.askTo(synchronizer, Next)
        ExternalSender.askTo(synchronizer, WaitEndOfActivity, 10 seconds)

        val label = Await.result(labelReceiver ? AskLabel, Config.defaultTimeout.duration).asInstanceOf[String]

        val output_values = outLayer.map { neuron =>
          (neuron, ExternalSender.askTo(neuron, GetProperty(NeuronValue)) match {
            case PropertyValue(v: Float) => v
          }, if (labels(label) == neuron) 1.0f else 0.0f)
        }

        output_values.foreach { case (neuron, output, target) =>

          val current_synapses = ExternalSender.askTo(neuron, GetAllConnectionProperty(SynapticWeightFloat)) match {
            case PropertyValue(values: Seq[(NetworkEntityPath, Float)] @unchecked) => values
          }

          ExternalSender.askTo(neuron, SetAllConnectionProperty(SynapticWeightFloat, current_synapses.map { case (path, value) =>
            val prime = -(target - output) * output * (1.0f - output) * getNeuronValue(path)
            //println(prime)
            (path, value - math.exp(-counter.toDouble / 10000.0).toFloat * prime)
          }))
        }

        val total_error = output_values.foldLeft(0.0f)((acc, curr) => acc+0.5f*(curr._3-curr._2)*(curr._3-curr._2))
        avg_error += total_error
        if(counter == 0) {
          error_graph.addSerie("error", counter, total_error)
          error_graph.addSerie("avg", counter, (avg_error/(counter+1).toDouble).toFloat)
          error_graph.launch()
        }
        else {
          error_graph.refreshSerie("error", counter, total_error)
          error_graph.refreshSerie("avg", counter, (avg_error / (counter + 1).toDouble).toFloat)
        }
      }
      catch {
        case e : java.util.concurrent.TimeoutException =>
          println("[Warning] Blocked network ("+e+")")
      }

      counter += 1
    }
  }
}
