/**************************************************************************************************
 * Contributors: 
 * 		- created by wgouzer & qbailleul
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal

/*****************************************************************************************************
 * Root package, it contains a Main class and various subpackages : corenetwork is the skeleton upon
 * which the simulator is built, features is supposed to be used "as is" and model is where the user
 * will defined his models.
 ****************************************************************************************************/
package object n2s3
