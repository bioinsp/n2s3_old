package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}

/**
  * Connects two neuron groups using an explicit list of connections
  */
class ManualConnection(connections : Traversable[(Int, Int, Option[NeuronConnection])]) extends ConnectionPolicy {

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    connections.map{ case(in, out, connection) =>
      Connection(from.neurons(in).actorPath.get, to.neurons(out).actorPath.get, connection)
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = true
}