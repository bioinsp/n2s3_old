package fr.univ_lille.cristal.emeraude.n2s3.models.bio

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.Inhibition
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

/**
  * Created by falezp on 13/07/16.
  */

case class ApplyLTP(limit : Timestamp) extends Message
case class ApplyLTD(limit : Timestamp) extends Message

class Synapse(w : Float = 0.475f, var delay : Time = 0 MilliSecond) extends FloatSynapse(w) {
  var a_exc = 0.03125
  var a_inh = 0.85*a_exc

  val tau_exc = 16.8 MilliSecond
  var tau_inh = 33.7 MilliSecond

  var t_last_pre : Timestamp = -7*math.max(tau_exc.timestamp, tau_inh.timestamp)-1
  var t_last_post : Timestamp = -7*math.max(tau_exc.timestamp, tau_inh.timestamp)-1

  var fixed = false

  def setFixed(state : Boolean) = this.fixed = state

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      if(!fixed) {
        if (timestamp+delay.timestamp - t_last_post <= 7*tau_exc.timestamp && t_last_post > t_last_pre)
          weight = math.max(0, weight - a_inh * math.exp(-(timestamp+delay.timestamp - t_last_post).toDouble / tau_inh.timestamp.toDouble)).toFloat
        t_last_pre = timestamp+delay.timestamp
      }
      ends.sendToOutput(timestamp+delay.timestamp, WeightedSpike(weight))
    case BackwardSpike =>
      if(!fixed) {
        if (timestamp - t_last_pre <= 7*tau_inh.timestamp && t_last_pre >t_last_post)
          weight = math.min(1, weight + a_exc * math.exp(-(timestamp - t_last_pre).toDouble / tau_exc.timestamp.toDouble)).toFloat
        t_last_post = timestamp
      }
  }
}

class FixedSynapse(w : Float = 0.475f, var delay : Time = 0 MilliSecond) extends FloatSynapse(w) {

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike => ends.sendToOutput(timestamp+delay.timestamp, WeightedSpike(weight))
    case BackwardSpike =>
  }
}

class SupervisedSynapse(w : Float = 0.475f) extends FloatSynapse(w) {
  var a_exc = 0.03125*0.1
  var a_inh = /*0.85**/a_exc

  var limit_LTP : Timestamp = 0L
  var limit_LTD : Timestamp = 0L

  var fixed = false

  def setFixed(state : Boolean) = this.fixed = state

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      if(timestamp <= limit_LTP)
        weight = math.min(1.0, weight+a_exc).toFloat
      if(timestamp <= limit_LTD)
        weight = math.max(0.0, weight-a_inh).toFloat

      ends.sendToOutput(timestamp, WeightedSpike(weight))
    case BackwardSpike =>
    case ApplyLTP(end) =>
      limit_LTP = end
    case ApplyLTD(end) =>
      limit_LTD = end
  }
}

class ReducedPrecisionSynapse extends FloatSynapse(0.5f/*if(Random.nextBoolean()) 1f else 0f*/) {
  var a_exc = 0.03125
  var a_inh = 0.85*a_exc

  val tau_exc = 16.8 MilliSecond
  var tau_inh = 33.7 MilliSecond

  var t_last_pre : Timestamp = -7*math.max(tau_exc.timestamp, tau_inh.timestamp)-1
  var t_last_post : Timestamp = -7*math.max(tau_exc.timestamp, tau_inh.timestamp)-1

  var fixed = false

  var hidden_weight = (0.5+Random.nextGaussian()/16.0).toFloat

  val th1 = 0.25f
  val th2 = 0.75f

  var precision = 3
  weight = reduce(Random.nextFloat(), precision)

  def setFixed(state : Boolean) = this.fixed = state

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      if(!fixed) {
        if (timestamp - t_last_post <= 7*tau_exc.timestamp && t_last_post > t_last_pre)
          hidden_weight = math.max(0, weight - a_inh * math.exp(-(timestamp - t_last_post).toDouble / tau_inh.timestamp.toDouble)).toFloat

        weight = reduce(hidden_weight, precision)

        t_last_pre = timestamp
      }
      ends.sendToOutput(timestamp, WeightedSpike(weight))
    case BackwardSpike =>
      if(!fixed) {
        if (timestamp - t_last_pre <= 7*tau_inh.timestamp && t_last_pre >t_last_post)
          hidden_weight = math.min(1, weight + a_exc * math.exp(-(timestamp - t_last_pre).toDouble / tau_exc.timestamp.toDouble)).toFloat

        weight = reduce(hidden_weight, precision)

        t_last_post = timestamp
      }
  }

  def reduce(weight : Float, number : Int) : Float = {
    math.max(0f, math.min(1f, (if(number == 0) weight else math.round(weight.toDouble*number.toDouble)/number.toDouble).toFloat))
  }

  def setPrecision(precision : Int) : Unit = {
    this.precision = precision
    weight = reduce(hidden_weight, precision)
  }

}

class InhibitorConnection extends NeuronConnection {

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      ends.sendToOutput(timestamp, Inhibition)
    case BackwardSpike => // nothing to do
  }
}