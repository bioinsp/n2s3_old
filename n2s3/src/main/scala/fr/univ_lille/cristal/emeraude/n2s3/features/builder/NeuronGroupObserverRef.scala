package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import akka.actor.ActorRef

/**
  * A reference to a neuron group observer
  */
abstract class NeuronGroupObserverRef {
  var deployed: Boolean = false
  def isDeployed: Boolean = this.deployed

  def ensureActorDeployed(n2s3: N2S3): Unit = {
    if (!this.isDeployed){
      this.deploy(n2s3)
      this.deployed = true
    }
  }

  protected def deploy(n2S3: N2S3)

  def getActor: ActorRef
}
