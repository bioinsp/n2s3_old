/**************************************************************************************************
 * Contributors:
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.util.concurrent.CopyOnWriteArrayList

import com.xeiam.xchart.Chart
import com.xeiam.xchart.StyleManager.ChartType
import com.xeiam.xchart.XChartPanel
import com.xeiam.xchart.SeriesLineStyle
import com.xeiam.xchart.SeriesMarker

/**************************************************************************************************
 * Class to display a Scatter graph of Activities of Neurons (When they fire)
 * This class is using java code and XChart library
 *************************************************************************************************/
class NeuronsGraph extends Graph {

  private val time: java.util.List[Integer] = new CopyOnWriteArrayList[Integer]()

  /*************************************************************************************************
   * Index of neurons : more it is deeper, more its index is high (N0,1 : 1,
   * for example)
   ************************************************************************************************/
  private val index: java.util.List[Integer] = new CopyOnWriteArrayList[Integer]()

  chart.getStyleManager().setChartType(ChartType.Line);

  
  /*************************************************************************************************
   * Method to refresh the framework while simuling
   * @param t
   *            new timestamps
   * @param n
   *            index of active neurons
   ************************************************************************************************/
  def refreshSerie(t: Int, n: Int): Unit = {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {

      override def run() {

        addSerie(t, n)

        if (!chart.getSeriesMap.containsKey("Activity of Neurons"))
          config()

        while (t - (time get 0) > 10000) {
          time remove 0
          index remove 0
        }

        pan.updateSeries("Activity of Neurons", time, index)

      }
    })
  }

  /*************************************************************************************************
   * Add a new content to the framework
   * @param t
   *            new timestamps
   * @param n
   *            index of active neurons
   ************************************************************************************************/
  def addSerie(t: Int, n: Int) {
    time add t
    index add n
  }

  def config(): Unit = {
    val serie = chart.addSeries("Activity of Neurons", time, index)
    serie.setLineStyle(SeriesLineStyle.NONE)
  }

  /*************************************************************************************************
   * Method to launch in offLine (no simulation) It will add contents to the
   * framework (Beware to add some contents before using it) And then call the
   * super Launch()
   ************************************************************************************************/
  def launchOffline(): Unit = {
    config()
    launch()
  }

}