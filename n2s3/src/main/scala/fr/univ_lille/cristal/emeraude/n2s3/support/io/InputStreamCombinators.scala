package fr.univ_lille.cristal.emeraude.n2s3.support.io

import java.io.IOException

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * Created by guille on 5/20/16.
  */

object InputStreamCombinators{
  implicit def streamCombinator[T](stream: InputStream[T]): InputStreamCombinator[T] = {
    new InputStreamCombinator(stream)
  }
}

class InputStreamCombinator[T](stream: InputStream[T]) extends {

  /** ******************************************************************************************************************
    * Combinators
    * *****************************************************************************************************************/
  def take(readLimit: Int): TakeInputStream[T] = new TakeInputStream(readLimit, stream)

  def groupBy(groupFunction: T => Any): GroupByInputStream[T] = new GroupByInputStream[T](groupFunction, stream)

  def optionalOutput(): OptionInputStream[T] = new OptionInputStream[T](stream)

  def repeat(repetitions: Int): RepeatInputStream[T] = new RepeatInputStream(stream, repetitions)

  def shuffle() : ShuffleInputStream[T] = new ShuffleInputStream(stream)

  def doing(function: T => Unit): DoingInputStream[T] = new DoingInputStream(stream, function)

  def map[T2](function: T => T2): InputStream[T2] = new MapStream(stream, function)

  def flatMap[T2](function: (T) => Seq[T2]): InputStream[T2] = new FlatMap(stream, function)
}

class DoingInputStream[T](stream: InputStream[T], function: (T) => Unit) extends InputStreamDecorator[T, T](stream) {

  override def next(): T = {
    val next = stream.next()
    function(next)
    next
  }
  override def atEnd(): Boolean = stream.atEnd()
}

class PositionInputStream[T](val stream: InputStream[T]) extends InputStreamDecorator[T,T](stream) {
  var position = 1

  override def next() = {
    this.position += 1
    stream.next()
  }
  override def atEnd(): Boolean = stream.atEnd()
  override def reset(): Unit = this.position = 1; stream.reset()
  override def toString = " image=" + this.position + super.toString()
}

class OptionInputStream[T](val stream: InputStream[T]) extends InputStreamDecorator[Option[T],T](stream) {
  override def next(): Option[T] = if (this.atEnd()) None else Some(stream.next())
  override def atEnd(): Boolean = stream.atEnd()
  override def reset(): Unit = stream.reset()
}

class TakeInputStream[T](val numberOfEntries: Int, val stream: InputStream[T]) extends InputStreamDecorator[T,T](stream){
  var taken = 0
  def next(): T = {
    if (this.atEnd()){
      throw new IOException("Reading after limit of " + numberOfEntries)
    }
    this.taken += 1
    stream.next()
  }

  override  def reset() = {
    this.taken = 0
    stream.reset()
  }

  def atEnd() = stream.atEnd() || (this.taken >= numberOfEntries && numberOfEntries >= 0 )
}

class GroupByInputStream[T](val groupingFunction: T => Any, val stream: InputStream[T]) extends InputStreamDecorator[Seq[T],T](stream){
  var peeked: Option[T]= None
  def next(): Seq[T] = {
    var grouped = new ArrayBuffer[T]()

    if (peeked.isEmpty){
      peeked = Some(stream.next())
    }

    val groupingValue = groupingFunction(peeked.get)
    do {
      grouped += peeked.get
      peeked = if (this.atEnd()) None else Some(stream.next())
    } while(peeked.nonEmpty && groupingFunction(peeked.get) == groupingValue)
    grouped
  }

  override  def reset() = {
    this.peeked = None
    stream.reset()
  }

  override def atEnd(): Boolean = stream.atEnd()
}

class RepeatInputStream[T](val stream: InputStream[T], val repetitions: Int = 1) extends InputStreamDecorator[T,T](stream){
  var currentRepetition = 1

  override def next(): T = {
    if (stream.atEnd()){
      stream.reset()
      this.currentRepetition += 1
    }
    stream.next()
  }
  override def atEnd(): Boolean = stream.atEnd() && this.currentRepetition == repetitions

  override def reset(): Unit = {
    stream.reset()
    this.currentRepetition = 1
  }

  override def toString = " repetition=" + this.currentRepetition + "/" + repetitions  + " " + super.toString()
}

class ShuffleInputStream[T](val stream: InputStream[T]) extends InputStreamDecorator[T,T](stream){

  val bufferSize = 100
  val buffer = new scala.collection.mutable.Queue[T]()

  override def next(): T = {
    if(buffer.isEmpty) {
      while(!stream.atEnd() && buffer.size < bufferSize)
        buffer += stream.next()
      Random.shuffle(buffer)
    }

    buffer.dequeue()
  }
  override def atEnd(): Boolean = stream.atEnd() && buffer.isEmpty

  override def reset(): Unit = stream.reset()
  override def toString = stream.toString
}

class MapStream[T, T2](stream: InputStream[T], function: (T) => T2) extends InputStreamDecorator[T2, T](stream) {
  override def next(): T2 = function(stream.next())
  override def atEnd(): Boolean = stream.atEnd()
}

class FlatMap[T, T2](stream: InputStream[T], function: (T) => Seq[T2]) extends InputStreamDecorator[T2, T](stream) {
  var cachedNext: Option[T2] = None
  var innerStream: InputStream[T2] = new EmptyInputStream()

  override def reset() = {
    this.innerStream = new EmptyInputStream()
    stream.reset()
  }

  override def next(): T2 = {
    this.peek() match {
      case Some(elem) => {
        this.cachedNext = None
        elem
      }
      case None => throw new IndexOutOfBoundsException()
    }
  }

  def safelyFetchNext(): Option[T2] = {
    if(!this.innerStream.atEnd()) {
      val found = this.innerStream.next()
      Some(found)
    }else{
      if (stream.atEnd()){
        None
      }else{
        this.innerStream = InputStream(function(stream.next()))
        this.safelyFetchNext()
      }
    }
  }

  def peek() : Option[T2] = {
    this.cachedNext match {
      case Some(elem) => Some(elem)
      case None => {
        this.cachedNext = this.safelyFetchNext()
        this.cachedNext
      }
    }
  }

  override def atEnd(): Boolean = {
    this.peek().isEmpty
  }
}