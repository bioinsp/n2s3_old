package fr.univ_lille.cristal.emeraude.n2s3.support.event

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Represents events that are raised by the simulation at runtime.
  * When an event happens, the subscriber will receive a message of type [[Response]]
  */
abstract class Event[Response <: EventResponse] extends Serializable {

  /**
    * When true, all subscriptions to this event will be removed after the event is risen
    */
  def isSingleUsage : Boolean = false

  /**
    * Returns a default message to use when this event happens.
    *
    * @throws UnsupportedOperationException by default if subclasses don't override this method
    */
  def defaultResponse : Response = throw new UnsupportedOperationException
}

abstract class EventResponse extends Message

abstract class TimedEvent[Response <: TimedEventResponse] extends Event[Response]

abstract class TimedEventResponse extends EventResponse {
  val timestamp : Timestamp
}