/************************************************************************************************************
 * Contributors: 
 * 		- created by guillermo.polito@univ-lille1.fr
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Inhibition, NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{NetworkEntityActor, ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.{MembraneThresholdTypeEnum, _}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.ElectricPotential
import squants.electro.ElectricPotentialConversions.ElectricPotentialConversions

import scala.math._

object QBGParameters {
  var thetaLeak = 1e7 MilliSecond
  var theta_step = 0.05 V
  var R = 1
  var v = 1


  var g_max = 1.0f
  var g_min = 0.0001f
  var alf_p = 0.01f
  var alf_m = 0.005f
  var beta_p = 1.5f
  var beta_m = 2.5f

  // Weight initialize
  var init_mean = 0.5f
  var init_std = 0.25f

  var stdpWindow = 50 MilliSecond
}

object BackwardSpike extends Message

object QBGNeuron extends NeuronModel {
  override def createNeuron(): Neuron = new QBGNeuron()
}

class QBGNeuron extends Neuron {
  /********************************************************************************************************************
  * Model Parameters
  ******************************************************************************************************************/
  private var fixedParameter = false
  private var membraneThresholdType = MembraneThresholdTypeEnum.Dynamic
  private var theta = 0 V
  private var membranePotential = 0 V
  private var membranePotentialThreshold = 1 V
  private var tRefract = 1 MilliSecond
  private var tInhibit = 10 MilliSecond
  private var tLeak = 100 MilliSecond

 /********************************************************************************************************************
  * Remembered properties
  ******************************************************************************************************************/
  private var tLastTheta : Timestamp = 0
  private var tLastInhib : Timestamp = -tRefract.timestamp
  private var tLastSpike : Timestamp = 0
  private var tLastFire : Timestamp = -tInhibit.timestamp

  /********************************************************************************************************************
    * Properties and Events
    ******************************************************************************************************************/

  addProperty[Boolean](FixedParameter, () => fixedParameter, fixedParameter = _)
  addProperty[MembraneThresholdTypeEnum.Value](MembraneThresholdType, () => membraneThresholdType, membraneThresholdType = _)

  addProperty[ElectricPotential](Theta, () => theta, theta = _)
  addProperty[ElectricPotential](MembraneThresholdPotential, () => membranePotentialThreshold, membranePotentialThreshold = _)
  addProperty[ElectricPotential](MembraneVoltagePotential, () => membranePotential, membranePotential = _)
  addProperty[Time](MembraneLeakTime, () => tLeak, tLeak = _)

  addProperty[Time](MembraneRefractoryDuration, () => tRefract, tRefract = _)
  addProperty[Time](NeuronInhibitRefractory, () => tInhibit, tInhibit = _)



  addConnectionProperty[Float](SynapticWeightFloat, {
    case  qbg: FloatSynapse => Some(qbg.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case  qbg: FloatSynapse => qbg.setWeight(value)
      case _ =>
    })

  addProperty[Int](WeightPrecision, () => 0, i => {
    inputConnections.foreach { c =>
      c.connection match {
        case o: QBGNeuronConnection => o.setPrecision(i)
        case o: QBGNeuronConnectionWithNegative => o.setPrecision(i)
        case _ =>
      }
    }
  })

  def beFixed() = this.fixedParameter = true

  addEvent(NeuronPotentialEvent)

 /********************************************************************************************************************
  * Neuron Initialization
  ******************************************************************************************************************/

  def defaultConnection = new QBGNeuronConnection

  /********************************************************************************************************************
  * Neuron Behavior
  ******************************************************************************************************************/

  def processSomaMessage(timestamp : Timestamp, message : Message, fromSynapse : Option[Int], ends : NeuronEnds) : Unit = message match {
    case WeightedSpike(weight) =>
      if(timestamp-tLastFire < tRefract.timestamp || timestamp-tLastInhib < tInhibit.timestamp)
        return

      membranePotential = (membranePotential.toVolts*exp(-(timestamp - tLastSpike).toDouble / tLeak.timestamp.toDouble) + QBGParameters.R * weight) V

      triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, membranePotential.toMillivolts.toFloat))

      if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
        this.theta = this.theta * exp(-(timestamp - this.tLastTheta).toDouble / QBGParameters.thetaLeak.timestamp.toDouble).toFloat
        this.tLastTheta = timestamp
      }

      if(membranePotential >= membranePotentialThreshold+theta) {
        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        membranePotential = 0 V

        triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, membranePotential.toMillivolts.toFloat))

        if(!fixedParameter) {

          if(membraneThresholdType == MembraneThresholdTypeEnum.Dynamic) {
            this.theta += QBGParameters.theta_step
          }

          ends.sendToAllInput(timestamp, BackwardSpike)
        }

        ends.sendToAllOutput(timestamp, ShapelessSpike)
        tLastFire = timestamp
      }
      tLastSpike = timestamp
    case Inhibition =>
      tLastInhib = timestamp
      membranePotential = 0 millivolts

      triggerEventWith(NeuronPotentialEvent, NeuronPotentialResponse(timestamp, this.container.self, membranePotential.toMillivolts.toFloat))
    }
}

//
// Actor specialization
//

class QBGNeuronActor extends NetworkEntityActor(new QBGNeuron)