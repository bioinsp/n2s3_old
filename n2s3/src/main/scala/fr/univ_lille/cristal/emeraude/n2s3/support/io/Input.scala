package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by guille on 5/26/16.
  */
class InputSeq[T <: Input](seq : Seq[T]) extends Seq[T] {
  override def length: Int = seq.size

  override def iterator: Iterator[T] = seq.iterator

  override def apply(idx: Int): T = seq.apply(idx)

  def transformInputSeq(f : Seq[T] => Seq[T]) : this.type = new InputSeq[T](f(seq)).asInstanceOf[this.type]
}


abstract class Input {
  def prefixTimestamp(prefix : Timestamp) : this.type
  def endTimestamp : Timestamp
}

trait DimensionalInput extends Input {
  def setIndex(index : Int*) : this.type
  def getIndex : Seq[Int]
}

case class LabelInput(label : String, start: Timestamp, end : Timestamp) extends Input {
  def prefixTimestamp(prefix : Timestamp) : this.type = {
    LabelInput(label, prefix+start, prefix+end).asInstanceOf[this.type]
  }

  def endTimestamp : Timestamp = end
}

abstract class N2S3Input extends Input

case class N2S3InputSpike(message: Message, timestamp: Timestamp, index : Seq[Int]) extends N2S3Input with DimensionalInput {
  def prefixTimestamp(prefix : Timestamp) : this.type = {
    N2S3InputSpike(message, prefix+timestamp, index).asInstanceOf[this.type]
  }

  def endTimestamp : Timestamp = timestamp

  def setIndex(newIndex : Int*) : this.type = {
    N2S3InputSpike(message, timestamp, newIndex).asInstanceOf[this.type]
  }

  def getIndex : Seq[Int] = index
}

case class N2S3InputLabel(label: String, start : Timestamp, end : Timestamp) extends N2S3Input {
  def prefixTimestamp(prefix : Timestamp) : this.type = {
    N2S3InputLabel(label, prefix+start, prefix+end).asInstanceOf[this.type]
  }

  def endTimestamp : Timestamp = start // avoid infinite
}

case class N2S3InputEnd(timestamp : Timestamp) extends N2S3Input {
  def prefixTimestamp(prefix : Timestamp) : this.type = {
    N2S3InputEnd(prefix + timestamp).asInstanceOf[this.type]
  }

  def endTimestamp : Timestamp = timestamp
}