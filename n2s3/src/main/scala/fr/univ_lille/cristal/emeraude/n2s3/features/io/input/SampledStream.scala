package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.io._
import fr.univ_lille.cristal.emeraude.n2s3.support.{InputDistribution, PoissonDistribution, Time}

import scala.util.Random

/**
  * Created by falezp on 27/06/16.
  */

case class SampleUnitInput(value : Float, index : Int*) extends DimensionalInput {
  override def setIndex(newIndex: Int*) : this.type = {
    SampleUnitInput(value, newIndex:_*).asInstanceOf[this.type]
  }
  override def getIndex: Seq[Int] = index

  override def prefixTimestamp(prefix: Timestamp): SampleUnitInput.this.type = throw new UnsupportedOperationException

  override def endTimestamp: Timestamp = 0
}

case class SampleInput(sample : Seq[SampleUnitInput], label : String) extends InputSeq[SampleUnitInput](sample) {
  override def transformInputSeq(f : Seq[SampleUnitInput] => Seq[SampleUnitInput]) : this.type = SampleInput(f(seq), label).asInstanceOf[this.type]
}

object SampleToSpikeTrainConverter {
  def apply(minimumFrequency: Float = 0f,
            maximumFrequency: Float = 10f,
            pauseDuration: Time = 1 Second,
            expositionDuration: Time = 1 Second) =
    new SampleToSpikeTrainConverter(minimumFrequency, maximumFrequency, pauseDuration ,expositionDuration)
}

class SampleToSpikeTrainConverter(val minimumFrequency: Float = 0f,
                                  val maximumFrequency: Float = 10f,
                                  val pauseDuration: Time = 1 Second,
                                  val expositionDuration: Time = 1 Second)

  extends StreamConverter[SampleInput, InputSeq[N2S3Input]] with StreamTimestampsManager {

  def dataConverter(in: SampleInput): InputSeq[N2S3Input] = {
    val distribution = new PoissonDistribution(0, expositionDuration.timestamp, minimumFrequency, maximumFrequency)
    def seq = Seq(N2S3InputLabel(in.asInstanceOf[SampleInput].label, prefix + 0, prefix + expositionDuration.timestamp + pauseDuration.timestamp))++
        in.flatMap { entry =>
      distribution.applyTo(entry.value).map { t =>
        N2S3InputSpike(ShapelessSpike, prefix + t, entry.index)
      }
    }
    setPrefix(prefix + expositionDuration.timestamp + pauseDuration.timestamp)
    new InputSeq(seq)
  }
}

object IntervalSampleToSpikeTrainConverter {
  def apply() = new IntervalSampleToSpikeTrainConverter
}

class IntervalSampleToSpikeTrainConverter extends StreamConverter[SampleInput, InputSeq[N2S3Input]] with StreamTimestampsManager {

  var intervalDuration = 1 Second
  var intervalInterDuration = 1 Second
  var intervalPerInput = 1

  var pauseDuration = 1 Second

  var distributionFunction : (Timestamp, Timestamp) => InputDistribution = (s,e) =>  new PoissonDistribution(s,e,0f,1.0/intervalDuration.asSecond)


  def dataConverter(in: SampleInput): InputSeq[N2S3Input] = {

    def seq = Seq(N2S3InputLabel(in.asInstanceOf[SampleInput].label, prefix + 0, prefix + intervalDuration.timestamp*intervalPerInput + pauseDuration.timestamp))++
      (0 until intervalPerInput).flatMap { i =>
        in.flatMap { entry =>
          val t = InputDistribution.poisson1(Random, math.max(1.0, intervalDuration.timestamp.toDouble*(1.0-math.min(0.9, entry.value))))
          //val t = math.min(intervalDuration.timestamp-1 ,math.max(0L, (intervalDuration.timestamp.toDouble*(Random.nextGaussian()/10.0+1.0-entry.value)).toLong))
          //val lambda = intervalDuration.timestamp.toDouble*(1.0-math.min(0.9, entry.value))
          //val t = (lambda+Random.nextGaussian()*(lambda/10.0)).toLong
          if(t <= intervalDuration.timestamp)
            Some(N2S3InputSpike(ShapelessSpike, prefix + i*(intervalDuration.timestamp+intervalInterDuration.timestamp) + t, entry.index))
          else
            None
          }
        }
    setPrefix(prefix + (intervalDuration.timestamp+intervalInterDuration.timestamp)*intervalPerInput + pauseDuration.timestamp)
    new InputSeq(seq)
  }

  def setIntervalDuration(duration : Time) : this.type = {
    intervalDuration = duration
    this
  }

  def setIntervalInterDuration(duration : Time) : this.type = {
    intervalInterDuration = duration
    this
  }


  def setPauseDuration(duration : Time) : this.type = {
    pauseDuration = duration
    this
  }

  def setIntervalPerInput(n : Int) : this.type = {
    intervalPerInput = n
    this
  }

}
