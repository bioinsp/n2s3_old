/************************************************************************************************************
 * Contributors: 
 * 		- created by falezp on 07/06/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.izhikevich

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection.ConnectionEnds
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

trait SynapseModel {
  def getWeight : Float
  def setWeight(value : Float) : Unit
}

/**
  * Implementation of the Izhikevich synapse model 
  * 
  * More informations can be found at the following address:
  * 	- http://www.izhikevich.org/publications/spikes.htm
  *   - http://www.izhikevich.org/publications/spikes.pdf
  */ 
class Synapse extends NeuronConnection with SynapseModel {

  var weight = 0.25f+Random.nextFloat()/2f

  def processConnectionMessage(timestamp : Timestamp, message : Message, ends : ConnectionEnds) : Unit = message match {
    case ShapelessSpike =>
      ends.sendToOutput(timestamp, WeightedSpike(weight))
  }

  def getWeight = weight
  def setWeight(value : Float) : Unit = weight = value
}