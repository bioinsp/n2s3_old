package fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional

import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkContainer, Neuron}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.NetworkEntityDeploymentPolicy

/**
  * Created by falezp on 18/05/16.
  */

@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
abstract class NetworkBuilder(var parentBuilder : NetworkBuilder = new EmptyNetworkBuilder) {
  protected var defaultNeuronConstructor : Option[() => Neuron] = None
  protected var defaultContainerConstructor : () => NetworkContainer = () => new NetworkContainer
  protected var defaultNetworkEntityPolicy : Option[NetworkEntityDeploymentPolicy] = None

  var identifier : Option[Any] = None

  def addToParent(parent : NetworkBuilder) : Unit = {
    parentBuilder = parent
  }

  def getDefaultNeuronConstructor : () => Neuron = defaultNeuronConstructor.orElse(Some(parentBuilder.getDefaultNeuronConstructor)).get

  def setDefaultNeuronConstructor(constructor : () => Neuron) : this.type = {
    this.defaultNeuronConstructor = Some(constructor)
    this
  }

  def getDefaultContainerConstructor : () => NetworkContainer = defaultContainerConstructor

  def setDefaultContainerConstructor(constructor : () => NetworkContainer) : this.type = {
    this.defaultContainerConstructor = constructor
    this
  }

  def getDefaultNetworkEntityPolicy : Option[NetworkEntityDeploymentPolicy] = defaultNetworkEntityPolicy.orElse(parentBuilder.getDefaultNetworkEntityPolicy)

  def setDefaultNetworkEntityPolicy(policy : NetworkEntityDeploymentPolicy) : this.type = {
    this.defaultNetworkEntityPolicy = Some(policy)
    this
  }

  def setIdentifier(identifier : Any) : this.type = {
    this.identifier = Some(identifier)
    this
  }
}

@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
class EmptyNetworkBuilder extends NetworkBuilder(null) {
  override def getDefaultNeuronConstructor : () => Neuron = throw new RuntimeException("No neuron constructor found")
  override def getDefaultNetworkEntityPolicy : Option[NetworkEntityDeploymentPolicy] = None
}