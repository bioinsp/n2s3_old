package fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection

/**
  * Created by falezp on 08/06/16.
  */

/**
  * Synapse which had a weight parameter
  */
abstract class WeightedSynapse[WeightType](initialWeight : WeightType) extends NeuronConnection {

  protected var weight : WeightType = initialWeight

  def getWeight : WeightType = weight
  def setWeight(weight : WeightType) = this.weight = weight
}

abstract class FloatSynapse(initialWeight : Float = 0f) extends WeightedSynapse[Float](initialWeight)
abstract class BinarySynapse(initialWeight : Boolean = false) extends WeightedSynapse[Boolean](initialWeight)