/************************************************************************************************************
 * Contributors: 
 * 		-  Pierre
 ***********************************************************************************************************/

package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor._
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.InputConnection
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import squants.electro.ElectricPotential

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer, Map, SeqLike}


abstract class PropertyMessage extends Message

/***********************************************************************************************************
 * Property are visible attribute of a model.
 * Properties can be get and set
 * 
 * There are two type of property :
 *  Basic which are unique per actor
 *  Connection which can be several in an actor
 **********************************************************************************************************/

/**********************************************************************************************************
 *  							                         MANAGE PROPERTIES
 *********************************************************************************************************/
case class GetAllProperties() extends PropertyMessage
case class PropertiesList(val content : Map[Any, Any]) extends PropertyMessage

/**********************************************************************************************************
 *  							                         NEURONS PROPERTIES
 *********************************************************************************************************/
case class SetProperty[A](val property : Property[A], val value : A) extends PropertyMessage
case class GetProperty[A](val property : Property[A]) extends PropertyMessage
case class PropertyValue[A](val value : A) extends PropertyMessage

/**********************************************************************************************************
 *  							                     CONNECTION PROPERTIES
 *********************************************************************************************************/
case class SetConnectionProperty[A](val property : ConnectionProperty[A], val input : NetworkEntityPath, val value : A) extends PropertyMessage
case class SetAllConnectionProperty[A](val property : ConnectionProperty[A], val values : Seq[(NetworkEntityPath, A)]) extends PropertyMessage
case class GetConnectionProperty[A](val property : ConnectionProperty[A], val input : NetworkEntityPath) extends PropertyMessage
case class GetAllConnectionProperty[A](val property : ConnectionProperty[A]) extends PropertyMessage


class PropertyHandler[A](val getter : () => A, val setter : A => Unit) extends Serializable {

  def setValue(value: Any): Unit ={
    setter(value.asInstanceOf[A])
  }

  def set(message : PropertyMessage) : Unit = {
    message match {
      case SetProperty(_, value) => this.setValue(value)
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }
  
  def get(message : PropertyMessage) : A = {
    getter()
  }
}

class ConnectionPropertyHandler[A](val getter : NeuronConnection => Option[A], val setter : (NeuronConnection, A) => Unit) extends Serializable {

  def set(connections : mutable.ArrayBuffer[InputConnection], connectionsAssoc : mutable.HashMap[NetworkEntityPath, InputConnection], message : PropertyMessage) : Unit = {
    message match {
      case SetConnectionProperty(_, input, value) =>
        setter(connectionsAssoc(input).connection, value.asInstanceOf[A])
      case SetAllConnectionProperty(_, values) =>
        /*values.foreach { case (connection, value) =>
          setter(connectionsAssoc(connection).connection, value.asInstanceOf[A])
        }*/
        connections.filter(_ != null).zip(values).foreach { case (connection, (_, value)) =>
          setter(connection.connection, value.asInstanceOf[A])
        }

      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }

  def get(connections : mutable.ArrayBuffer[InputConnection], connectionsAssoc : mutable.HashMap[NetworkEntityPath, InputConnection], message : PropertyMessage) : A = {
    message match {
      case GetConnectionProperty(_, input) =>  getter(connectionsAssoc(input).connection).get
      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }

  def getAll(connections : mutable.ArrayBuffer[InputConnection], connectionsAssoc : mutable.HashMap[NetworkEntityPath, InputConnection], message : PropertyMessage) : Traversable[(NetworkEntityPath, A)] = {
    message match {
      case GetAllConnectionProperty(_) =>
        connections.filter(_ != null).map(entry => (entry.path, getter(entry.connection))).filter(_._2.isDefined).map(entry => (entry._1, entry._2.get))


      case other => throw new RuntimeException("Incorrect PropertyMessage")
    }
  }
}

abstract class Property[A] extends Serializable {
  def create(getter : () => A, setter : A => Unit) : PropertyHandler[A] = {
    new PropertyHandler[A](getter, setter)
  }
}

abstract class ConnectionProperty[A] extends Serializable {
  def create(getter : NeuronConnection => Option[A], setter : (NeuronConnection, A) => Unit) : ConnectionPropertyHandler[A] = {
    new ConnectionPropertyHandler[A](getter, setter)
  }
}

/*******************************************************************************************************
 * PropertyHolder contains properties
 ******************************************************************************************************/
trait PropertyHolder {
  val properties = Map[Property[_], PropertyHandler[_]]()
  val connectionProperties = Map[ConnectionProperty[_], ConnectionPropertyHandler[_]]()

  var connections : mutable.ArrayBuffer[InputConnection] = _
  var connectionsAssoc : mutable.HashMap[NetworkEntityPath, InputConnection] = _

  def initializePropertyHolder(connections : mutable.ArrayBuffer[InputConnection], connectionsAssoc : mutable.HashMap[NetworkEntityPath, InputConnection]) : Unit = {
    this.connections = connections
    this.connectionsAssoc = connectionsAssoc
  }
  
  def addProperty[A](property : Property[A], getter : () => A, setter : A => Unit) : Unit = {
    properties += (property -> property.create(getter, setter))
  }
  
  def addConnectionProperty[A](property : ConnectionProperty[A], getter : NeuronConnection => Option[A], setter : (NeuronConnection, A) => Unit) : Unit = {
    connectionProperties += (property -> property.create(getter, setter))
  }

  def unableToFindProperty[A](property : A) = {
    //println("Warning : Unable to find property \""+property+"\"")
//    throw new RuntimeException("Unable to find property \""+property+"\"")
  }

  def setProperty(property: Property[_], value: Any): Unit = {
    properties.get(property) match {
      case Some(handler) => handler.setValue(value)
      case None => unableToFindProperty(property)
    }
  }


  def processPropertyMessage(message : PropertyMessage, sender : NetworkEntityReference) : Unit = {
    message match {
      case SetProperty(property, _) =>
        properties.get(property) match {
          case Some(handler) => handler.set(message)
          case None => unableToFindProperty(property)
        }
      case GetProperty(property) =>
        properties.get(property) match {
          case Some(handler) => sender.send(PropertyValue(handler.get(message)))
          case None => unableToFindProperty(property)
        }
      case SetConnectionProperty(property, _, _) =>
        connectionProperties.get(property) match {
          case Some(handler) => handler.set(connections, connectionsAssoc, message)
          case None => unableToFindProperty(property)
        }
      case SetAllConnectionProperty(property, _) =>
        connectionProperties.get(property) match {
          case Some(handler) => handler.set(connections, connectionsAssoc, message)
          case None => unableToFindProperty(property)
        }
      case GetConnectionProperty(property, _) =>
        connectionProperties.get(property) match {
          case Some(handler) => sender.send(PropertyValue(handler.get(connections, connectionsAssoc, message)))
          case None =>
            unableToFindProperty(property)
        }
      case GetAllConnectionProperty(property) => {
        connectionProperties.get(property) match {
          case Some(handler) => sender.send(PropertyValue(handler.getAll(connections, connectionsAssoc, message)))
          case None => unableToFindProperty(property)
        }}
      case GetAllProperties() =>
        sender.send(PropertiesList((properties++connectionProperties).map(entry => (entry._1, entry._2 match {
          case value : PropertyHandler[_] => value.getter()
          case value : ConnectionPropertyHandler[_] => value.getAll(connections, connectionsAssoc, GetAllConnectionProperty(null))
        }))))
      case other => throw new RuntimeException("Unknown message \""+message+"\"")
    } 
  }
  
}