/**************************************************************************************************
 * Contributors: 
 * 		- created by bdanglot
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/

package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import javax.swing.JFrame

import com.xeiam.xchart.Chart
import com.xeiam.xchart.StyleManager.LegendPosition
import com.xeiam.xchart.XChartPanel

/***************************************************************************************************
 * trait which all Graph will extends
 **************************************************************************************************/
trait Graph {

  /***************************************************************************************************
   * The framework
   **************************************************************************************************/
  protected val chart : Chart = new Chart(800,600)
  chart.getStyleManager().setChartTitleVisible(false)
  chart.getStyleManager().setLegendPosition(LegendPosition.OutsideE)
  
  /***************************************************************************************************
   * The panel
   **************************************************************************************************/
  protected val pan : XChartPanel = new XChartPanel(chart)

  var run : Boolean = false
  
  def isRunning() : Boolean = run
  
  /***************************************************************************************************
   * Create a JFrame and add the panel to It
   **************************************************************************************************/
  def launch() : JFrame = {
    run = true
    // Create and set up the window.
    val frame : JFrame = new JFrame("XChart")
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
    frame.add(pan)

    // Display the window.
    frame.pack()
    frame.setVisible(true)
    frame
  }
  
  
}