package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{EOFException, File}

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.ShapelessSpike
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.io._
import net.sf.jaer.aemonitor.AEPacketRaw
import net.sf.jaer.eventio.AEFileInputStream

/**
  * Created by falezp on 09/03/16.
  */
abstract class AERFormat extends StreamConverter[InputSeq[AERInput], InputSeq[N2S3Input]] {

  def transformAddress(address : Int) : Seq[Int]

  def dataConverter(in: InputSeq[AERInput]): InputSeq[N2S3Input] = {
    new InputSeq(in.map { input => N2S3InputSpike(ShapelessSpike, input.timestamp, transformAddress(input.address)) })
  }
}

object AERRetina {

  def withoutSeparateSign(shapeDimensions: Int*) = this(false, shapeDimensions: _*)
  def withSeparateSign(shapeDimensions: Int*) = this(true, shapeDimensions: _*)

  def apply(separateSign: Boolean, shapeDimensions: Int*) =
    new StreamEntry[InputSeq[AERInput]](Shape(shapeDimensions: _*)) >>
      new AERRetina(separateSign)

}

class AERRetina(separateSign : Boolean) extends AERFormat {

  def transformAddress(address : Int) : Seq[Int] = {
    val y = (shape(0)-1)-((address >> 1) & (shape(0)-1))
    val x = (shape(1)-1)-((address >> 8) & (shape(1)-1))

    val sign = address & 0x1
    if(separateSign){
      Seq(y, x, sign)
    } else {
      Seq(y, x)
    }
  }

  override def shapeConverter(shape : Shape) : Shape = {
    assert( shape.dimensionNumber == 2 )
    if (separateSign) {
      Shape(shape(0), shape(1), 2)
    } else {
      shape
    }
  }
}

object AERCochlea {
  def apply() = new AERCochlea
}

class AERCochlea extends AERFormat {

  def transformAddress(address : Int) : Seq[Int] = {
    Seq(address)
  }

  override def shapeConverter(shape : Shape) : Shape = {
    assert(shape.dimensionNumber == 1)
    shape
  }
}

/**
  * Represents the input of an AER file to send to the simulation.
  */
case class AERInput(timestamp: Timestamp, address : Int) extends Input {
  override def prefixTimestamp(prefix: Timestamp): this.type = {
    AERInput(prefix+timestamp, address).asInstanceOf[this.type]
  }

  override def endTimestamp: Timestamp = timestamp

}

/**
 * Reads AER files using jAER (jar file in lib directory).
 * All the methods return an IndexedSeq[Event]. The timestamps are
 * normalized to start at 0.
 *
 * @author boulet
 * @param filename: String, the name of the file to read from
 */
//TODO: expliquer la relation entre AERInputStream AERInput
class AERInputStream(filename: String, s : Shape, chunkSize : Int = 64) extends InputStream[InputSeq[AERInput]] with StreamTimestampsManager {

  val aeis = new AEFileInputStream(new File(filename))
  val startTime = aeis.getFirstTimestamp
  val numberImage = aeis.size().toInt
  var peekEvent: Option[Seq[AERInput]] = None
  var lastPrint = 0

  aeis.mark()

  /**
   * reset event reader
   */
  override def reset() : Unit = {
    maxPrefix()
    this.peekEvent = None
    aeis.rewind() }

  /**
   * @param p: AEPacketRaw, the packet of raw events read from the file
   * @return an IndexedSeq of Events
   */
  private def eventsFromAEPacket(p: AEPacketRaw) ={
    for (
      i <- 0 until p.getNumEvents;
      r = p.getEvent(i); // has to be done in the for loop
      e = AERInput(prefix + r.timestamp - startTime, r.address) // because getEvent always points to the same memory area
    ) yield {
      maxTimestamp = math.max(e.timestamp, maxTimestamp)
      e
    }
  }

  /**
   * @param dt, the time interval in µs to read events from the last read event
   * @return sequence of events
   */
  def readEventsByTime(dt: Int) = {
    eventsFromAEPacket(aeis.readPacketByTime(dt))
  }

  /**
   * @return Sequence of all the events in the file.
   */
  def readAllEvents() = eventsFromAEPacket(aeis.readPacketByNumber(aeis.size().toInt))

  override def next() = this.innerNext(chunkSize)

  def innerNext(n: Int): InputSeq[AERInput] = {
    if (aeis.position() >= aeis.size() && this.peekEvent.isEmpty){
      throw new EOFException()
    }

    var list = Seq[AERInput]()

    while(list.size < n && aeis.position() < aeis.size()) {
      list = list++this.basicNext(n)
    }


    if(list.size > n) {
      val split = list.splitAt(n)
      this.peekEvent = Some(split._2)
      list = split._1

    }

    if(list.last.timestamp/1000000L > lastPrint) {
      lastPrint = (list.last.timestamp/1000000L).toInt
      println("[AER] "+lastPrint+" seconds")
    }

    new InputSeq(list)
  }

  private def basicNext(n: Int) = {
    if (this.peekEvent.nonEmpty){
      val first = this.peekEvent.get
      this.peekEvent = None
      first ++ eventsFromAEPacket(aeis.readPacketByNumber(n - 1)).toList
    } else {


      try {
        eventsFromAEPacket(aeis.readPacketByNumber(math.min(n, (aeis.size() - aeis.position()).toInt)))
      } catch {
        case e : EOFException => println(aeis.position()+" "+aeis.size()+" "+this.peekEvent.isEmpty)
        Seq()
      }
    }
  }

  /**
    * return if we reach the end of the file
    */
  override def atEnd(): Boolean = aeis.position() >= aeis.size() - 1 && this.peekEvent.isEmpty

  override def toString: String = (aeis.position() + 1)  + "/" + aeis.size()
}