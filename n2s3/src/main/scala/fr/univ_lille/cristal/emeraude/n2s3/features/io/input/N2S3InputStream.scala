package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStreamCombinators._
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

abstract class StreamConverter[I <: InputSeq[_], O <: InputSeq[_]] {

  private var inputStream : InputStream[I] = _
  protected var shape = Shape()

  def setShape(shape : Shape) : Unit = {
    this.shape = shape
  }

  def setStream(inputStream : InputStream[I]) = {
    this.inputStream = inputStream
  }
  def atEnd() = inputStream.atEnd()

  def next() : O = {
    assert(inputStream != null)

    dataConverter(inputStream.next())
  }

  def reset() = inputStream.reset()

  def dataConverter(in : I) : O
  def shapeConverter(in : Shape) : Shape = in

}

object N2S3InputStreamCombinators{
  implicit def streamCombinator[T](stream: InputStream[T]): N2S3InputStreamCombinator[T, T] = {
    new N2S3InputStreamCombinator(stream)
  }
}

class N2S3InputStreamCombinator[T, T2](originalStream: InputStream[T], combinedStream: InputStream[T2]) extends InputStream[T2] {

  def this(stream: InputStream[T]) {
    this(stream, stream.asInstanceOf[InputStream[T2]])
  }

  def take(i: Int) = {
    val newCombinedStream = combinedStream.take(i)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def repeat(numberOfTimes: Int) = {
    val newCombinedStream = combinedStream.repeat(numberOfTimes)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def shuffle() = {
    val newCombinedStream = combinedStream.shuffle()
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def groupBy(function: T2 => Any) = {
    val newCombinedStream = combinedStream.groupBy(function)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def doing(function: T2 => Unit) = {
    val newCombinedStream = combinedStream.doing(function)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  def map[T3](function: T2 => T3) = {
    val newCombinedStream = combinedStream.map(function)
    new N2S3InputStreamCombinator(originalStream, newCombinedStream)
  }

  override def next(): T2 = combinedStream.next()
  override def atEnd(): Boolean = combinedStream.atEnd()
  override def reset(): Unit = combinedStream.reset()

  override def toString = combinedStream.toString
}
