package fr.univ_lille.cristal.emeraude.n2s3.core.actors

import fr.univ_lille.cristal.emeraude.n2s3.core.InputLayer
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.StreamSupport
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorCompanion, PropsBuilder, SinglePropsBuilder}
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{InputSeq, N2S3Input}

/**
  * Actor companion of [[InputLayerActor]].
  * It defines now to create such actor by defining a props builder
  */
object InputLayerActor extends ActorCompanion {

  /**
    * Returns a props builder to build an [[InputLayerActor]]
    */
  override def newPropsBuilder(): PropsBuilder = new SinglePropsBuilder[InputLayerActor]
}

/**
  * Actor for input layers.
  * This actor is built as a NetworkEntityActor using a single [[InputLayer]] as network entity.
  * @param stream the stream that will be used by the input layer to read data from
  */
class InputLayerActor(stream: StreamSupport[_, InputSeq[N2S3Input]]) extends NetworkEntityActor(new InputLayer(stream))
