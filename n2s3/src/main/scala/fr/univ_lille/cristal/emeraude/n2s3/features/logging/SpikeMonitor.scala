package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import akka.actor.{Actor, Props}
import akka.pattern.ask
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.LocalActorDeploymentStrategy
import fr.univ_lille.cristal.emeraude.n2s3.support.event.Subscribe

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Await

/**
  * Created by falezp on 19/10/16.
  */
class SpikeMonitor(n2s3 : N2S3, neurons : Seq[NetworkEntityPath]) {

  implicit val timeout = Config.defaultTimeout

  object GetSpikes
  object ClearSpikes

  val listener = n2s3.system.actorOf(Props(new Actor {

    val list = ArrayBuffer[(NetworkEntityPath, Timestamp)]()

    override def receive: Receive = {
      case NeuronFireResponse(timestamp, path) =>
        list += path -> timestamp
      case Done =>
        sender ! Done
      case GetSpikes =>
        sender ! list
      case ClearSpikes =>
        list.clear()
        sender ! Done
    }
  }), LocalActorDeploymentStrategy)

  neurons.foreach(n => ExternalSender.askTo(n, Subscribe(NeuronFireEvent, ExternalSender.getReference(listener))))

  def getSpikes : Seq[(NetworkEntityPath, Timestamp)] = {
    Await.result(listener ? GetSpikes, timeout.duration).asInstanceOf[Seq[(NetworkEntityPath, Timestamp)]]
  }

  def clearSpikes() : Unit = {
    Await.result(listener ? ClearSpikes, timeout.duration)
  }
}
