package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{ConnectionSet, ConnectionPolicy}

/**
  * Models a connection between two neuron groups.
  * @param from [[NeuronGroupRef]] origin of the connection
  * @param to [[NeuronGroupRef]]  destination of the connection
  * @param connectionType the kind of connection used
  */
class ConnectionRef(from: NeuronGroupRef, to: NeuronGroupRef, connectionType: ConnectionPolicy) {

  var deployed = false
  def isDeployed = this.deployed
  var connections : ConnectionSet = _

  def ensureDeployed(n2s3: N2S3) = {
    if (!this.isDeployed){
      deployed = true
      this.deploy(n2s3)
    }
  }

  def deploy(n2s3: N2S3) = {
    connections = connectionType.create(from, to)
  }

  def disconnect() : Unit = {
    assert(connections != null, "connection already disconnected")
    connections.disconnect()
    connections = null
  }

  def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = {
    (aNeuron.group == from) & (anotherNeuron.group == to) & connectionType.connects(aNeuron, anotherNeuron)
  }
}