package fr.univ_lille.cristal.emeraude.n2s3.core.models.properties

import fr.univ_lille.cristal.emeraude.n2s3.core.Property
import squants.electro.ElectricPotential

/**
  * Created by falezp on 25/10/16.
  */

/**
  * Property used to represent membrane voltage of a neuron
  * @tparam T is the voltage value type
  */
class MembraneVoltage[T] extends Property[T]

//
//  Type specializations
//
object MembraneVoltageFloat extends MembraneVoltage[Float]

object MembraneVoltagePotential extends MembraneVoltage[ElectricPotential]
