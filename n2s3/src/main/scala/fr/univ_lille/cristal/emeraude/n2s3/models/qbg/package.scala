/************************************************************************************************************
 * Contributors: 
 * 		- created by wgouzer & qbailleul
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models

/********************************************************************************************************
 * Simple model that increase when we receive a post-synaptic spike and decrease when we
 * receive a pre-synaptic spike
 *******************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg