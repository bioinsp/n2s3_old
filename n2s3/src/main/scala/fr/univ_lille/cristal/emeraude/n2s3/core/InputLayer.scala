package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer._
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.StreamSupport
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.event.{Observable, ObservableMessage, TimedEvent, TimedEventResponse}
import fr.univ_lille.cristal.emeraude.n2s3.support.io._

import scala.annotation.tailrec

/********************************************************************************************************************
 * Messages
 ******************************************************************************************************************/

object AskRemainInput extends Message

/********************************************************************************************************************
 * Events
 ******************************************************************************************************************/

object LabelChangeEvent extends TimedEvent[LabelChangeResponse]
case class LabelChangeResponse(timestamp: Timestamp, endTime : Timestamp, label : String) extends TimedEventResponse

/**
  * Specialization of the NetworkContainer for the input layer
  */
class InputLayer(stream: StreamSupport[_, InputSeq[N2S3Input]]) extends NetworkContainer with Observable {

 /********************************************************************************************************************
  * Declaration of events
  ******************************************************************************************************************/
  addEvent(LabelChangeEvent)

  @tailrec
  private def addressConverter(index : Seq[Int], dimension : Seq[Int], acc : Int) : Int = {
    if(index.isEmpty)
      acc
    else
      addressConverter(index.tail, dimension.tail, acc+index.head*dimension.tail.product)
  }

  override def receiveMessage(message: Message, sender: NetworkEntityReference): Unit = message match {
    case AskInput =>
      if (!stream.atEnd()) {
        val list = stream.next()
        if (list.isEmpty) {
          sender.send(CleanQueue)
          sender.send(ResponseInput)
        }
        else {
          var maxTimestamp : Timestamp = 0
          list.foreach {
            case N2S3InputSpike(spike, timestamp, index) =>
              if(timestamp < 0)
                throw new RuntimeException("neg")
              assert(index.size == stream.shape.dimensionNumber)
              childEntityAt(addressConverter(index.reverse, stream.shape.dimensions.reverse, 0)).sendMessageFrom(NeuronMessage(timestamp, spike, null, null, None), TrashNetworkEntityReference)
              maxTimestamp = math.max(maxTimestamp, timestamp)
            case N2S3InputLabel(label, start, end) =>
              this.triggerEventWith(LabelChangeEvent, LabelChangeResponse(start, end, label))
            case N2S3InputEnd(_) =>
          }
          //this.stream.getStageManagementStrategy().itemProcessed()
          sender.send(ResponseInput)
          sender.send(ProcessUntil(maxTimestamp))
        }
      }
      else{
        sender.send(NoMoreInput)
        sender.send(ProcessUntil(-1L))
      }
    case AskRemainInput =>
      sender.answer(!stream.atEnd())
    case m: ObservableMessage =>
      processEventHolderMessage(m)
    case _ => super.receiveMessage(message, sender)
  }

}
