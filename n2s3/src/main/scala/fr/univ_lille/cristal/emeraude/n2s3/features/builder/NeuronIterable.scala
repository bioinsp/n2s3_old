package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.Shape

/**
  * Created by falezp on 23/05/16.
  */
@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
abstract class NeuronIterable extends Traversable[NetworkEntityPath] {
  val shape : Shape
  def dimensionNumber = shape.dimensionNumber
  def get(index :  Seq[Int]) : NetworkEntityPath
  def getDimension(index : Int*) : NeuronIterable
  def firstDimension : Seq[NeuronIterable]

  def apply(index : Int*) = get(index)
}

@deprecated("Use the correct Neuron and NeuronGroup refs instead", "4/8/2016")
case class ShapedNeuronIterable(neurons : Seq[NetworkEntityPath], shape : Shape) extends NeuronIterable {

  assert(this.shape.product == neurons.size)


  def get(index :  Seq[Int]) : NetworkEntityPath = {
    assert(index.size == shape.dimensionNumber)
    neurons(shape.indices.map( i => shape.drop(i+1).product ).zip(index).map{ case(dim, i) => i*dim}.sum)
  }

  def getDimension(index : Int*) : NeuronIterable = {
    assert(index.size < shape.dimensionNumber)

    def computeBound(start : Int, remainDimension : Shape, remainIndex : Seq[Int]) : (Int, Int, Shape) = {
      if(remainIndex.isEmpty)
        (start, start+remainDimension.product, remainDimension)
      else {
        val size = remainDimension.product
        computeBound(start + remainIndex.head * size, remainDimension.tail, remainIndex.tail)
      }
    }
    val(start, end, remainDimension) = computeBound(0, shape, index)
    ShapedNeuronIterable(neurons.slice(start, end), remainDimension)
  }

  def firstDimension : Seq[NeuronIterable] = {
    neurons.grouped(shape.tail.product).map(e => ShapedNeuronIterable(e, shape.tail)).toSeq
  }

  override def foreach[U](f: (NetworkEntityPath) => U): Unit = neurons.foreach(f)

}