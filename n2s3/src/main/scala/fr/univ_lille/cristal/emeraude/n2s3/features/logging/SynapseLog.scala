package fr.univ_lille.cristal.emeraude.n2s3.features.logging

import java.io.FileOutputStream

import akka.actor._
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.WaitEndOfActivity
import fr.univ_lille.cristal.emeraude.n2s3.core.{ExternalSender, NetworkEntityPath}
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph.{SynapsesConductancesGraph, SynapsesWeightsBarGraph, ValuesGraph}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.SynapseWeightResponse
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitTime._
import fr.univ_lille.cristal.emeraude.n2s3.support.event.EventTriggered

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


case class Run(beg: Time, end: Time)

/**
 * Will monitor weights of synapses in the file output/log/synapse.log
 */
class SynapsesLogText extends Actor {
  val fos: FileOutputStream = new FileOutputStream("output/log/synapse.log")
  def receive = {
    case SynapseWeightResponse(timestamp, source, connectionId, value) =>
      fos.write((source.path.name + ":" + connectionId + "\t" + timestamp + "\t" + value + "\n").map(_.toByte).toArray)
  }
}

/**
 * Will monitor weights of synapses during the simulation in a bar graph
 */
class SynapsesLogBarGraph extends Actor {
  val graph = new SynapsesWeightsBarGraph
  def receive = {
    case SynapseWeightResponse(timestamp, source, id, value) =>
      graph.refreshSerie(source.path.name + ":" + id, value)
      if (!(graph isRunning()))
        graph.launch()
  }
}

/**
 * It will record the weights of subscribed synapses, and will display a plot
 * when it receive Run(beg, end)
  *
  * @param step : the difference in time between two points
 * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class SynapsesGraph(step: Int = 50, unit : TimeUnitType = MilliSecond) extends Actor {
  val event = mutable.HashMap[Timestamp, ListBuffer[(String, Float)]]()
  var lastT : Timestamp = 0
  def receive = {
    case SynapseWeightResponse(t, source, id, value) =>
      if (t - lastT > step) {
        event.get(t) match {
          case Some(e) =>
            e += ((source.path.name + ":" + id, value))
          case _ =>
            event += t -> new ListBuffer[(String, Float)]()
            event(t) += ((source.path.name + ":" + id, value))
        }
        lastT = t
      }
    case Run(beg: Time, end: Time) =>
      val graph = new ValuesGraph
      event.keys.toList.sortBy(x => x).foreach { k =>
        if (k >= beg.timestamp && k <= end.timestamp)
          for (i <- 0 until event.get(k).size) {
            graph.addSerie(event(k)(i)._1, convert(unit, Time(k)), event(k)(i)._2)
          }
      }
      graph launchOffline()
  }
}

/**
 * Will monitor weights of synapses during the simulation
  *
  * @param step : the difference in time between two points
 * @param unit : the unit of Time used (Second, MilliSecond, MicroSecond)
 */
class SynapsesLogGraph(step: Int = 50, unit : TimeUnitType = MilliSecond) extends Actor {
  val graph = new ValuesGraph
  var lastT : Timestamp = 0
  def receive = {
    case SynapseWeightResponse(t, source, id, value) =>
      if (t - lastT > step) {
        graph.refreshSerie(source.path.name + ":" + id, convert(unit, Time(t)), value)
        if (!graph.isRunning())
          graph.launch()
        lastT = t
      }
  }
}

/**
 * Will monitor 1 weight for each of synapses
 */
class SynapsesWeightRepartition(sync: NetworkEntityPath) extends Actor {
  val values = new scala.collection.mutable.HashMap[String, Float]()
  ExternalSender.askTo(sync, WaitEndOfActivity)
  def receive = {
    case SynapseWeightResponse(timestamp, source, id, value) =>
      values += (source.path.name + ":" + id) -> value
    case EventTriggered =>
      val graph = new SynapsesConductancesGraph
      val fos: FileOutputStream = new FileOutputStream("output/log/synapseRepart.log")
      values.keys.foreach { k =>
        fos.write((k + "\t" + values(k) + "\n").map(_.toByte).toArray)
        graph.addSerie(values(k))
      }
      graph.launch()
      fos.close()
  }
}