/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.PrintWriter

import fr.univ_lille.cristal.emeraude.n2s3.core.actors.Config

class BenchmarkHTMLView(result : BenchmarkResult) extends ReportHTML {

  implicit val timeout = Config.defaultTimeout

  addExperimentParameter("Neuron number", result.neurons.size.toString)
  addExperimentParameter("Label number", result.labels.size.toString)

  def generateContent(writer : PrintWriter) : Unit = {

    // General stats
    addCategory("General stats", { writer =>
      writer.println(s"""
          Input number : ${result.inputNumber} <br />
          Correct input (Max neuron spiking) : ${result.evaluationByMaxSpiking.score} (${(result.evaluationByMaxSpiking.score.toFloat / result.inputNumber.toFloat * 100.0f)} %)<br/>
          Correct input (Only good neuron spiking) : ${result.evaluationByOneSpiking.score} ( ${(result.evaluationByOneSpiking.score.toFloat / result.inputNumber.toFloat * 100.0f)} %)<br/>
          Correct input (First spiking) : ${result.evaluationByFirstSpiking.score} ( ${(result.evaluationByFirstSpiking.score.toFloat / result.inputNumber.toFloat * 100.0f)} %)<br/>
          Quiet input number : ${result.quietInputCount}""")
    })
    
    val tablewidth = math.floor(100.0f / (result.labels.size + 2).toFloat).toInt
    var tableth = s"<th style='width: $tablewidth%'></th>"            
    result.labels.foreach { 
      label => tableth+=s"<th style='width: $tablewidth%'> $label </th>"
    }
    tableth+=s"<th style='width:$tablewidth%'>Total</th>"
    
    // Max Label Scoring
    addCategory("Cross stats", { writer =>
      Map("Max neuron spiking" -> result.evaluationByMaxSpiking, 
          "Good neuron spiking" -> result.evaluationByOneSpiking,
          "First neuron spiking" -> result.evaluationByFirstSpiking
      ).foreach { case (name, evaluation) =>
          writer.println(s"""
            <center><h3> $name </h3></center>
            <table>
            <tr>
            $tableth
            </tr>
          """)
          
          result.neurons.zipWithIndex.foreach { case (actor, neuronIndex) =>
            writer.println(s"<tr><th>$actor</th>")

            result.labels.zipWithIndex.foreach { case (label, labelIndex) =>
              val color = "%02X".format(255 - math.floor(evaluation.crossScore(labelIndex)(neuronIndex).toFloat / evaluation.scorePerNeuron(actor).toFloat * 255f).toInt)
              writer.println("<td" + (if (evaluation.labelAssoc(actor) == label)
                " style=\"background-color: green\""
              else
                " style=\"background-color: #FF" + color + color + "\""
                ) + ">" + evaluation.crossScore(labelIndex)(neuronIndex) + "</td>")
            }
            writer.println(s"<td>${evaluation.scorePerNeuron(actor)}</td>")
            writer.println(s"</tr>")
          }
          writer.println("<tr><th>Total</th>")
          result.labels.foreach { label =>
            writer.println(s"<td>${evaluation.scorePerLabel(label)}</td>")
          }
          writer.println("</tr>")
          writer.println("</table>")
        }

      writer.println(s"""
        <center><h3>Quiet input</h3></center>
        <table>
        <tr>
        $tableth
        <tr><th>Number</th>""")

      result.labels.foreach { label =>
        val r = result.quietLabelCount.get(label) match {
                    case Some(i) => i.toString
                    case None => "/"
                }
        writer.println(s"<td>$r</td>")
      }
      writer.println(s"<td>${result.quietInputCount}</td>")
      writer.println("</tr>")
      writer.println("</table>")
    })

    // neuron detail
    addCategory("Neuron detail", { writer =>

      writer.println(s"""
        <table>
        <tr><th>Neuron</th><th>Theta (Dynamic Threshold)</th><th>Average input synaptic wieght</th><th>Average frequency</th></tr>
      """)
      
      result.neurons.foreach { actor =>
        writer.println(f"""
           <tr>
             <th> $actor </th>
             <td>${result.theta(actor)}%1.2f</td>
             <td>${result.avgSynapsticWeight(actor)}%1.2f</td>
             <td>${result.neuronActivity(actor).toFloat / ((result.endTimestamp - result.startTimestamp).toFloat/1e6f)}%1.4f </td>
           </tr>""")
      }
      writer.println("</table>")
    })
  }
}