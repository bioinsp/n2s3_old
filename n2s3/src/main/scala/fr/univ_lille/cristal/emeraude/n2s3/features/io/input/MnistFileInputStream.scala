package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.{DataInputStream, FileInputStream, IOException}

import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStream

/**
 * Class Reader for mnist file
 */

object MnistEntry {
  def apply() = new StreamEntry[SampleInput](Shape(28, 28))
}

class MnistFileInputStream(val imageFile: String, val labelFile: String, val sizeOfChunk: Int  = 64) extends InputStream[SampleInput] {


  //Magic number
  val MAGIC_IMAGE = 2051
  val MAGIC_LABEL = 2049

  //stream of file
  var labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
  var imagesInputStream = new DataInputStream(new FileInputStream(imageFile))

  //Check the magic number
  if( labelsInputStream.readInt() != MAGIC_LABEL )
    throw new IOException("Error on magic number for label file: ["+labelFile+"]")

  if( imagesInputStream.readInt != MAGIC_IMAGE )
    throw new IOException("Error on magic number for image file: ["+imageFile+"]")

  //check the number of elements
  val numberImage = imagesInputStream.readInt()
  val numberLabel = labelsInputStream.readInt()
  private var numberOfReadImages: Int = 0

  var cursor = 0

  if( numberImage != numberLabel )
    throw new IOException("The number of images contained in: ["+imageFile+"] does not match the number of label ["+labelFile+"]")

  val rows = imagesInputStream.readInt()
  val columns = imagesInputStream.readInt()

  val imagesBuffer = Array.ofDim[Double](sizeOfChunk, rows, columns)
  val label = Array.ofDim[Double](sizeOfChunk)

  def next() : SampleInput = {

    if (this.numberOfReadImages >= this.numberLabel){
      throw new IOException("Attempt to read after the end of the file")
    }


    val label =  labelsInputStream.readUnsignedByte().toString
    val image = for{
      i <- 0 until rows
      j <- 0 until columns
    } yield SampleUnitInput(imagesInputStream.readUnsignedByte().toFloat / 255f, j, i)
    this.numberOfReadImages += 1

    if(numberOfReadImages%100 == 0) {
      println("[MNIST] "+numberOfReadImages+"/"+this.numberLabel)
    }

    SampleInput(image, label)
  }

  def atEnd(): Boolean = this.numberOfReadImages >= this.numberImage

  def reset() = {
    labelsInputStream = new DataInputStream(new FileInputStream(labelFile))
    labelsInputStream.readInt()
    labelsInputStream.readInt()
    imagesInputStream = new DataInputStream(new FileInputStream(imageFile))
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    imagesInputStream.readInt()
    cursor = 0
    numberOfReadImages = 0
  }

  def shape : Shape = Shape(columns, rows)

  override def toString = " data_file=" + this.imageFile + " label_file=" + this.labelFile
}
