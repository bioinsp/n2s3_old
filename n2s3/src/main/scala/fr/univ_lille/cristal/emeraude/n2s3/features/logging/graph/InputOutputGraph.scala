/**************************************************************************************************
 * Contributors:
 * 		- created by emeraude
 *    - (add your name here if you contribute to this code). 
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph

import java.awt.{Color, Dimension, FlowLayout, Graphics}
import javax.swing.{JFrame, JLabel, JPanel}

import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntityPath
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

/*************************************************************************************************
 * Class to display input and output in the same time (neurons which are firing)
 ************************************************************************************************/
class InputOutputGraph(rawLayers : Seq[(String, Int, Seq[NetworkEntityPath], Int)]) {

  val bufferSize = 1024

  val frame = new JFrame("Spike viewer")

  val pan = new JPanel

  pan setLayout new FlowLayout

  frame add pan

  val layers = Map(rawLayers.flatMap { case (name, width, actors, caseSize) =>
    val panel = new LayerPanel(width, actors, caseSize)
    pan add new JLabel(name)
    pan add panel
    actors.map(key => (key, panel))
  }:_*)

  frame setLocation (100, 100)

  frame setVisible true

  frame pack

  frame setDefaultCloseOperation JFrame.EXIT_ON_CLOSE

  //private val queue = scala.collection.mutable.PriorityQueue[(Timestamp, NetworkEntityPath)]()(Ordering.by(_._1))
  private var currentTimestamp = 0L
  var lastPaintTime = System.nanoTime()

  def event(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    //if (timestamp >= currentTimestamp)
    //queue.enqueue((timestamp, sender))

    //display()

    if(timestamp < currentTimestamp)
      return

    currentTimestamp = math.max(currentTimestamp, timestamp)

    layers.get(sender) match {
      case Some(panel) =>
        panel.event(currentTimestamp, sender)
      case None => throw new RuntimeException("[InputOutputGraph] Unable to find entry")
    }

    if(System.nanoTime()-lastPaintTime > 1000000000/24) {
      frame.repaint()
      lastPaintTime = System.nanoTime()
      layers.foreach(_._2.setTimestamp(currentTimestamp))
    }
  }

  def display() = {
   /* while (queue.nonEmpty) {
      currentTimestamp = queue.head._1
      layers.foreach(_._2.setTimestamp(currentTimestamp))
      while(queue.nonEmpty && queue.head._1 == currentTimestamp) {
        queue.dequeue() match { case (_, path) =>
          layers.get(path) match {
            case Some(panel) =>
              panel.event(currentTimestamp, path)
            case None => throw new RuntimeException("[InputOutputGraph] Unable to find entry")
          }
        }
      }
    }
    if(System.nanoTime()-lastPaintTime > 1000000000) {
      frame.repaint()
      lastPaintTime = System.nanoTime()
    }*/
  }

}

class LayerPanel(val layerWidth: Int, val sources : Seq[NetworkEntityPath], val neuronSize: Int = 25) extends JPanel {
  private val mapper = Map(sources.zipWithIndex:_*)

  private val pontential = Array.ofDim[Float](mapper.size)
  private var currentTimestamp : Timestamp = 0

  val leakTime = 25 MilliSecond

  setPreferredSize(new Dimension(neuronSize * layerWidth + 1, neuronSize * math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt + 1))
  override def paintComponent(g: Graphics): Unit = {
    g.setColor(Color.black)
    g.fillRect(0, 0, layerWidth * neuronSize, math.ceil(mapper.size.toFloat/layerWidth.toFloat).toInt * neuronSize)

    pontential.zipWithIndex.foreach{ case (pv, index) =>
      val v = 1.0f-math.exp(-pv).toFloat
      g.setColor(new Color(v, v, v))
      g.fillRect((index%layerWidth) * neuronSize, (index/layerWidth) * neuronSize, neuronSize, neuronSize)
    }
  }

  def setTimestamp(timestamp : Timestamp) : Unit = {
    if(timestamp != currentTimestamp) {
      val leak = math.exp(-(timestamp - currentTimestamp).toFloat / leakTime.timestamp.toFloat).toFloat
      for (i <- pontential.indices)
        pontential(i) *= leak
      currentTimestamp = timestamp
    }
  }

  def event(timestamp : Timestamp, sender : NetworkEntityPath) : Unit = {
    pontential(mapper(sender)) += 0.3f
  }

}
