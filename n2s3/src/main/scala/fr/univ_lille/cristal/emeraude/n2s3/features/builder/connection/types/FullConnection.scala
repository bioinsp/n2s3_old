package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.NeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}

/**
  * Connects all entities inside a [[NeuronGroupRef]] to all entities inside another [[NeuronGroupRef]]
  */
class FullConnection(neuronConnectionConstructor : Option[() => NeuronConnection]) extends ConnectionPolicy {

  def this(){
    this(None)
  }

  def this(neuronConnectionConstructor : () => NeuronConnection){
    this(Some(neuronConnectionConstructor))
  }

  private def newConnection(): Option[NeuronConnection] = {
    neuronConnectionConstructor match {
      case Some(f) => Some(f())
      case None => None
    }
  }

  override def generate(from : NeuronIterable, to : NeuronIterable) = {
    for {
      in <- from
      out <- to
      if in != out
    } yield {
      Connection(in, out, this.newConnection())
    }
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    for {
      in <- from.neurons
      out <- to.neurons
      if this.connects(in, out)
    } yield {
      Connection(in.actorPath.get, out.actorPath.get, this.newConnection())
    }
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = aNeuron != anotherNeuron
}