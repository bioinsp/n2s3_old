package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.PrintWriter

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp

/**
  * Created by falezp on 15/03/16.
  */
abstract class Report {

  val experimentParameter = scala.collection.mutable.Map[String, String]()
  var writer : PrintWriter = _

  def addExperimentParameter(key : String, value : String) : Unit = {
    experimentParameter += key -> value
  }

  def formatTimestamp(timestamp : Timestamp) : String = {
    var str = (timestamp%1000)+"us"

    val ms = timestamp/1000
    str = (ms%1000)+"ms "+str

    val s = ms/1000
    str = (s%1000)+"s "+str

    str
  }

  def save(filename : String) : Unit
}
