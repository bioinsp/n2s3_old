package fr.univ_lille.cristal.emeraude.n2s3.support.event

import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.SynchronizedMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.EventNotFoundException
import fr.univ_lille.cristal.emeraude.n2s3.core.{NetworkEntityPath, NetworkEntityReference}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/********************************************************************************************************************
 * Messages
 ******************************************************************************************************************/

abstract class ObservableMessage extends Message
object EventTriggered extends EventResponse

case class Subscribe(event : Event[_ <: EventResponse], ref : NetworkEntityReference) extends ObservableMessage
case class SubscribeSynchronized(event : TimedEvent[_ <: TimedEventResponse], ref : NetworkEntityReference, synchronizer: NetworkEntityPath) extends ObservableMessage
case class Unsubscribe(event : Event[_ <: EventResponse], ref : NetworkEntityReference) extends ObservableMessage

case class SynchronizedEvent(synchronizer : NetworkEntityPath, override val postSync : NetworkEntityReference, override val message : Message, override val timestamp : Timestamp) extends SynchronizedMessage

/**
  * Implementation of the Observer design pattern
  * Users of this trait becomes observables.
  */
trait Observable {
  /**
    * Contains the list of declared event, associated with all observers
    */
  private val events = mutable.HashMap[Event[_ <: EventResponse], mutable.ArrayBuffer[NetworkEntityReference]]()
  private val synchronizedEvents = mutable.HashMap[TimedEvent[_ <: TimedEventResponse], mutable.ArrayBuffer[(NetworkEntityReference, NetworkEntityPath, NetworkEntityReference)]]()

  /**
    * Declares an event to be handled by this observable.
    *
    * @param event is the type of the event
    */
  def addEvent(event : Event[_ <: EventResponse]): Unit = {
    events += event -> ArrayBuffer[NetworkEntityReference]()

    if(event.isInstanceOf[TimedEvent[_]])
      synchronizedEvents += event.asInstanceOf[TimedEvent[TimedEventResponse]] -> ArrayBuffer[(NetworkEntityReference, NetworkEntityPath, NetworkEntityReference)]()
  }

  /**
    * Subscribes an observer to the specified event
    *
    * @param entity is the entity which will receive response when event is triggered
    * @param event is the event to be observed
    * @throws EventNotFoundException is the event is not declared in this class
    */
  def addSubscription(entity : NetworkEntityReference, event : Event[_ <: EventResponse]) : Unit = {
    events.get(event) match {
      case Some(list) => list += entity
      case None => throw new EventNotFoundException(event, this.getClass)
    }
  }

  /**
    * Subscribes an observer to the specified event. Triggered message will pass through the synchronizer
    *
    * @param entity is the entity which will receive response when event is triggered
    * @param event is the event to be observed
    * @throws EventNotFoundException is the event is not declared in this class
    */
  def addSynchronizedSubscription(entity : NetworkEntityReference, event : TimedEvent[_ <: TimedEventResponse], synchronizer : NetworkEntityPath) : Unit = {
    synchronizedEvents.get(event) match {
      case Some(list) => list += ((referenceToSynchronizer(synchronizer), synchronizer, entity))
      case None => throw new EventNotFoundException(event, this.getClass)
    }
  }

  /**
    * Unsubscribes an observer from an event
    *
    * @param entity is the entity which will receive response when event is triggered
    * @param event is the event to be observed
    * @throws EventNotFoundException is the event is not declared in this class
    */
  def removeSubscription(entity : NetworkEntityReference, event : Event[_ <: EventResponse]) : Unit = {
    events.get(event) match {
      case Some(list) => list -= entity
      case None => throw new EventNotFoundException(event, this.getClass)
    }
    events.getOrElseUpdate(event, ArrayBuffer[NetworkEntityReference]())
  }

  /**
    * Triggers an event.
    * Sends the default response of this event to all its observers.
    * If this event is single usage, remove all subscriptions to it.
    *
    * @param event is the event to trigger
    * @tparam Response is the response type of the event
    */
  def triggerEvent[Response <: EventResponse](event : Event[Response]) : Unit = {
    triggerEventWith(event, event.defaultResponse)
  }

  /**
    * Triggers an event with a specific response message.
    * Sends the specified response to all observers.
    * If this event is single usage, all observers will be removed
    *
    * @param event is the event to trigger
    * @param response is the response which will be sent to observers
    * @tparam Response is the response type of the event
    */
  def triggerEventWith[Response <: EventResponse](event : Event[Response], response : Response) : Unit = {
    events.get(event) match {
      case Some(subscribers) =>
        subscribers.foreach(subscriber => subscriber.send(response))
        if(event.isSingleUsage)
          subscribers.clear()
      case None =>
    }

    response match {
      case message : TimedEventResponse =>
        synchronizedEvents.get(event.asInstanceOf[TimedEvent[TimedEventResponse]]) match {
          case Some(subscribers) =>
            subscribers.foreach{ case (synchronizer, path, subscriber) => synchronizer.send(SynchronizedEvent(path, subscriber, message, message.timestamp)) }
            if(event.isSingleUsage)
              subscribers.clear()
          case None =>
        }
      case _ =>
    }
  }

  /**
    * method which allow to process all ObservableMessage
    *
    * @param message is the content
    */
  def processEventHolderMessage(message : ObservableMessage) : Unit = {
    message match {
      case Subscribe(event, ref) => addSubscription(ref, event)
      case SubscribeSynchronized(event, ref, synchronizer) =>
        addSynchronizedSubscription(ref, event, synchronizer)
      case Unsubscribe(event, ref) => removeSubscription(ref, event)
    }
  }

  def referenceToSynchronizer(synchronizer : NetworkEntityPath) : NetworkEntityReference
}