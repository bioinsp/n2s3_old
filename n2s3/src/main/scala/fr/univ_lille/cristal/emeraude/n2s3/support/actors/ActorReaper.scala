/**************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille.1
 *    - (add your name here if you contribute to this code).
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.{Actor, ActorRef, Terminated}
import scala.collection.mutable.ArrayBuffer

object Reaper {
  // Used by others to register an Actor for watching
  case class WatchMe(ref: ActorRef)
}

/************************************************************************************************
  * Helper class to track the termination of actors.
  * This actor reaper is an actor on itself.
  * It should be configured with the actors to watch using the WatchMe message.
  * Concrete implementations should implement allSoulsReaped to act when every watched actor is dead,
  * for example, to finish the actor system when every actor is dead
  ***********************************************************************************************/
abstract class ActorReaper extends Actor {
  import Reaper._

  // Keep track of what we're watching
  val watched = ArrayBuffer.empty[ActorRef]

  // Derivations need to implement this method.  It's the
  // hook that's called when everything's dead
  def allSoulsReaped(): Unit

  // Watch and check for termination
  final def receive = {
    case WatchMe(ref) =>
      context.watch(ref)
      watched += ref
    case Terminated(ref) =>
      watched -= ref
      if (watched.isEmpty) allSoulsReaped()
  }
}