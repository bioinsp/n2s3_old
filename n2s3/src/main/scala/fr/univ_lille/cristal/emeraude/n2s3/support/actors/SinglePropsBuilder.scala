/**************************************************************************************************
 * Contributors:
 * 		- created by guillermo.polito@univ-lille.1
 *    - (add your name here if you contribute to this code).
 **************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.support.actors

import akka.actor.{Actor, Props}

import scala.reflect.ClassTag

class SinglePropsBuilder[T <: Actor : ClassTag ] extends PropsBuilder {
  override def build(): Props = Props[T]
}
