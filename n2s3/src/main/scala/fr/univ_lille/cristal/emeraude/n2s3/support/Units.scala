package fr.univ_lille.cristal.emeraude.n2s3.support

import GlobalTypesAlias._

object UnitTime extends Enumeration {
  type TimeUnitType = Value
  val Second, MilliSecond, MicroSecond = Value

  def convert(unit : TimeUnitType, t : Time) : Int = {
    unit match  {
      case Second =>  (t.timestamp / 1e6).toInt
      case MilliSecond => (t.timestamp / 1e3).toInt
      case MicroSecond =>  t.timestamp.toInt
    }
  }
}

/**********************************************************************************************************
  * Time Unit
  * basic time unit is microsecond
  **********************************************************************************************************/
case class Time(timestamp : Timestamp) {
  def *(n : Timestamp) : Time = Time(n*timestamp)
  def +(time : Time) : Time = Time(timestamp+time.timestamp)

  def asSecond = timestamp.toFloat/1e6f
  def asMilliSecond = timestamp.toFloat/1e3f
  def asMicroSecond = timestamp.toFloat
}

class TimeUnit(val value : Double) {
  def Second = Time((value*1e6).toInt)
  def MilliSecond = Time((value*1e3).toInt)
  def MicroSecond = Time(value.toInt)
}

object UnitCast {
  implicit def timeCast(value : Double) : TimeUnit = new TimeUnit(value.toFloat)
  implicit def timeCast(value : Float) : TimeUnit = new TimeUnit(value)
  implicit def timeCast(value : Int) : TimeUnit = new TimeUnit(value)
}