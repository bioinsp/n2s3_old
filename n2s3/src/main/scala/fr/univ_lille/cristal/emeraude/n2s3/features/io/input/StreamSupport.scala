package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.EOFException

import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{Input, InputSeq, InputStream, N2S3Input}

import scala.collection.mutable.ArrayBuffer

/**
  * Created by falezp on 30/06/16.
  */

trait StreamTimestampsManager {

  private var currentPrefix : Timestamp = 0
  protected var maxTimestamp : Timestamp = 0

  def prefix = currentPrefix

  def setPrefix(timestamp : Timestamp) : Unit = {
    this.currentPrefix = timestamp
  }

  def maxPrefix() = {
    this.currentPrefix = maxTimestamp
  }

  def resetTimestamps() = {
    currentPrefix = 0L
    maxTimestamp = 0L
  }

}

object GlobalStream extends StreamTimestampsManager {

  def process[T <: InputSeq[Input]](data : T) = data.transformInputSeq(seq => seq.map {
      case t: N2S3Input =>
        val n = t.prefixTimestamp(prefix)
        maxTimestamp = math.max(maxTimestamp, n.endTimestamp)
        n
      case t =>
        throw new UnsupportedOperationException
  })

}

class StreamSupport[I <: InputSeq[_ <: Input], O <: InputSeq[_  <: Input]](val shape : Shape) {

  var input : StreamEntry[I] = _
  var output : StreamSupport[I, InputSeq[Input]] = _
  var outputConverter : StreamConverter[O, InputSeq[Input]] = _

  def pipe[T <: InputSeq[_ <: Input]](converter : StreamConverter[O, T]): StreamSupport[I, T] = {
    converter.setShape(converter.shapeConverter(shape))
    val outputStream = new StreamSupport[I, T](converter.shapeConverter(shape))
    outputStream.input = getEntry
    output = outputStream.asInstanceOf[StreamSupport[I, InputSeq[Input]]]
    outputConverter = converter.asInstanceOf[StreamConverter[O, InputSeq[Input]]]
    outputStream
  }

  def getEntry : StreamEntry[I] = {
    if(input == null)
      throw new RuntimeException("no entry provided")
    else
      input
  }

  def >> [T <: InputSeq[_ <: Input]](converter : StreamConverter[O, T]) = pipe[T](converter)

  def process(data : I) : O = {
    input.innerProcess(data).asInstanceOf[O]
  }

  protected def innerProcess(data : O) : InputSeq[_] = {
    if(output == null) {
      GlobalStream.process(data.asInstanceOf[InputSeq[Input]])
    }
    else
      output.innerProcess(outputConverter.dataConverter(data))
  }

  def append(stream : InputStream[I]) : Unit = {
    input.append(stream)
  }

  def clean(): Unit = {

  }

  def atEnd() : Boolean = input.atEnd()

  def next() : O = process(input.next())
}

class StreamEntry[I <: InputSeq[_ <: Input]](shape : Shape) extends StreamSupport[I, I](shape) {

  private val entryStreams = new ArrayBuffer[InputStream[I]]
  private var entryCursor = 0

  override def getEntry : StreamEntry[I] = {
    this
  }

  override def append(stream : InputStream[I]) : Unit = {
    entryStreams += stream
  }

  override def clean() : Unit = {
    entryStreams.clear()
    entryCursor = 0
  }

  def restart() = {
    entryStreams.foreach(_.reset())
    entryCursor = 0
  }

  def checkCurrentStream() : Unit = {
    while(entryCursor < entryStreams.size && entryStreams(entryCursor).atEnd()) {
      GlobalStream.maxPrefix()
      entryCursor += 1
    }
  }

  override def atEnd() : Boolean = {
    checkCurrentStream()
    entryCursor >= entryStreams.size
  }

  override def next(): I = {
    if(atEnd())
      throw new EOFException

    if(output == null) {
      GlobalStream.process(entryStreams(entryCursor).next().asInstanceOf[InputSeq[Input]]).asInstanceOf[I]
    }
    else
      entryStreams(entryCursor).next()
  }
}