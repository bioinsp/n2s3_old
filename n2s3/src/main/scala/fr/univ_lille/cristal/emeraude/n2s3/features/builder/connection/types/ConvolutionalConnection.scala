package fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types

import fr.univ_lille.cristal.emeraude.n2s3.core.models.convolutional.SharedNeuronConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{NeuronGroupRef, NeuronIterable, NeuronRef}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.convolutional.Filter2D

import scala.collection.mutable

/**
  * Convolutional connection between two neuron groups
  * @param filters
  * @param connectionConstructor
  * @param sharedObjConstructor
  * @tparam A
  */
class ConvolutionalConnection[A](filters : Traversable[Filter2D], connectionConstructor : A => SharedNeuronConnection[A], sharedObjConstructor : () => A) extends ConnectionPolicy {

  override def generate(from: NeuronIterable, to: NeuronIterable): Traversable[Connection] = {
    assert(from.shape.dimensionNumber == 2 && to.shape.dimensionNumber == 3)

    val sharedObj = (for (filter <- filters) yield {
      (for {
        offset_x <- 0 until filter.width
        offset_y <- 0 until filter.height
      } yield sharedObjConstructor()).toSeq
    }).toSeq

    for {
      (filter, filter_index) <- filters.toSeq.zipWithIndex
      offset_x <- 0 until filter.width
      offset_y <- 0 until filter.height
      in_x <- 0 until from.shape(0) - filter.width + 1
      in_y <- 0 until from.shape(1) - filter.height + 1
    } yield {
      //println(sharedObj+" "+offset_x+" "+offset_y)
      Connection(from(in_x + offset_x, in_y + offset_y), to(filter_index, in_x, in_y), Some(connectionConstructor(sharedObj(filter_index)(offset_x * filter.width + offset_y))))
    }
  }

  override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
    throw new NotImplementedError()
  }

  /** ******************************************************************************************************************
    * Testing
    * *****************************************************************************************************************/
  override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = throw new NotImplementedError()
}
