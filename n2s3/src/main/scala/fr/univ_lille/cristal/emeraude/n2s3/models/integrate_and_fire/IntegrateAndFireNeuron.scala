package fr.univ_lille.cristal.emeraude.n2s3.models.integrate_and_fire

import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

/**
  * Created by falezp on 03/06/16.
  */

/**
  * Generic Integrate and Fire neuron model
  */
abstract class IntegrateAndFireNeuron extends Neuron {

  /**
    * Compute and return the current membrane potential
    *
    * @param timestamp is current timestamp
    * @param message is content
    * @return current membrane potential
    */
  def computeMembranePotential(timestamp : Timestamp, message : Message) : Float

  /**
    * Compute and return the current membrane threshold
    *
    * @param timestamp is current timestamp
    * @param message is content
    * @return current membrane threshold
    */
  def computeMembraneThreshold(timestamp : Timestamp, message : Message) : Float

  /**
    * Method called when the membrane potential reach the threshold
    *
    * @param timestamp is current timestamp
    * @param message is content
    */
  def fire(timestamp : Timestamp, message : Message, ends: NeuronEnds): Unit

  /**
    * Basic behavior of a Integrate and Fire neuron model
    *
    * @param timestamp is the current time
    * @param message is the content
    */
  def processSomaMessage(timestamp : Timestamp, message : Message, ends: NeuronEnds) : Unit = {
    if(this.computeMembranePotential(timestamp, message) >= this.computeMembraneThreshold(timestamp, message)) {
      triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, NetworkEntityPath(container.self, localPath)))
      fire(timestamp, message, ends)
    }
  }


}
