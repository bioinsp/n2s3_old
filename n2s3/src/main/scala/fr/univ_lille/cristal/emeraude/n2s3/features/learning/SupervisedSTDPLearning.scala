package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import akka.actor.Actor
import fr.univ_lille.cristal.emeraude.n2s3.core.NetworkEntity.AskReference
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.NeuronMessage
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.event.{SubscribeSynchronized, SynchronizedEvent}

/**
  * Created by falezp on 14/10/16.
  */

case class LTPUntil(end : Timestamp) extends Message
case class LTDUntil(end : Timestamp) extends Message


class SupervisedSTDPLearning(n2s3 : N2S3, outputTargets : Map[String, NetworkEntityPath]) extends Actor {

  val outputReferences = outputTargets.map { case (label, path) =>
    val syncRef = new ExternalNetworkEntityReference(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer)
    (label, (syncRef, syncRef.ask(AskReference(path)).asInstanceOf[NetworkEntityReference]))
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    println("[DEBUG] Restart Actor "+self+" : "+reason+(if(message.isDefined) " | "+message else ""))
    super.preRestart(reason, message)
  }

  ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, SubscribeSynchronized(LabelChangeEvent, ExternalSender.getReference(self), n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer))

  override def receive = {
    case SynchronizedEvent(s, _, m, _) =>
      m match {
        case LabelChangeResponse(start, end, label) =>
          outputReferences.get(label) match {
            case Some(entry) =>
              ExternalSender.sendTo(s, NeuronMessage(start, LTPUntil(end), entry._1, entry._2, None))
              outputReferences.filterKeys(_ != label).foreach{case(_, (pre, post)) => ExternalSender.sendTo(s, NeuronMessage(start, LTDUntil(end), pre, post, None))}
            case None => println("Unrecognized label \""+label+"\"")
            // force ltd others ?
          }
        case _ => throw new RuntimeException("Unrecognized message : "+m)
      }
      ExternalSender.sendTo(s, Done)
    case m => throw new RuntimeException("Unrecognized message : "+m)
  }
}
