package fr.univ_lille.cristal.emeraude.n2s3.features.learning

import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.{Next, SetInput, WaitEndOfActivity}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.N2S3
import fr.univ_lille.cristal.emeraude.n2s3.features.logging.{LabelMonitor, SpikeMonitor}
import fr.univ_lille.cristal.emeraude.n2s3.models.bio.SynapseWeightAndDelay
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._

import scala.concurrent.duration._
/**
  * Created by falezp on 26/10/16.
  */

/*
 * From "A supervised learning approach based on STDP and polychronization in spiking neuron network"
 * http://publications.idiap.ch/downloads/papers/2007/paugam-esann-2007.pdf
 */
object DelayAdaptation {

  private def timestampOfNeuron(spikes : Seq[(NetworkEntityPath, Timestamp)], neuron : NetworkEntityPath) : Long = {
    spikes.filter(_._1 == neuron) match {
      case s if s.isEmpty => spikes.maxBy(_._2)._2+(20 MilliSecond).timestamp//tmp Long.MaxValue
      case other => other.minBy(_._2)._2
    }
  }

  def execute(n2s3 : N2S3, outputs : Map[String, NetworkEntityPath], learningRate : Double) : Unit = {

    val min_margin = 10 MilliSecond

    ExternalSender.sendTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, SetInput(n2s3.inputLayerRef.get.actorPath.get))

    val spikeMonitor = new SpikeMonitor(n2s3, n2s3.layers.flatMap(_.neuronPaths))
    val labelMonitor = new LabelMonitor(n2s3, n2s3.inputLayerRef.get.getContainer)

    while (ExternalSender.askTo(n2s3.inputLayerRef.get.getContainer, AskRemainInput).asInstanceOf[Boolean]) {

      ExternalSender.askTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, Next)
      ExternalSender.askTo(n2s3.buildProperties.getSynchronizerPolicy.getInputSynchronizer, WaitEndOfActivity, 100 seconds)

      val spikes = spikeMonitor.getSpikes
      val labels = labelMonitor.getLabels
      if(labels.size != 1)
        throw new RuntimeException("Only one label is allowed for each input step")
      val label = labels.head._1


      // compute margin
      val timings = outputs.map { case(key, value) =>
        (key, timestampOfNeuron(spikes, value))
      }

      val margin = timings.filterKeys(_ != label).minBy(_._2)._2-timings(label)

      if(margin < min_margin.timestamp) {

        outputs.foreach { case(key, neuron_path) =>
          val current_timing = timestampOfNeuron(spikes, neuron_path)

          val current_synapses = ExternalSender.askTo(neuron_path, GetAllConnectionProperty(SynapseWeightAndDelay)) match {
            case PropertyValue(values: Seq[(NetworkEntityPath, (Float, Time))] @unchecked) => values
          }

          val max_timing = current_synapses.map { case (input_neuron, (w, d)) =>
            (input_neuron, timestampOfNeuron(spikes, input_neuron)+d.timestamp)
          }.filter(_._2 <= current_timing).maxBy(_._2)



          ExternalSender.askTo(neuron_path, SetAllConnectionProperty(SynapseWeightAndDelay, current_synapses.map { case(input_neuron, (w, d)) =>
            val n_d = if(timestampOfNeuron(spikes, input_neuron)+d.timestamp == max_timing._2) {
              Time(math.max(0, d.timestamp+(if(outputs(label) == neuron_path) -(learningRate MilliSecond).timestamp else (learningRate MilliSecond).timestamp)))
            }
            else
              d
            (input_neuron, (w, n_d))
          }))
        }
      }

      spikeMonitor.clearSpikes()
      labelMonitor.clearLabels()

    }
  }
}
