/************************************************************************************************************
 * Contributors: 
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.PrintWriter

class BenchmarkSetHTMLView(result : BenchmarkSetResult) extends ReportHTML {

  def generateContent(writer : PrintWriter) : Unit = {

    addCategory("Benchmark ("+result.list.size+" runs)", { writer =>

      Map("Correct input (Max neuron spiking)" -> result.maxSpikingScore, "Correct input (Only good neuron spiking)" -> result.oneSpikingScore,
        "Quiet input number" -> result.quietCount).foreach { case (name, values) =>

        writer.println(f"""
          <b>$name</b> : <br /> 
          &nbsp;&nbsp;&nbsp;Average : ${values.avg}%1.2f ( ${values.avg / result.inputNumber * 100.0f}%1.2f %%)<br />
          &nbsp;&nbsp;&nbsp;Standart Deviation : ${values.std}%1.2f ( ${values.std / result.inputNumber * 100.0f}%1.2f %%)<br />
          &nbsp;&nbsp;&nbsp;Minimum : ${values.min}%1.2f ( ${values.min / result.inputNumber * 100.0f}%1.2f %%)<br />
          &nbsp;&nbsp;&nbsp;Maximum : ${values.max}%1.2f ( ${values.max / result.inputNumber * 100.0f}%1.2f %%)<br />
          <br />
          """)
      }
    })

    result.list.zipWithIndex.foreach{ case (r, index) =>
      writer.println("<div style=\"width: 100%;background-color: #ddd;text-align: center;margin-top: 10px\">Run "+(index+1)+"/"+result.list.size+"</div>")
      /*val view = new BenchmarkHTMLViewOld(r)
      view.writer = writer
      view.generateContent(writer)*/
      throw new UnsupportedOperationException
    }
  }
}
