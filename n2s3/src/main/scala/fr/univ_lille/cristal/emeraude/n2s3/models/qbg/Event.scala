/************************************************************************************************************
 * Contributors: 
 * 		- created by pierre
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.qbg

import akka.actor._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.event.{Event, EventResponse}

/********************************************************************************************************
 * Event trigger when potential of neuron change
 *******************************************************************************************************/

case class NeuronPotentialResponse(timestamp : Timestamp, source : ActorRef, value : Float) extends EventResponse
object NeuronPotentialEvent extends Event[NeuronPotentialResponse]


/********************************************************************************************************
 * Event trigger when weight of synapses changes
 *******************************************************************************************************/

case class SynapseWeightResponse(timestamp : Timestamp, source : ActorRef, connectionId : Int, value : Float) extends EventResponse
object SynapseWeightEvent extends Event[SynapseWeightResponse]

case class ThetaChangeResponse(timestamp : Timestamp, source : ActorRef, value : Float) extends EventResponse
object ThetaChangeEvent extends Event[ThetaChangeResponse]