/************************************************************************************************************
 * Contributors:
 * 		- created by Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.features.io.report

import java.io.{File, PrintWriter}

import akka.actor.Actor
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core.Synchronizer.Done
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message
import fr.univ_lille.cristal.emeraude.n2s3.support.event.{Subscribe, Unsubscribe}

import scala.collection.mutable

/**
  * BenchmarkMonitor messages
  */
case class GetResult() extends Message
object Initialize extends Message



class InputCrossInfo(val label : String , val startTime : Timestamp, val endTime : Timestamp) {
  val spikeList = mutable.ArrayBuffer[(NetworkEntityPath, Timestamp)]()
}
class BenchmarkMonitor(outputNeuron : Seq[NetworkEntityPath]) extends Actor {

  override def postStop() = {
    outputNeuron.foreach { path =>
      ExternalSender.sendTo(path, Unsubscribe(NeuronFireEvent, ExternalSender.getReference(self)))
    }
  }

  outputNeuron.foreach{ path => ExternalSender.sendTo(path, Subscribe(NeuronFireEvent, ExternalSender.getReference(self))) }

  val inputList = scala.collection.mutable.ListBuffer[InputCrossInfo]()

  val inAdvanceSpike = scala.collection.mutable.Queue[(NetworkEntityPath, Timestamp)]()

  def classifySpike(origin : NetworkEntityPath, timestamp : Timestamp) : Unit = {
    var currentIndex = inputList.size - 1

    while (currentIndex > 0 && inputList(currentIndex).startTime > timestamp)
      currentIndex -= 1

    if (currentIndex >= 0)
      inputList(currentIndex).spikeList += ((origin, timestamp))
  }

  def receive = {
    case LabelChangeResponse(start, end, label) =>
      inputList += new InputCrossInfo(label, start, end)

    case NeuronFireResponse(timestamp, path) =>
      if(inputList.isEmpty || timestamp > inputList.last.endTime) {
        inAdvanceSpike += ((path, timestamp))
      }
      else {
        classifySpike(path, timestamp)
      }
    case GetResult() =>
      inAdvanceSpike.foreach(spike => classifySpike(spike._1, spike._2))
      inAdvanceSpike.clear()

      val file = new File("results/raw_spikes")
      if (file.exists()) file.delete()
      file.getParentFile.mkdirs()
      val writer = new PrintWriter(file)
      inputList.zipWithIndex.foreach{ case(input, index) =>
        writer.println("#" + (index+1) + " : " + input.label+" ["+input.startTime+";"+input.endTime+"]")
        input.spikeList.foreach(spike => writer.println(spike._1+" "+spike._2))
      }
      writer.close()
      sender ! new BenchmarkResult(inputList.map(input => input.label).distinct , outputNeuron, inputList)

    case Done => sender ! Done
    case Initialize => sender ! Done
  }

}