package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import java.io.File
import javax.imageio.ImageIO

import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStream

import scala.util.Random
/**
  * Created by falezp on 18/08/16.
  */
object LFWEntry {
  val width = 250
  val height = 250

  def apply() = new StreamEntry[SampleInput](Shape(width, height))
}

class LFWReader(directoryPath : String) extends InputStream[SampleInput] {

  val mainDirectory = new File(directoryPath)

  override def next(): SampleInput = {

    val subDirectories = mainDirectory.listFiles()
    val selectedDirectory = subDirectories(Random.nextInt(subDirectories.length))
    val images = selectedDirectory.listFiles()
    val selectedImage = images(Random.nextInt(images.length))
    val image = ImageIO.read(selectedImage)

    val label =  "_"
    val data = for{
      i <- 0 until LFWEntry.width
      j <- 0 until LFWEntry.height
    } yield SampleUnitInput(image.getRGB(i, j) & 0xFF, i, j)

    println("[LWF] "+selectedImage.getPath)
    SampleInput(data, label)
  }

  override def atEnd(): Boolean = false

  override def reset(): Unit = {}
}
