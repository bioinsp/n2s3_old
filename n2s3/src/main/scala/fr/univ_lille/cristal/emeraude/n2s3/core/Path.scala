/************************************************************************************************************
 * Contributors: 
 * 		-  Pierre Falez
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.ActorRef
import akka.util.Timeout
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{Config, NetworkEntityActor}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.{ExplicitSenderRoutedMessage, ImplicitSenderRoutedMessage}
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.concurrent.{Await, Future}


object NetworkEntityPath {

  def apply(actor : ActorRef, local : Traversable[Any]) = new NetworkEntityPath(actor, local)
  def apply(actor : ActorRef) = new NetworkEntityPath(actor, Nil)
}

/********************************************************************************************************
  * Represent an absolute path of a network entity
  *
  * @param actor which is the reference to the container actor of the targeted network entity
  * @param local is the sequence of getIdentifier in order to pass through the local actor hierarchy
  *
  *******************************************************************************************************/
@SerialVersionUID(1L)
class NetworkEntityPath(val actor : ActorRef, val local : Traversable[Any]) extends Serializable {
  def this(actor : ActorRef) = this(actor, Nil)

  def /(child : Any) : NetworkEntityPath = NetworkEntityPath(actor, local++Seq(child))

  override def equals(that : Any) = that match {
    case obj : NetworkEntityPath => actor == obj.actor && local == obj.local
    case _ => false
  }

  override def hashCode() : Int = {
    actor.hashCode()+31*local.hashCode()
  }

  override def toString : String = local.foldLeft(actor.path.name+":")((acc, curr) => acc+"/"+curr.toString)
}

/********************************************************************************************************
  * Represent the reference of a targeted network entity according of the sender
  *******************************************************************************************************/
trait NetworkEntityReference extends Serializable{
  var hasSendMessage = false

  def send(message: Message) : Unit
  def ask(message : Message) : Any

  def answer(value: Any): Unit

  def disableAutoResponse() : Unit = {
    hasSendMessage = true
  }
}

/********************************************************************************************************
  * Specialization of the NetworkEntityReference dedicated to network entities which lives in the same actor.
  * Messages can be directly sent to the object
  *
  * @param target is the direct access to the target network entity
  *******************************************************************************************************/
class LocalNetworkEntityReference(val container : NetworkEntityActor, val target : NetworkEntity, val sender : NetworkEntity) extends NetworkEntityReference {

  def send(message: Message) : Unit = {
    container.addToQueue(message.asInstanceOf[Message], target, new LocalNetworkEntityReference(container, sender, target))
    hasSendMessage = true
  }

  def ask(message : Message) : Any = {
    class DummyReference(var response : Any) extends NetworkEntityReference {
      def send(message: Message) : Unit = throw new UnsupportedOperationException
      def ask(message : Message) : Message = throw new UnsupportedOperationException

      override def answer(value: Any): Unit = response = value
    }
    val sender = new DummyReference(null)
    target.sendMessageFrom(message, sender)
    assert(sender.response != null)
    sender.response
  }

  override def answer(value: Any): Unit = throw new UnsupportedOperationException

  override def toString = "LocalNetworkEntityReference(target="+target.getNetworkAddress+",sender="+sender.getNetworkAddress+")"

}

/********************************************************************************************************
  * Specialization of the NetworkEntityReference dedicated to network entities which lives in different actor
  * Messages need to go through the actor mailbox before reach the destination network entity
  *
  * @param target is the absolute pass of the targeted network entity
  *******************************************************************************************************/
class RemoteNetworkEntityReference(val target : NetworkEntityPath, val sender : NetworkEntityPath) extends NetworkEntityReference with Serializable {

  implicit val timeout = Config.longTimeout

  def send(message: Message) : Unit = {
    target.actor ! ExplicitSenderRoutedMessage(target.local, message, sender)
    hasSendMessage = true
  }

  def ask(message: Message) : Any = {
    Await.result(akka.pattern.ask(target.actor, ImplicitSenderRoutedMessage(target.local, message)), timeout.duration)
  }

  def answer(value: Any): Unit = target.actor ! value

  override def toString = "RemoteNetworkEntityReference(target="+target+",sender="+sender+")"
}

class ExternalNetworkEntityReference(val target : NetworkEntityPath) extends NetworkEntityReference with Serializable {

  implicit val timeout = Config.defaultTimeout

  def send(message: Message) : Unit = {
    target.actor ! ImplicitSenderRoutedMessage(target.local, message)
    hasSendMessage = true
  }

  def ask(message: Message) : Message = {
    Await.result(akka.pattern.ask(target.actor, ImplicitSenderRoutedMessage(target.local, message)), timeout.duration).asInstanceOf[Message]
  }

  def answer(value: Any): Unit = target.actor ! value

  override def toString = "ExternalNetworkEntityReference("+target+")"
}

/**
  * Specialization of the NetworkEntityReference dedicated to unknown network entities
  *
  * @param target is the destination akka Actor
  */
class ExternalActorReference(target : ActorRef) extends NetworkEntityReference {

  implicit val timeout = Config.longTimeout

  def send(message: Message): Unit = {
    target ! message
    hasSendMessage = true
  }

  def ask(message: Message): Message = {
    Await.result(akka.pattern.ask(target, message), timeout.duration).asInstanceOf[Message]
  }

  override def answer(value: Any): Unit = target ! value

  override def toString = "ExternalActorReference("+target.path+")"
}

object TrashNetworkEntityReference extends NetworkEntityReference {

  def send(message: Message) : Unit = {

  }

  def ask(message: Message) : Message = {
    object NullMessage extends Message
    NullMessage
  }

  def answer(value: Any) = {

  }
}

/**
  * Helper for interact with system entities
  */
object ExternalSender {

  implicit val timeout: Timeout = Config.longTimeout

  def sendTo(path : NetworkEntityPath, message: Message) : Unit = {
    path.actor ! ImplicitSenderRoutedMessage(path.local, message)
  }

  def askTo(path : NetworkEntityPath, message: Message, timeout: Timeout = timeout) : Any = {
    Await.result(askFuture(path, message), timeout.duration)
  }

  def askFuture(path : NetworkEntityPath, message: Message) : Future[Any] = {
    akka.pattern.ask(path.actor, ImplicitSenderRoutedMessage(path.local, message.setResponseMode()))
  }

  def getReference(actor: ActorRef) : NetworkEntityReference = new ExternalActorReference(actor)

}