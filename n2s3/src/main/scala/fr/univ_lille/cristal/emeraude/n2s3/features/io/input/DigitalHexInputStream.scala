package fr.univ_lille.cristal.emeraude.n2s3.features.io.input

import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStream

/**
  * Factory for [[DigitalHexInputStream]].
  */
object DigitalHexEntry {

  def apply() = new StreamEntry[SampleInput](Shape(3, 5))
}

/**
  * An input stream that generates hexadecimal chars in a 3x5 picture of binary pixels.
  * This stream is generated in memory.
  */
class DigitalHexInputStream extends InputStream[SampleInput] {


  val characters = List(
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 0, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1),
      List(0, 0, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(0, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 0, 1)
    ),
    List(
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(0, 0, 1),
      List(0, 0, 1),
      List(1, 1, 1),
      List(1, 0, 1),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 0),
      List(1, 0, 0),
      List(1, 1, 1)
    ),
    List(
      List(1, 1, 1),
      List(1, 0, 0),
      List(1, 1, 0),
      List(1, 0, 0),
      List(1, 0, 0)
    )
  )

  val label = List("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F")

  var cursor = 0

  def atEnd() = cursor >= characters.size
  def next() = {
    val seq = for {
      j <- 0 until 3
      i <- 0 until 5
    } yield {
      SampleUnitInput(characters(cursor)(i)(j).toFloat, j, i)
    }
    val next = SampleInput(seq, label(cursor))
    cursor += 1
    next
  }

  def reset() = {
    cursor = 0
  }

  def shape = Shape(3, 5)

  override def toString = "DigHex"
}
