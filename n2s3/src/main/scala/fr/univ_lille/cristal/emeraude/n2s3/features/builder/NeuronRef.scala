package fr.univ_lille.cristal.emeraude.n2s3.features.builder

import fr.univ_lille.cristal.emeraude.n2s3.core._

/**
  * [[NetworkEntityRef]] that refers to a neuron inside the simulation
  * @param group a ref to the neuron group that contains the neuron
  * @param index the index of the neuron in [[group]]
  */
class NeuronRef(val group: NeuronGroupRef, val index: Int) extends NetworkEntityRef {
  def getIdentifier: Any = this.group.getIdentifierOf(this)
  def newNeuron(): NetworkEntity = group.defaultNeuronConstructor()
  def isConnectedTo(aNeuron: NeuronRef) = group.isConnected(this, aNeuron)
}
