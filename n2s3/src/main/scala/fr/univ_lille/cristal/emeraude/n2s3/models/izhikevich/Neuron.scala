/************************************************************************************************************
 * Contributors: 
 * 		- created by falezp on 07/06/16
 ***********************************************************************************************************/
package fr.univ_lille.cristal.emeraude.n2s3.models.izhikevich

import fr.univ_lille.cristal.emeraude.n2s3.core
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{Inhibition, NeuronEnds, NeuronFireEvent, NeuronFireResponse}
import fr.univ_lille.cristal.emeraude.n2s3.core._
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.{ShapelessSpike, WeightedSpike}
import fr.univ_lille.cristal.emeraude.n2s3.core.models.properties.SynapticWeightFloat
import fr.univ_lille.cristal.emeraude.n2s3.core.models.synapse.FloatSynapse
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.{BackwardSpike, QBGNeuronConnection}
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.Time
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.Message

import scala.util.Random

 /**
  * Implementation of the Izhikevich neuron model 
  * 
  * More informations can be found at the following address:
  * 	- http://www.izhikevich.org/publications/spikes.htm
  *   - http://www.izhikevich.org/publications/spikes.pdf
  */ 
class Neuron extends core.Neuron {

  /*
   * Parameters
   */
  val a = 0.02f
  val b = 0.2f
  val c = -65f
  val d = 8f

  var v = c // TODO in mV
  var u = b*v

  var lastTimestamps : Timestamp = 0

  /*
   *  Properties
   */
  addConnectionProperty[Float](SynapticWeightFloat, {
    case s : FloatSynapse => Some(s.getWeight)
    case _ => None
  },
    (connection,  value) => connection match {
      case s : FloatSynapse => s.setWeight(value)
      case _ =>
    })

  /**
    * Allow to use a default connection object in case of no explicit constructor is given on the creation of a new connection
    *
    * @return a neuron connection
    */
  override def defaultConnection: NeuronConnection = new QBGNeuronConnection//new Synapse

  /**
    * Method used to describe the behavior of the neuron model
    *
    * @param timestamp is the current time
    * @param message   is the content
    */
  override def processSomaMessage(timestamp: Timestamp, message: Message, fromSynapse : Option[Int], ends: NeuronEnds): Unit = message match {
    case WeightedSpike(weight) =>

      v += Time(timestamp-lastTimestamps).asMilliSecond*(0.04f*v*v+5.0f*v+140.0f-u+weight+5f*Random.nextGaussian().toFloat)
      u += a*(b*v-u)

      if(v >= 30.0) {
        v = c
        u += d

        triggerEventWith(NeuronFireEvent, NeuronFireResponse(timestamp, getNetworkAddress))
        ends.sendToAllOutput(timestamp, ShapelessSpike)
        ends.sendToAllInput(timestamp, BackwardSpike)
      }

      lastTimestamps = timestamp
    case Inhibition =>
      v = c
      u += d
  }
}
