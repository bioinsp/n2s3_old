package fr.univ_lille.cristal.emeraude.n2s3.features.logging.graph;

import java.awt.Color;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.xeiam.xchart.StyleManager.ChartType;

/**
 * 
 * @author bdanglot
 * Class to display weights of Synapses. Cannot run during simulation.
 */
public class SynapticsConductancesGraph extends AbstractGraph {
	
	/**
	 * List of weights of Synapses
	 */
	private final List<Float> weights = new CopyOnWriteArrayList<Float>();
	
	/**
	 * List full of 0 in order to add the couple (w,0)
	 */
	private final List<Integer> listZero = new CopyOnWriteArrayList<Integer>();
	
	/**
	 * Constructor Call the super() constructor and change the ChartType to
	 * Scatter
	 */
	public  SynapticsConductancesGraph() {
		super();
		this.chart.getStyleManager().setChartType(ChartType.Scatter);
	}
	
	/**
	 * Add content
	 * @param val the value of weights
	 */
	public void addSerie(float val) {
		this.weights.add(val);
		this.listZero.add(0);
	}
	
	/**
	 * @Overriding the superLaunch()
	 */
	public void launch() {
		this.chart.addSeries("Synapses", weights, listZero);
		this.chart.getSeriesMap().get("Synapses").setMarkerColor(new Color(0,0,255,32));
		super.launch();
	}

}
