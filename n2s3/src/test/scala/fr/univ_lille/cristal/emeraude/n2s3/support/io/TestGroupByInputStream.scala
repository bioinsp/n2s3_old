package fr.univ_lille.cristal.emeraude.n2s3.support.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream

/**
  * Created by guille on 6/2/16.
  */
class TestGroupByInputStream extends UnitSpec {
  val testImageFileName: String = this.getClass.getResource("/train2k-images.idx3-ubyte").getFile
  val testLabelFileName: String = this.getClass.getResource("/train2k-labels.idx1-ubyte").getFile

  "GroupBy input stream" should "group all subsequent elements by condition" in {
    val wrappee = new SequenceInputStream(List(1,1,1,1))
    val stream = new GroupByInputStream((x: Int) => x,wrappee)

    stream.next().length shouldBe 4
  }

  "GroupBy input stream" should "stop grouping when value changes" in {
    val wrappee = new SequenceInputStream(List(1,1,1,1, 2))
    val stream = new GroupByInputStream((x: Int) => x,wrappee)

    stream.next().length shouldBe 4
  }

  "GroupBy input stream" should "continue with next group" in {
    val wrappee = new SequenceInputStream(List(1,1,1,1, 2))
    val stream = new GroupByInputStream((x: Int) => x,wrappee)

    stream.next()
    val next = stream.next()
    next.length shouldBe 1
    next.head shouldBe 2
  }

  "GroupBy input stream" should "be at end with inner stream is at end" in {
    val wrappee = new SequenceInputStream(List(1,1,1,1))
    val stream = new GroupByInputStream((x: Int) => x,wrappee)

    stream.next()

    stream shouldBe 'atEnd
  }
}
