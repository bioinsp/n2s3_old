package fr.univ_lille.cristal.emeraude.n2s3.core

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.DuplicatedChildException

/**
  * Created by guille on 8/5/16.
  */
class TestNetworkContainer extends UnitSpec {

  "New Network Container" should "be empty" in {
    val container = new NetworkContainer
    container should be ('empty)
  }

  "Network Container with children" should "not be empty" in {
    val container = new NetworkContainer
    container.addChild("identifier", new MockNeuron)
    container should not be 'empty
  }

  "Network Container" should "not have children with repeated identifiers" in {
    val container = new NetworkContainer
    container.addChild("identifier", new MockNeuron)
    a[DuplicatedChildException] shouldBe thrownBy(container.addChild("identifier", new MockNeuron))
  }

  "Network Container" should "map identifier to child" in {
    val container = new NetworkContainer
    val neuron = new MockNeuron
    container.addChild("identifier", neuron)
    container.childEntityAt("identifier") should be(neuron)
  }

  "Network Container" should "fail to get unknown identifier" in {
    val container = new NetworkContainer
    a[NoSuchElementException] shouldBe thrownBy(container.childEntityAt("identifier"))
  }

  "Network Container hierarchy" should "resolve path of existing child" in {
    val level0 = new NetworkContainer
    val level1 = new NetworkContainer
    val level2 = new NetworkContainer
    level0.addChild("level1", level1)
    level1.addChild("level2", level2)
    level0.resolvePath(Seq("level1", "level2")) should be(level2)
  }

  "Network Container hierarchy" should "fail to resolve path of non-existing child" in {
    val level0 = new NetworkContainer
    val level1 = new NetworkContainer
    val level2 = new NetworkContainer
    level0.addChild("level1", level1)
    level1.addChild("level2", level2)
    a[NoSuchElementException] shouldBe thrownBy(level0.resolvePath(Seq("level1", "level3")))
  }

  "Add child to network container" should "add parent relation in child" in {
    val level0 = new NetworkContainer
    val level1 = new NetworkContainer
    level0.addChild("level1", level1)
    level1.getParent shouldBe level0
  }

  "Network Container root address" should "be empty" in {
    val level0 = new NetworkContainer
    level0.getNetworkAddress shouldBe NetworkEntityPath(null, Seq())
  }

  "Network Container child address" should "be non empty" in {
    val level0 = new NetworkContainer
    val level1 = new NetworkContainer
    level0.addChild("level1", level1)
    level1.getNetworkAddress shouldBe NetworkEntityPath(null, Seq("level1"))
  }

  "Network Container child's child address" should "be non empty" in {
    val level0 = new NetworkContainer
    val level1 = new NetworkContainer
    val level2 = new NetworkContainer
    level0.addChild("level1", level1)
    level1.addChild("level2", level2)
    level2.getNetworkAddress shouldBe NetworkEntityPath(null, Seq("level1", "level2"))
  }
}
