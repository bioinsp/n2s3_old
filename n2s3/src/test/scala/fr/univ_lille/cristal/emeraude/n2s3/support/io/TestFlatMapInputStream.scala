package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.support.io.InputStreamCombinators._

/**
  * Created by guille on 6/2/16.
  */
class TestFlatMapInputStream extends UnitSpec {
  val testImageFileName: String = this.getClass.getResource("/train2k-images.idx3-ubyte").getFile
  val testLabelFileName: String = this.getClass.getResource("/train2k-labels.idx1-ubyte").getFile

  "Flatmap an empty array" should "be at end" in {
    val stream = InputStream(new Array[Seq[Int]](0)).flatMap(array => array)
    stream shouldBe 'atEnd
  }

  "Flatmap a single empty array" should "be at end" in {
    val stream = InputStream(Array(new Array[Int](0))).flatMap(array => array)
    stream shouldBe 'atEnd
  }

  "Flatmap a single array inside an array" should "return the elements in the inner array" in {
    val wrappee = InputStream(Array(Array(1,2,3)))
    val stream = wrappee.flatMap( array => array)
    stream.next() shouldBe 1
    stream.next() shouldBe 2
    stream.next() shouldBe 3
    stream shouldBe 'atEnd
  }

  "Flatmap several arrays inside an array" should "return the elements inside both inner arrays" in {
    val wrappee = InputStream(Array(Array(1,2,3), Array(4,5,6)))
    val stream = wrappee.flatMap( array => array)
    stream.next() shouldBe 1
    stream.next() shouldBe 2
    stream.next() shouldBe 3
    stream.next() shouldBe 4
    stream.next() shouldBe 5
    stream.next() shouldBe 6
    stream shouldBe 'atEnd
  }
}
