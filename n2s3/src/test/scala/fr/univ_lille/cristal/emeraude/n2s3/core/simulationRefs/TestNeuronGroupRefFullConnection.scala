package fr.univ_lille.cristal.emeraude.n2s3.core.simulationRefs

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.types.FullConnection
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef}

/**
  * Created by guille on 8/5/16.
  */
class TestNeuronGroupRefFullConnection extends UnitSpec{

  "Full connection between neuron groups A and B" should "connect all in A to all in B" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)

    groupA.connectTo(groupB, new FullConnection)

    for(
      aNeuron <- groupA.neurons;
      bNeuron <- groupB.neurons
    ){
      aNeuron.isConnectedTo(bNeuron) shouldBe true
    }
  }

  "Full connection between neuron groups A and B" should "not connect neurons in A to neurons in A" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)

    groupA.connectTo(groupB, new FullConnection)

    for(
      aNeuron <- groupA.neurons;
      bNeuron <- groupA.neurons
    ){
      aNeuron.isConnectedTo(bNeuron) shouldBe false
    }
  }

  "Full connection between neuron groups A and B" should "not connect neurons to a third neuron group" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupB = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)
    val groupC = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(2)

    groupA.connectTo(groupB, new FullConnection)

    for(
      aNeuron <- groupA.neurons;
      bNeuron <- groupC.neurons
    ){
      aNeuron.isConnectedTo(bNeuron) shouldBe false
    }
  }

  "Full connection between same group" should "connect all in A to all others except itself" in {
    val groupA = new NeuronGroupRef(new N2S3()).setNumberOfNeurons(4)

    groupA.connectTo(groupA, new FullConnection)

    for(
      aNeuron <- groupA.neurons;
      bNeuron <- groupA.neurons
    ){
      aNeuron.isConnectedTo(bNeuron) shouldBe (aNeuron != bNeuron)
    }
  }
}
