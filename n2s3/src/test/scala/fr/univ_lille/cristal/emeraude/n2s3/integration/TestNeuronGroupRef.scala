package fr.univ_lille.cristal.emeraude.n2s3.integration

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.QBGNeuron
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorPerNeuronGroupPolicy, ActorPerNeuronPolicy, SingleActorPolicy}

/**
  * Created by guille on 9/1/16.
  */
class TestNeuronGroupRef extends UnitSpec {

  "Deploying a Neuron Group with per neuron actor policy" should "create one actor per neuron" in {
    val n2s3 = new N2S3

    val neuronGroup = new NeuronGroupRef(n2s3)
    neuronGroup.setNeuronModel(QBGNeuron)
    neuronGroup.setNumberOfNeurons(2)

    neuronGroup.setActorPolicy(new ActorPerNeuronPolicy(n2s3))
    neuronGroup.ensureActorDeployed(n2s3)

    val firstAddress = neuronGroup.neurons.head.getNetworkAddress
    val secondAddress = neuronGroup.neurons(1).getNetworkAddress

    firstAddress.actor shouldNot be(secondAddress.actor)
  }

  "Deploying a Neuron Group with per group actor policy" should "create single actor" in {
    val n2s3 = new N2S3

    val neuronGroup = new NeuronGroupRef(n2s3)
    neuronGroup.setNeuronModel(QBGNeuron)
    neuronGroup.setNumberOfNeurons(2)

    neuronGroup.setActorPolicy(new ActorPerNeuronGroupPolicy(n2s3))
    neuronGroup.ensureActorDeployed(n2s3)

    val firstAddress = neuronGroup.neurons.head.getNetworkAddress
    val secondAddress = neuronGroup.neurons(1).getNetworkAddress

    firstAddress.actor should be(secondAddress.actor)
  }

  "Deploying a Neuron Group with per group actor policy" should "create different actors for different groups" in {
    val n2s3 = new N2S3
    val policy = new ActorPerNeuronGroupPolicy(n2s3)

    val neuronGroup = new NeuronGroupRef(n2s3)
    neuronGroup.setNeuronModel(QBGNeuron)
    neuronGroup.setNumberOfNeurons(1)
    neuronGroup.setActorPolicy(policy)

    val otherNeuronGroup = new NeuronGroupRef(n2s3)
    otherNeuronGroup.setNeuronModel(QBGNeuron)
    otherNeuronGroup.setNumberOfNeurons(1)
    otherNeuronGroup.setActorPolicy(policy)

    neuronGroup.ensureActorDeployed(n2s3)
    otherNeuronGroup.ensureActorDeployed(n2s3)

    val firstAddress = neuronGroup.neurons.head.getNetworkAddress
    val secondAddress = otherNeuronGroup.neurons.head.getNetworkAddress

    firstAddress.actor shouldNot be(secondAddress.actor)
  }

  "Deploying a Neuron Group with single actor policy" should "create single actor for different groups" in {
    val n2s3 = new N2S3
    val policy = new SingleActorPolicy(n2s3)

    val neuronGroup = new NeuronGroupRef(n2s3)
    neuronGroup.setIdentifier("test1")
    neuronGroup.setNeuronModel(QBGNeuron)
    neuronGroup.setNumberOfNeurons(1)
    neuronGroup.setActorPolicy(policy)

    val otherNeuronGroup = new NeuronGroupRef(n2s3)
    otherNeuronGroup.setIdentifier("test2")
    otherNeuronGroup.setNeuronModel(QBGNeuron)
    otherNeuronGroup.setNumberOfNeurons(1)
    otherNeuronGroup.setActorPolicy(policy)

    neuronGroup.ensureActorDeployed(n2s3)
    otherNeuronGroup.ensureActorDeployed(n2s3)

    val firstAddress = neuronGroup.neurons.head.getNetworkAddress
    val secondAddress = otherNeuronGroup.neurons.head.getNetworkAddress

    firstAddress.actor should be(secondAddress.actor)
  }

}
