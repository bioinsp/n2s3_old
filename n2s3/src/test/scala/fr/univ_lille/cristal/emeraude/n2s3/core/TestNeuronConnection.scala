package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{SetSynchronizer, CreateNeuronConnectionWith}
import fr.univ_lille.cristal.emeraude.n2s3.models.qbg.{QBGNeuronActor, QBGNeuron}
import fr.univ_lille.cristal.emeraude.n2s3.{UnitActorSpec, UnitSpec}
import org.scalatest.Matchers

/**
  * First step towards real tests for N2S3.
  * Using Akka-TestKit, which provides
  * - synchronous messaging to actors and;
  * - access to the underlying actor object to send normal scala messages.
  *
  * Created by Guillermo Polito on 24/03/16.
  */
class TestNeuronConnection extends UnitActorSpec {

  "* New neuron" should "have 0 input and output connections" in {
    val n1 = TestActorRef(new MockNeuronActor)

    n1.underlyingActor.entity.asInstanceOf[MockNeuron].getNumberOfInputConnections shouldBe 0
    n1.underlyingActor.entity.asInstanceOf[MockNeuron].getNumberOfOutputConnections shouldBe 0
  }

  "* In network N1 -> N2, with N1 and N2 in different actors, N1" should "have 1 remote output connection" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val synchronizerPath = NetworkEntityPath(aSynchronizerActor)
    val n1 = TestActorRef(new MockNeuronActor)
    n1 ! SetSynchronizer(synchronizerPath)

    val n2 = TestActorRef(new MockNeuronActor)
    n2 ! SetSynchronizer(synchronizerPath)

    ExternalSender.sendTo(new NetworkEntityPath(n1), CreateNeuronConnectionWith(new NetworkEntityPath(n2)))

    val neuron1 = n1.underlyingActor
    neuron1.entity.asInstanceOf[MockNeuron].getNumberOfOutputConnections shouldBe 1

    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe aSynchronizerActor
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe n1
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe Nil

    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe n2
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe aSynchronizerActor
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe Nil

    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).connectionId shouldBe 0
    neuron1.entity.asInstanceOf[MockNeuron].getOutputConnectionById(0).path shouldBe NetworkEntityPath(n2)
  }

  "* In network N1 -> N2, with N1 and N2 in different actors, N2" should "have 1 remote input connection" in {
    val aSynchronizerActor = TestActorRef[SynchronizerActor]
    val synchronizerPath = NetworkEntityPath(aSynchronizerActor)

    val n1 = TestActorRef(new MockNeuronActor)
    n1 ! SetSynchronizer(synchronizerPath)

    val n2 = TestActorRef(new MockNeuronActor)
    n2 ! SetSynchronizer(synchronizerPath)

    ExternalSender.sendTo(new NetworkEntityPath(n1), CreateNeuronConnectionWith(new NetworkEntityPath(n2)))

    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe aSynchronizerActor
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe n2
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe Nil

    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe n1
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe aSynchronizerActor
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe Nil

    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).connection shouldBe an[MockNeuronConnection]
    n2.underlyingActor.entity.asInstanceOf[MockNeuron].getInputConnectionById(0).path shouldBe NetworkEntityPath(n1)

  }

}