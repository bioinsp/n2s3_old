package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream

/**
  * Created by guille on 6/2/16.
  */
class TestOptionInputStream extends UnitSpec {
  val testImageFileName: String = this.getClass.getResource("/train2k-images.idx3-ubyte").getFile
  val testLabelFileName: String = this.getClass.getResource("/train2k-labels.idx1-ubyte").getFile

  "Option input stream" should "return Some element if available" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new OptionInputStream(wrappee)
    stream.next() shouldNot be(None)
  }

  "Option input stream" should "return None if element not available" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new OptionInputStream(wrappee)
    stream next wrappee.numberImage
    stream.next() should be(None)
  }

  "Option input stream" should "avoid None when asking many elements" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new OptionInputStream(wrappee)
    stream.next(wrappee.numberImage +1).flatten.length should be(wrappee.numberImage)
  }
}
