package fr.univ_lille.cristal.emeraude.n2s3.support.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.MnistFileInputStream

/**
  * Created by guille on 6/2/16.
  */
class TestRepeatInputStream extends UnitSpec {
  val testImageFileName: String = this.getClass.getResource("/train2k-images.idx3-ubyte").getFile
  val testLabelFileName: String = this.getClass.getResource("/train2k-labels.idx1-ubyte").getFile

  "Repeat input stream 1 times" should "yield the wrappee" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new RepeatInputStream(wrappee)
    stream.next(wrappee.numberImage)
  }

  "Repeat input stream 1 times" should "be at end after one repetition" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new RepeatInputStream(wrappee)
    stream.next(wrappee.numberImage)
    stream shouldBe 'atEnd
  }

  "Repeat input stream 2 times" should "not be at the end after one iteration" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new RepeatInputStream(wrappee, 2)
    stream.next(wrappee.numberImage)
    stream shouldNot be ('atEnd)
  }

  "Repeat input stream 2 times" should "yield the wrappee twice" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new RepeatInputStream(wrappee, 2)
    stream.next(wrappee.numberImage)
    stream.next(wrappee.numberImage)
  }

  "Repeat input stream 2 times" should "be at the end after two iterations" in {
    val wrappee = new MnistFileInputStream(testImageFileName, testLabelFileName)
    val stream = new RepeatInputStream(wrappee, 2)
    stream.next(wrappee.numberImage)
    stream.next(wrappee.numberImage)
    stream shouldBe 'atEnd
  }
}
