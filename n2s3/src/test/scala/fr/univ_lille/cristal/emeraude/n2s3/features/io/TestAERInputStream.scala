/**
  * contributors:
  * damien.marchal@univ-lille1.fr, Copyright CNRS 2016
  */
package fr.univ_lille.cristal.emeraude.n2s3.features.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{AERFileReader, AERInputStream, Shape}


class TestAERInputStream extends UnitSpec {
  val filename = this.getClass.getResource("/aerdata/freeway2.mat.dat").getFile

  //TODO: Using the following file makes the tests fail. Is the file right?
  //val filename = "src/test/resources/aerdata/freeway.mat.dat"

  "AERInputStream" should "not be at the end of a not read file" in {

    val a = new AERFileReader(filename)

    println(a.nextEvents(128).mkString(", "))

    val inputStream = new AERInputStream(filename, Shape(1))

    val r = inputStream.next
    println(r.map(e => (e.address, e.timestamp)).mkString(", "))



    inputStream shouldNot be ('atEnd)
  }

  "AERInputStream" should "be at the end of a read file" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128), 1)

    for(i <- 0 until inputStream.numberImage)
      inputStream.next()

    inputStream should be ('atEnd)
  }

  "AERInputStream" should "know number of images in file" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128))
    inputStream.numberImage shouldBe 6505992
  }

  "AERInputStream next" should "be able to read all images without throwing an exception" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128), 1)
    for (i <- 0 until inputStream.numberImage) {
        inputStream.next()
    }
  }

  "AERInputStream next" should "throw an exception after end of file" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128), 1)
    for (i <- 0 until inputStream.numberImage) {
      inputStream.next()
    }
    inputStream should be ('atEnd)
    intercept[IOException](inputStream.next())
  }

  "AERInputStream reset" should "take it back to the beginning" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128), 1)
    val first = inputStream.next()
    inputStream.reset()
    first should equal (inputStream.next())
  }

  "AERInputStream reset" should "take it back to the beginning When everything was read before" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128), 1)
    inputStream.next(inputStream.numberImage)
    inputStream.reset()
    inputStream.next()
  }

  "AERInputStream next with arguments" should "return a list of the desired size" in {
    val inputStream = new AERInputStream(filename, Shape(128, 128))
    inputStream.next(2000).length shouldBe 2000
  }

  "AERInputStream" should "throw an IOException if initialized with a non existing file" in {
    a[IOException] should be thrownBy {
      val reader = new AERInputStream("src/test/resources/nonexistingfile", Shape(128, 128))
    }
  }
}