package fr.univ_lille.cristal.emeraude.n2s3.core

import akka.testkit.TestActorRef
import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkEntityActor.AddChildEntity
import fr.univ_lille.cristal.emeraude.n2s3.core.Neuron.{CreateNeuronConnectionWith, NeuronMessage, SetSynchronizer}
import fr.univ_lille.cristal.emeraude.n2s3.core.actors.NetworkContainerActor

/**
  * Created by guille on 4/7/16.
  */
class TestNeuronLayerActorConnection extends UnitActorSpec {

  "Connection between two NetworkLayerActor's" should "add both remote input and output entry" in {
    val synchronizer = TestActorRef[SynchronizerActor]
    val synchronizerPath = NetworkEntityPath(synchronizer)

    val neuronLayer1 = TestActorRef[NetworkContainerActor]
    val neuron1Path = NetworkEntityPath(neuronLayer1)/"neuron1"

    neuronLayer1 ! AddChildEntity("neuron1", new MockNeuron)
    ExternalSender.sendTo(neuron1Path, SetSynchronizer(synchronizerPath))

    val neuronLayer2 = TestActorRef[NetworkContainerActor]
    val neuron2Path = NetworkEntityPath(neuronLayer2)/"neuron2"
    neuronLayer2 ! AddChildEntity("neuron2", new MockNeuron)

    ExternalSender.sendTo(neuron2Path, SetSynchronizer(synchronizerPath))

    ExternalSender.askTo(neuron1Path, CreateNeuronConnectionWith(neuron2Path))

    // Output connection
    val neuron1 = neuronLayer1.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron1").asInstanceOf[MockNeuron]
    neuron1.getNumberOfInputConnections shouldBe 0
    neuron1.getNumberOfOutputConnections shouldBe 1

    // N1 to synchronizer
    neuron1.getOutputConnectionById(0).preSyncRef shouldBe an[RemoteNetworkEntityReference]
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe synchronizer
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe neuronLayer1
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe List("neuron1")

    // Synchronizer to N2
    neuron1.getOutputConnectionById(0).postSyncRef shouldBe an[RemoteNetworkEntityReference]
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe neuronLayer2
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe List("neuron2")
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.actor shouldBe synchronizer
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].sender.local shouldBe Nil

    neuron1.getOutputConnectionById(0).connectionId shouldBe 0
    neuron1.getOutputConnectionById(0).path shouldBe neuron2Path

    // Input connection
    val neuron2 = neuronLayer2.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron]

    neuron2.getNumberOfInputConnections shouldBe 1
    neuron2.getInputConnectionById(0).preSyncRef shouldBe an[RemoteNetworkEntityReference]
    neuron2.getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe synchronizer
    neuron2.getInputConnectionById(0).preSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe Nil
    neuron2.getInputConnectionById(0).postSyncRef shouldBe an[RemoteNetworkEntityReference]
    neuron2.getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.actor shouldBe neuronLayer1
    neuron2.getInputConnectionById(0).postSyncRef.asInstanceOf[RemoteNetworkEntityReference].target.local shouldBe List("neuron1")

    neuron2.getNumberOfOutputConnections shouldBe 0
  }

  "Connection between two entity in the same actor" should "add both local input and output entry" in {
    val containerActor = TestActorRef[NetworkContainerActor]

    val synchronizerPath = NetworkEntityPath(containerActor)/"synchronizer"
    containerActor ! AddChildEntity("synchronizer", new Synchronizer)

    val neuron1Path = NetworkEntityPath(containerActor)/"neuron1"
    containerActor ! AddChildEntity("neuron1", new MockNeuron)
    ExternalSender.sendTo(neuron1Path, SetSynchronizer(synchronizerPath))

    val neuron2Path = NetworkEntityPath(containerActor)/"neuron2"
    containerActor ! AddChildEntity("neuron2", new MockNeuron)
    ExternalSender.sendTo(neuron2Path, SetSynchronizer(synchronizerPath))


    ExternalSender.askTo(neuron1Path, CreateNeuronConnectionWith(neuron2Path))

    val neuron1 = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron1").asInstanceOf[MockNeuron]
    val neuron2 = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron]
    val synchronizer = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("synchronizer").asInstanceOf[Synchronizer]

    // Output connection
    neuron1.getNumberOfInputConnections shouldBe 0
    neuron1.getNumberOfOutputConnections shouldBe 1

    // N1 to synchronizer
    neuron1.getOutputConnectionById(0).preSyncRef shouldBe an[LocalNetworkEntityReference]
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[LocalNetworkEntityReference].target shouldBe synchronizer
    neuron1.getOutputConnectionById(0).preSyncRef.asInstanceOf[LocalNetworkEntityReference].sender shouldBe neuron1

    // synchronizer to N2
    neuron1.getOutputConnectionById(0).postSyncRef shouldBe an[LocalNetworkEntityReference]
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[LocalNetworkEntityReference].target shouldBe neuron2
    neuron1.getOutputConnectionById(0).postSyncRef.asInstanceOf[LocalNetworkEntityReference].sender shouldBe synchronizer

    neuron1.getOutputConnectionById(0).connectionId shouldBe 0
    neuron1.getOutputConnectionById(0).path shouldBe neuron2Path

    // Input connection
    neuron2.getNumberOfInputConnections shouldBe 1
    neuron2.getInputConnectionById(0).preSyncRef shouldBe an[LocalNetworkEntityReference]
    neuron2.getInputConnectionById(0).preSyncRef.asInstanceOf[LocalNetworkEntityReference].target shouldBe synchronizer
    neuron2.getInputConnectionById(0).preSyncRef.asInstanceOf[LocalNetworkEntityReference].sender shouldBe neuron2
    neuron2.getInputConnectionById(0).postSyncRef.asInstanceOf[LocalNetworkEntityReference].target shouldBe neuron1
    neuron2.getInputConnectionById(0).postSyncRef.asInstanceOf[LocalNetworkEntityReference].sender shouldBe synchronizer

    neuron2.getNumberOfOutputConnections shouldBe 0
  }


  "Connection between two local NetworkLayerActor's" should "transmit Spike if neurons are connected" in {
    val containerActor = TestActorRef[NetworkContainerActor]

    val synchronizerPath = NetworkEntityPath(containerActor)/"synchronizer"
    containerActor ! AddChildEntity("synchronizer", new Synchronizer)

    val neuron1Path = NetworkEntityPath(containerActor)/"neuron1"
    containerActor ! AddChildEntity("neuron1", new MockNeuron)
    ExternalSender.sendTo(neuron1Path, SetSynchronizer(synchronizerPath))

    val neuron2Path = NetworkEntityPath(containerActor)/"neuron2"
    containerActor ! AddChildEntity("neuron2", new MockNeuron)
    ExternalSender.sendTo(neuron2Path, SetSynchronizer(synchronizerPath))

    ExternalSender.askTo(neuron1Path, CreateNeuronConnectionWith(neuron2Path))
    ExternalSender.askTo(neuron1Path, NeuronMessage(0, MockSpike, null, null, None))

    val neuron1 = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron1").asInstanceOf[MockNeuron]
    val neuron2 = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron]
    val synapse = containerActor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron].getInputConnectionById(0).connection.asInstanceOf[MockNeuronConnection]

    neuron1.lastReceivedMessage shouldBe MockSpike
    neuron1.lastReceivedTimestamp shouldBe 0

    synapse.lastReceivedMessage shouldBe MockSpike
    synapse.lastReceivedTimestamp shouldBe 1

    neuron2.lastReceivedMessage shouldBe MockSpike
    neuron2.lastReceivedTimestamp shouldBe 2
  }

  "Connection between two remote NetworkLayerActor's" should "transmit Spike if neurons are connected" in {
    val synchronizerActor = TestActorRef[NetworkContainerActor]
    val synchronizerPath = NetworkEntityPath(synchronizerActor)/"synchronizer"
    synchronizerActor ! AddChildEntity("synchronizer", new Synchronizer)

    val neuron1Actor = TestActorRef[NetworkContainerActor]
    val neuron1Path = NetworkEntityPath(neuron1Actor)/"neuron1"
    neuron1Actor ! AddChildEntity("neuron1", new MockNeuron)
    ExternalSender.sendTo(neuron1Path, SetSynchronizer(synchronizerPath))

    val neuron2Actor = TestActorRef[NetworkContainerActor]
    val neuron2Path = NetworkEntityPath(neuron2Actor)/"neuron2"
    neuron2Actor ! AddChildEntity("neuron2", new MockNeuron)
    ExternalSender.sendTo(neuron2Path, SetSynchronizer(synchronizerPath))


    ExternalSender.askTo(neuron1Path, CreateNeuronConnectionWith(neuron2Path))
    ExternalSender.askTo(neuron1Path, NeuronMessage(0, MockSpike, null, null, None))

    val neuron1 = neuron1Actor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron1").asInstanceOf[MockNeuron]
    val neuron2 = neuron2Actor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron]
    val synapse = neuron2Actor.underlyingActor.entity.asInstanceOf[NetworkContainer].childEntityAt("neuron2").asInstanceOf[MockNeuron].getInputConnectionById(0).connection.asInstanceOf[MockNeuronConnection]

    neuron1.lastReceivedMessage shouldBe MockSpike
    neuron1.lastReceivedTimestamp shouldBe 0

    synapse.lastReceivedMessage shouldBe MockSpike
    synapse.lastReceivedTimestamp shouldBe 1

    neuron2.lastReceivedMessage shouldBe MockSpike
    neuron2.lastReceivedTimestamp shouldBe 2
  }
}