/**
  * contributors:
  * damien.marchal@univ-lille1.fr, Copyright CNRS 2016
  */
package fr.univ_lille.cristal.emeraude.n2s3.features.io

import java.io.IOException

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input.{DigitalHexInputStream, MnistFileInputStream}

class TestDigitalHexInputStream extends UnitSpec {

  val numberImage = 16

  "New DigitalHexInputStream" should "not be at the end" in {
    val inputStream = new DigitalHexInputStream()
    inputStream shouldNot be ('atEnd)
  }

  "New DigitalHexInputStream" should "be at the end of a read file" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.next(numberImage)
    inputStream should be ('atEnd)
  }

  "DigitalHexInputStream" should "know number of rows per image" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.shape(0) shouldBe 3
  }

  "DigitalHexInputStream" should "know number of columns per image" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.shape(1) shouldBe 5
  }

  "DigitalHexInputStream next" should "return an image" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.next().sample.length shouldBe inputStream.shape(0) * inputStream.shape(1)
  }

  "DigitalHexInputStream next" should "be able to read all images without throwing an exception" in {
    val inputStream = new DigitalHexInputStream()
    for (i <- 0 until numberImage) {
      inputStream.next()
    }
  }

  "DigitalHexInputStream next" should "throw an exception after end of file" in {
    val inputStream = new DigitalHexInputStream()
    for (i <- 0 until numberImage) {
      inputStream.next()
    }
    intercept[IndexOutOfBoundsException](inputStream.next())
  }

  "DigitalHexInputStream reset" should "take it back to the beginning" in {
    val inputStream = new DigitalHexInputStream()
    val first = inputStream.next()
    inputStream.reset()
    first should equal (inputStream.next())
  }

  "DigitalHexInputStream reset" should "take it back to the beginning When everything was read before" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.next(numberImage)
    inputStream.reset()
    inputStream.next()
  }

  "DigitalHexInputStream next with arguments" should "return a list of the desired size" in {
    val inputStream = new DigitalHexInputStream()
    inputStream.next(8).length shouldBe 8
  }
}