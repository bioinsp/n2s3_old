package fr.univ_lille.cristal.emeraude.n2s3.integration

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.core.MockNeuron
import fr.univ_lille.cristal.emeraude.n2s3.core.exceptions.N2S3Exception
import fr.univ_lille.cristal.emeraude.n2s3.features.builder._
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.connection.{Connection, ConnectionPolicy}
import fr.univ_lille.cristal.emeraude.n2s3.features.io.input._
import fr.univ_lille.cristal.emeraude.n2s3.support.UnitCast._
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.{ActorPerNeuronGroupPolicy, LocalActorDeploymentStrategy}
import fr.univ_lille.cristal.emeraude.n2s3.support.io.{InputStream, SequenceInputStream}



/**
  * Created by guille on 6/2/16.
  */
class TestIntegrationN2S3 extends UnitSpec {
  def newTestStreamEntry() = {
    val stream = new StreamEntry[SampleInput](Shape(1, 2))
    stream.append(this.newTestInputStream)
    stream >> new SampleToSpikeTrainConverter(0, 23, 150 MilliSecond, 350 MilliSecond)
  }

  def newTestInputStream: InputStream[SampleInput] = {
    new InputStream[SampleInput] {
      val stream = new SequenceInputStream[SampleInput](List(SampleInput(label = "label", sample = List(SampleUnitInput(1, 0, 1), SampleUnitInput(1, 0, 1)))))

      override def next(): SampleInput = stream.next()

      override def atEnd(): Boolean = stream.atEnd()

      override def reset(): Unit = stream.reset()
    }
  }

  "Simulator without input" should "fail" in {
    val n2s3 = new N2S3()
    a[N2S3Exception] shouldBe thrownBy(n2s3.run())
  }

  "Simulator with input stream" should "consume entire stream after end of activity" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()

    inputStream shouldNot be('atEnd)

    val input = n2s3.createInput(inputStream)
    val layer = n2s3.createNeuronGroup("group", 2)
      .setNeuronModel(MockNeuron)
    input.connectTo(layer)
    n2s3.runAndWait()

    inputStream shouldBe 'atEnd
  }

  "Run a simulator twice" should "not recreate input actor" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()

    val inputLayer = n2s3.createInput(inputStream)
    n2s3
      .createNeuronGroup("group", 2)
      .setNeuronModel(MockNeuron)
    n2s3.run()

    val actorBeforeSecondStart = inputLayer.actorPath.get.actor
    n2s3.run()

    val actorAfterSecondStart = inputLayer.actorPath.get.actor
    actorBeforeSecondStart shouldBe actorAfterSecondStart
  }

  "Run a simulator twice" should "not recreate non input actors" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()

    n2s3.createInput(inputStream)
    val rbmLayer = n2s3.createNeuronGroup("group", 10)
      .setActorPolicy(new ActorPerNeuronGroupPolicy(LocalActorDeploymentStrategy))
      .setNeuronModel(MockNeuron)
    n2s3.run()

    val actorBeforeSecondStart = rbmLayer.actorPath.get.actor
    n2s3.run()

    val actorAfterSecondStart = rbmLayer.actorPath.get.actor
    actorBeforeSecondStart shouldBe actorAfterSecondStart
  }

  "Run a simulator twice" should "not recreate connections" in {
    val n2s3 = new N2S3(new TestActorSystem)
    val inputStream = this.newTestStreamEntry()

    val inputLayer = n2s3.createInput(inputStream)
    val rbmLayer = n2s3.createNeuronGroup("group", 10)
      .setNeuronModel(MockNeuron)

    var called = 0
    inputLayer.connectTo(rbmLayer, new ConnectionPolicy{
      override def generate(from: NeuronGroupRef, to: NeuronGroupRef): Traversable[Connection] = {
        called = called + 1
        List()
      }

      /** ******************************************************************************************************************
        * Testing
        * *****************************************************************************************************************/
      override def connects(aNeuron: NeuronRef, anotherNeuron: NeuronRef): Boolean = false
    })

    n2s3.run()
    n2s3.run()

    called shouldBe 1
  }

  "Run a simulator twice" should "not recreate observer actors" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()

    val inputLayer = n2s3.createInput(inputStream)
    val layer = n2s3.createNeuronGroup("group", 2)
      .setNeuronModel(MockNeuron)
    n2s3.createSynapseWeightGraphOn(inputLayer, layer)

    n2s3.run()

    val actorBeforeSecondStart = n2s3.layerObservers(0).getActor
    n2s3.run()
    val actorAfterSecondStart = n2s3.layerObservers(0).getActor

    actorBeforeSecondStart shouldBe actorAfterSecondStart
  }

  "Wait end of activity" should "not block if run finishes before" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()

    val input = n2s3.createInput(inputStream)
    val layer = n2s3.createNeuronGroup("group", 2)
      .setNeuronModel(MockNeuron)
    input.connectTo(layer)

    n2s3.run()
    n2s3.waitEndOfActivity(1000)
  }


  "Simulator without output layer" should "fail" in {
    val n2s3 = new N2S3()
    a[N2S3Exception] shouldBe thrownBy(n2s3.run())
  }

  "Change input stream" should "change stream in actor if already deployed" in {
    val n2s3 = new N2S3()
    val inputStream = this.newTestStreamEntry()
    n2s3.createInput(inputStream)
    n2s3.createNeuronGroup("group", 2)
        .setNeuronModel(MockNeuron)
    n2s3.run()

    inputStream.append(this.newTestInputStream)
    n2s3.run()

    inputStream shouldBe 'atEnd
  }

}
