package fr.univ_lille.cristal.emeraude.n2s3.core.simulationRefs

import fr.univ_lille.cristal.emeraude.n2s3.UnitActorSpec
import fr.univ_lille.cristal.emeraude.n2s3.features.builder.{N2S3, NeuronGroupRef}

/**
  * Created by guille on 8/5/16.
  */
class TestNeuronGroupRef extends UnitActorSpec {

  "New neuron group" should "be empty" in {
    val group = new NeuronGroupRef(new N2S3())
    group.isEmpty shouldBe true
  }

  "New neuron group" should "not be deployed" in {
    val group = new NeuronGroupRef(new N2S3())
    group.isDeployed shouldBe false
  }

  "New neuron group" should "not have identifier" in {
    val group = new NeuronGroupRef(new N2S3())
    group.hasIdentifier shouldBe false
  }

  "Neuron group with neurons" should "not be empty" in {
    val group = new NeuronGroupRef(new N2S3())
    group.setNumberOfNeurons(10)
    group.isEmpty shouldBe false
  }

  "Neuron group with identifier" should "have identifier" in {
    val group = new NeuronGroupRef(new N2S3())
    group.setIdentifier("Test neuron group")
    group.hasIdentifier shouldBe true
  }

  "Neurons in neuron group" should "have correct index" in {
    val group = new NeuronGroupRef(new N2S3())
    group.setNumberOfNeurons(10)

    group.neurons.zipWithIndex.foreach{ tuple =>
      tuple._1.index == tuple._2 shouldBe true
    }

  }
}
