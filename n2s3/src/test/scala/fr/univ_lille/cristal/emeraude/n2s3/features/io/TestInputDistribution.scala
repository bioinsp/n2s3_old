package fr.univ_lille.cristal.emeraude.n2s3.features.io

import fr.univ_lille.cristal.emeraude.n2s3.UnitSpec
import fr.univ_lille.cristal.emeraude.n2s3.support.GlobalTypesAlias.Timestamp
import fr.univ_lille.cristal.emeraude.n2s3.support.InputDistribution

import scala.util.Random

/**
  * Created by falezp on 24/03/16.
  */
class TestInputDistribution extends UnitSpec {

  "regular distribution of n average" should "return floor(n) timestamps between [start; end] regular spaced" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.0f
    val list = InputDistribution.regular(start, end, n)

    assert(list.size == math.floor(n).toInt)

    list.zipWithIndex.foreach { case (timestamp, index) =>
      assert(timestamp == start+(index*(end-start).toDouble/math.floor(n)).toLong)
    }
  }

  "uniform distribution of n average" should "return floor(n) timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.uniform(new Random(), start, end, n)

    assert(list.size == math.floor(n).toInt)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "gaussian distribution of n average" should "return floor(n) timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.gaussian(new Random(), start, end, n)

    assert(list.size == math.floor(n).toInt)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "poisson distribution of n average" should "return timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.poisson(new Random(), start, end, n)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

  "jitter distribution of n average" should "return timestamps between [start; end]" in {
    val start : Timestamp = 100
    val end : Timestamp = 200
    val n = 10.5f
    val list = InputDistribution.jitter(new Random(32), start, end, n, 0.1)

    list.foreach { timestamp =>
      assert(timestamp >= start && timestamp <= end)
    }
  }

}