package fr.univ_lille.cristal.emeraude.n2s3.cluster

import akka.actor.{Address, Deploy}
import akka.cluster.{Cluster, MemberStatus}
import akka.remote.RemoteScope
import fr.univ_lille.cristal.emeraude.n2s3.support.actors.ActorDeploymentStrategy

import scala.util.Random

class RandomActorDeploymentStrategy(cluster: Cluster) extends ActorDeploymentStrategy {
  override def getDeployForActor(): Deploy = {
    val members = cluster.state.members.filter(_.status == MemberStatus.Up)
    val member = members.toList(new Random().nextInt(members.size))
    Deploy(scope = RemoteScope(member.address))
  }
}

class ExplicitActorDeploymentStrategy(address: Address) extends ActorDeploymentStrategy {
  override def getDeployForActor(): Deploy = {
    Deploy(scope = RemoteScope(address))
  }
}