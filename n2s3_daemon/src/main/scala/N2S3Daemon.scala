import akka.actor.ActorSystem

/**
  * Created by guille on 7/28/16.
  */
object N2S3Daemon extends App {
  val system: ActorSystem = ActorSystem.create("N2S3ClusterSystem")
}
