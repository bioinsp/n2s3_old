// create eclipse definition project
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.5.0")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.3")
addSbtPlugin("com.typesafe.sbt" % "sbt-multi-jvm" % "0.3.11")

//Signing for publishing in Sonatype
addSbtPlugin("com.typesafe.sbt" % "sbt-pgp" % "0.8.3")