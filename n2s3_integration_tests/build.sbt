import com.typesafe.sbt.SbtMultiJvm
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.scalatestOptions
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.MultiJvm

name             := "N2S3_Integration_Tests"

/*********************************************************************************************************************
  * Dependencies
  *******************************************************************************************************************/
libraryDependencies ++= {
  val scalaXmlV = "1.0.2"
  val akkaV = "2.3.7"
  val scalatestV = "2.2.1"
  Seq(
    "com.typesafe.akka" %% "akka-cluster"   % akkaV,
    "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaV,
    "org.scalatest"     %% "scalatest"       % scalatestV % "test"
  )
}
