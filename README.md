# N2S3 a Neural Network Simulator. 
Thank you for downloading N2S3. 

The aim of this simulator is to simulate different physical neural networks topology with different Synapses/Neurons models. 

## Testing N2S3 without downloading the source code. 
//TODO(dmarchal): est ce qu'on a un sch�ma de diffusion de .class m�me sans le code source ? 

## Developping N2S3
* To develop with eclipse
  * Install [Scala IDE for Eclipse](http://scala-ide.org/)
  * To be able to use SVN from Eclipse, install the Subversive Eclipse plugin (just search svn in the install new software dialog)
  * Install [sbt](http://scala-sbt.org/)
  * From a terminal in the N2S3 directory, run `sbt eclipse`
  * In Eclipse, create a new project located in the N2S3 directory
  * To get the source code from JSpikeStack, clone the SVN repository
	  `svn co svn://svn.code.sf.net/p/jaer/code/trunk/host/java/subprojects/JSpikeStack/ jaer-code`
  * Finally, create an Eclipse project pointing to the JSpikeStack directory

* Directory organization
the src directory contains the scala or java sources. The main subdirectory stores the core implementation
of n2s3 as well as the network and neuron models. The test subdirectory contains the units tests that can be executed to validate that the changes made in the software is still conformant with the initial API.
The different examples applications is in apps subdirectory. 

## Todo / Progress
* Visualisations
    * Utiliser une architecture o� l'on peut accrocher des "viewers" � des
      acteurs.  Si un viewer est accroch� � un acteur, les evenements produits
      par cet acteur sont transmits � ce viewer. On doit pouvoir ajouter et
      enlever des viewers � tout moment. Ces viewers peuvent servir au log o� �
      la visu temps-reelle.
	* Proposition : red�finir la methode process du Synchronizer.
* �crire des fichiers .aer
* Cr�er des neurones d'entr�e et des neurones (ou synapses ?) de sortie 
  * Langage textuel
  * Fichier .aer
  
## Remarques 
* Besoin du mod�le de mahyar (formule de modification du poids synaptique)
