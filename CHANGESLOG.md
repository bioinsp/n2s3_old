## Changes log v1.0

N2S3 1.0 is mainly an infrastructure release oriented towards the ease of development and distribution. This version builds on version 0.2 with ~165 new commits! There are also some news in the core of the simulator and the documentation. Here you have the major changes:

Infrastructure:
  - N2S3 is now an SBT multi-project [1]
  - Better integration with IntelliJ IDEA and Eclipse [2]
  - Supporting the system's distribution:
    - Clustering support
    - Refactoring to support the dynamic deployment of actors in different cluster nodes.
    - Two deployment strategies were implemented so far: Random and concrete node.
    - Fixes in the core to make it distributable. E.g, fixed serialization of messages, global state.
  - The Continuous Integration service was extended and enhanced [4]
  - A new input mechanism was implemented [5, 6]

Core:
  - Preliminar version of supervised learning algorithm SpikeProp
  - Preliminar version of Supervised Xor, an example of learning the Xor function with SpikeProp (Error-backpropagation in temporally encoded networks of spiking neurons)
  - Added bio-inspired model i.e., a model with parameters similar to those in the nature.

Examples:
  - Examples of Mnist/Freeway/sevenSegment with the bio-inspired model.
  - Example of Masquelier's experiment: Spike Timing Dependent Plasticity Finds the Start of Repeating Patterns in Continuous Spike Trains
  - Preliminar version of an example of direction detection with reservoir computing (using supervised STDP and only 2 directions)

Documentation:
  - The core scaladocs were entirely revisited
  - Technical documentation about the core implementation can be found in [7]

[1] https://sourcesup.renater.fr/wiki/n2s3/environmentsetup#sbt_integration
[2] https://sourcesup.renater.fr/wiki/n2s3/environmentsetup#ide_integration
[3] https://sourcesup.renater.fr/wiki/n2s3/simulation_cluster
[4] https://sourcesup.renater.fr/jenkins/job/N2S3/
[5] https://sourcesup.renater.fr/wiki/n2s3/simulation_stimuli
[6] https://sourcesup.renater.fr/wiki/n2s3/inputs
[7] https://sourcesup.renater.fr/wiki/n2s3/devdocs

## Changes log v0.2

Infrastructural:
 - all changes required to package and publish n2s3 as a library
 - all tests are green

Documentation:
 - on the documentation side, scaladocs were added on the main classes that are needed to setup experiments

Core:
 - Added ternary synapse model
